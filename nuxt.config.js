import path from 'path';
import { prismicLinkResolver } from './utils/prismic-helpers';
import getSitemapRoutes from './utils/get-sitemap-routes';
import { otherLocales } from './utils/locale-helpers';

const {
	PRISMIC_ENDPOINT,
	PRISMIC_ACCESS_TOKEN,
	SENTRY_DSN,
	SENTRY_DISABLED,
	POWERREVIEWS_READ_API_KEY,
	POWERREVIEWS_WRITE_URL,
	POWERREVIEWS_WRITE_API_KEY,
	POWERREVIEWS_MERCHANT_GROUP_ID,
	POWERREVIEWS_MERCHANT_ID_US,
	POWERREVIEWS_MERCHANT_ID_CA,
	POWERREVIEWS_IOVATION_URL,
	MAGENTO_URL,
	MK_ENV,
	CACHE_AWS_ACCESS_KEY_ID,
	CACHE_AWS_SECRET_ACCESS_KEY,
	CACHE_CLOUDFRONT_ID,
	TEALIUM_ASYNC_URL,
	CLYDE_URL,
	CLYDE_KEY,
	PROXY_PRISMIC_IMAGES = JSON.stringify(
		process.env.NODE_ENV !== 'production' || process.env.NODE_ENV !== 'apac' || process.env.NODE_ENV !== 'aurora-dev'
	),
	CUSTOM_APIS_EXTERNAL = 'false',
	MAGENTO_CART_HASH_CREATE,
	PROXY_MAGENTO = JSON.stringify(
		process.env.NODE_ENV !== 'production' || process.env.NODE_ENV !== 'apac' || process.env.NODE_ENV !== 'aurora-dev'
	),
	RECAPTCHA_SITE_KEY,
	GTM_URL,
} = process.env;

const requiredBuildEnvVariables = ['MAGENTO_URL', 'MAGENTO_CART_HASH_CREATE', 'RECAPTCHA_SITE_KEY', 'SENTRY_DISABLED'];
const requiredRuntimeEnvVariables = [
	'MAGENTO_URL',
	'MAGENTO_CART_HASH_CREATE',
	'ITERABLE_API_KEY',
	'ITERABLE_NEWSLETTER_CAMPAIGN_ID',
	'SENTRY_DISABLED',
	'PRISMIC_ENDPOINT',
	'PRISMIC_ACCESS_TOKEN',
	'POWERREVIEWS_IOVATION_URL',
	'POWERREVIEWS_READ_URL',
	'POWERREVIEWS_READ_API_KEY',
	'POWERREVIEWS_WRITE_URL',
	'POWERREVIEWS_WRITE_API_KEY',
	'POWERREVIEWS_MERCHANT_GROUP_ID',
	'POWERREVIEWS_MERCHANT_ID_US',
	'POWERREVIEWS_MERCHANT_ID_CA',
	'SG_API_KEY',
	'SG_PURCHASE_INQUIRY_TEMPLATE_ID',
	'SG_CONTENT_DOWNLOAD_TEMPLATE_ID',
	'FORMS_PURCHASE_TO',
	'FORMS_SMB_TO',
	'ZENDESK_SELL_LEADS_ENDPOINT',
	'ZENDESK_SELL_API_KEY',
	'FORMS_PURCHASE_ENDPOINT',
	'FORMS_SMB_ENDPOINT',
	'FORMS_SHEETS_SECRET',
	'RECAPTCHA_SITE_KEY',
	'RECAPTCHA_SECRET_KEY',
];

const nuxtModules = [
	'@nuxtjs/sentry',
	'@nuxtjs/axios',
	'@nuxtjs/pwa',
	'@anatta/cloudfront',
	'@/modules/anatta-clodfront-logging',
	'@anatta/prismic',
	'@nuxtjs/sitemap',
  'nuxt-basic-auth-module',
];

if (process.env.MK_ENV === 'production') nuxtModules.push('@nuxtjs/robots');

function validateEnvVariables(list) {
	const missing = [];
	list.forEach((variable) => {
		if (!process.env[variable]) {
			missing.push(variable);
		}
	});
	if (missing.length > 0) {
		throw new Error(`Missing env variables: ${missing.join(', ')}`);
	}
}

let cacheSettings = {};
if (CACHE_AWS_ACCESS_KEY_ID && CACHE_AWS_SECRET_ACCESS_KEY && CACHE_CLOUDFRONT_ID) {
	cacheSettings = {
		cachePath: path.join(__dirname, '/cache'),
		awsAccessKeyId: CACHE_AWS_ACCESS_KEY_ID,
		awsSecretAccessKey: CACHE_AWS_SECRET_ACCESS_KEY,
		cloudfrontDistributionId: CACHE_CLOUDFRONT_ID,
		cloudfrontPathsLimit: 20,
	};
}

// Ignore lack of sass in production
let sass;
try {
	sass = require('sass');
} catch (e) {
	//
}

const magentoUrlParsed = MAGENTO_URL ? new URL(MAGENTO_URL) : {};

// MyFonts tracking, minified version is used later
// (function() {
// 	window.addEventListener('DOMContentLoaded', function() {
// 		['https://hello.myfonts.net/count/30bbc3', 'https://hello.myfonts.net/count/30bbd6'].forEach(function(counter) {
// 			var el = document.createElement('link');
// 			el.rel = 'stylesheet';
// 			el.type = 'text/css';
// 			el.media = 'only x';
// 			el.href = counter;
// 			document.head.appendChild(el);
// 		});
// 	});
// })();

export default {
	/*
	 ** Headers of the page
	 */
	head: {
		link: [],
		// Tealium sync and Sentry release are injected in layouts/default.vue
		script: [
			// {
			// 	src: '/nr.js',
			// },
			{
				src: `https://maps.googleapis.com/maps/api/js?key=AIzaSyATzu4PA0QtqwRTL_w07hN_sLZLH75bVus&libraries=places`,
				defer: true,
			},
			{
				hid: 'myfonts-tracking-script',
				type: 'text/javascript',
				innerHTML: `(function(){window.addEventListener("DOMContentLoaded",function(){["https://hello.myfonts.net/count/30bbc3","https://hello.myfonts.net/count/30bbd6"].forEach(function(e){var t=document.createElement("link");t.rel="preload",t.type="text/css",t.media="only x",t.href=e,document.head.appendChild(t)})});})();`,
			},
			{
				hid: 'twitter-json',
				type: 'application/ld+json',
				json: {
					'@context': 'http://schema.org',
					'@type': 'Organization',
					address: {
						'@type': 'PostalAddress',
						addressLocality: 'San Francisco',
						AddressRegion: 'CA',
					},
					brand: 'Molekule',
					name: 'Molekule',
					sameAs: [
						'https://twitter.com/molekuleair',
						'https://www.instagram.com/molekuleair/',
						'https://www.facebook.com/molekuleair',
						'https://www.pinterest.com/molekuleair/',
						'https://www.youtube.com/channel/UCnE584ezgOTeGhqtZ3UzcSA',
					],
					logo: {
						'@type': 'ImageObject',
						url: 'https://images.prismic.io/molekule/254605a9-c084-439c-bb67-4b592db75439_logo.png',
					},
					slogan: 'Air Purification, Reinvented',
					description:
						'The air purifier, reinvented. Molekule makes the best air purifiers by using PECO technology to break down pollutants on a molecular level, destroying airborne VOCs, bacteria, mold, viruses &amp; allergens.',
					url: 'https://molekule.com/',
				},
			},
		],

		meta: [
			{
				hid: 'og:title',
				property: 'og:title',
				content: 'Air Purification, Reinvented',
			},
			{
				hid: 'og:type',
				property: 'og:type',
				content: 'website',
			},
			{
				hid: 'og:url',
				property: 'og:url',
				content: 'https://molekule.com',
			},
			{
				hid: 'og:image',
				property: 'og:image',
				content:
					'https://images.prismic.io/molekule/9ff6f8d4-e721-486d-8010-994b0e2232ff_molekule+productssm.png?max-w=1200&max-h=1200&fit=crop&q=80',
			},
			{
				hid: 'og:image:alt',
				property: 'og:image:alt',
				content: 'Molekule Air and Molekule Air Mini available now at molekule.com',
			},
			{
				hid: 'og:description',
				property: 'og:description',
				content:
					'The air purifier, reinvented. Molekule makes the best air purifiers by using PECO technology to break down pollutants on a molecular level, destroying airborne VOCs, bacteria, mold, viruses &amp; allergens.',
			},
			{
				hid: 'og:site_name',
				property: 'og:site_name',
				content: 'Molekule',
			},
			{
				hid: 'twitter:card',
				name: 'twitter:card',
				content: 'summary_large_image',
			},
			{
				hid: 'twitter:site',
				name: 'twitter:site',
				content: '@molekuleair',
			},
			{
				hid: 'twitter:title',
				name: 'twitter:title',
				content: 'Air Purification, Reinvented',
			},
			{
				hid: 'twitter:description',
				name: 'twitter:description',
				content:
					'The air purifier, reinvented. Molekule makes the best air purifiers by using PECO technology to break down pollutants on a molecular level, destroying airborne VOCs, bacteria, mold, viruses &amp; allergens.',
			},
			{
				hid: 'twitter:image',
				name: 'twitter:image',
				content:
					'https://images.prismic.io/molekule/9ff6f8d4-e721-486d-8010-994b0e2232ff_molekule+productssm.png?max-w=4096&max-h=4096&fit=crop&q=80&fm=webp',
			},
			{
				hid: 'twitter:image:alt',
				name: 'twitter:image:alt',
				content: 'Molekule Air and Molekule Air Mini available now at molekule.com',
			},
		],

		__dangerouslyDisableSanitizersByTagID: {
			'myfonts-tracking-script': ['innerHTML'],
		},
	},

	meta: {
		description: false,
		ogDescription: false,
	},

	manifest: {
		name: 'Molekule',
		short_name: 'Molekule',
		description: undefined,
		crossorigin: 'use-credentials', /// https://github.com/nuxt-community/pwa-module/issues/94
	},

	icons: {
		sizes: [64, 120, 144, 152, 192, 384, 512],
	},

	/*
	 ** Customize the progress-bar color
	 */
	loading: {
		color: '#7befb2',
	},

	/*
	 ** Global CSS
	 */
	css: [],

	env: {
		magento: {
			url: MAGENTO_URL,
			cartHashCreate: MAGENTO_CART_HASH_CREATE,
			pathname: magentoUrlParsed.pathname,
		},

		magentoUrl: MAGENTO_URL,
		// >> AURORA
		stripe_pk: process.env.STRIPE_PK,
		prismic_img_token: process.env.PRISMIC_ACCESS_TOKEN,
		prismic_endpoint: process.env.PRISMIC_ENDPOINT,
		baseApi: {
			product_base_url: process.env.CT_PRODUCT_API,
			cart_base_url: process.env.CT_CART_API,
			checkout_base_url: process.env.CT_CHECKOUT_API,
			order_base_url: process.env.CT_ORDER_API,
			account_base_url: process.env.CT_ACCOUNT_API,
		},
		aws_identity_pool: process.env.COGNITO_IDENTITYPOOLID,
		aws_region: process.env.COGNITO_REGION,
		aws_userpool: process.env.COGNITO_USERPOOLID,
		aws_web_client_id: process.env.COGNITO_USERPOOLWEBCLIENTID,
		talkable_site_id: process.env.TALKABLE_SITE_ID,
		NR_ENV: process.env.MK_ENV,
		CMS_DEFAULT_LOCALE: process.env.CMS_DEFAULT_LOCALE,
		ZENDESK_WEB_WIDGET_ID: process.env.ZENDESK_WEB_WIDGET_ID,
		PRISMIC_DOMAIN_URL: process.env.PRISMIC_DOMAIN_URL,
		MK_DEBUG: process.env.MK_DEBUG
	},

	publicRuntimeConfig: {
		powerReviews: {
			readApiKey: POWERREVIEWS_READ_API_KEY,
			writeUrl: POWERREVIEWS_WRITE_URL,
			writeApiKey: POWERREVIEWS_WRITE_API_KEY,
			merchantGroupId: POWERREVIEWS_MERCHANT_GROUP_ID,
			merchantIdUS: POWERREVIEWS_MERCHANT_ID_US,
			merchantIdCA: POWERREVIEWS_MERCHANT_ID_CA,
			templatePageId: 'molekule-air',
			iovationUrl: POWERREVIEWS_IOVATION_URL,
		},

		recaptcha: {
			siteKey: RECAPTCHA_SITE_KEY,
		},

		tealium: {
			asyncUrl: TEALIUM_ASYNC_URL,
		},

		clydeUrl: CLYDE_URL,
		clydeKey: CLYDE_KEY,
		prismicApiEndpoint: PRISMIC_ENDPOINT,
		gtmUrl: GTM_URL,
		affirmPublicKey: process.env.AFFIRM_PUBLIC_KEY,
		affirmPrivateKey: process.env.AFFIRM_PRIVATE_KEY,
	},

	router: {
		prefetchLinks: false,
		extendRoutes(routes, resolve) {
			const allRoute = routes.find(({ path }) => path === '/*');
			const routesWithoutAll = routes.filter(({ path }) => path !== '/*');
			routesWithoutAll.push({
				name: 'holiday',
				path: '/holiday',
				component: resolve(__dirname, 'pages/index'),
			});
			const otherLocalesRoutes = otherLocales.reduce((final, locale) => {
				const localeRoutes = routesWithoutAll.map((route) => ({
					...route,
					name: `${route.name}___${otherLocales.includes(locale) ? locale.split('-').reverse().join('-') : locale}`,
					path: `/${locale}${route.path}`,
				}));

				final.push(...localeRoutes);

				return final;
			}, []);
			return [...routesWithoutAll, ...otherLocalesRoutes, allRoute];
		},
		middleware: ['country-middleware', 'first-page-load-middleware'],
	},

	/*
	 ** Plugins to load before mounting the App
	 */
	plugins: [
		'~/plugins/directives.client.js',
		'~/plugins/lazysizes.client.js',
		'~/plugins/blog-redirect',
		'~/plugins/country-redirect.js',
		'~/plugins/base-components',
		'~/plugins/global-components',
		'~/plugins/server-headers',
		'~/plugins/magento-axios-instance',
		'~/plugins/filters',
		'~/plugins/has-rich-text',
		{ src: `~/plugins/utility`, ssr: false },
		{ src: '~/plugins/repositories', ssr: false },
		{ src: '~/plugins/vee-validate', ssr: false },
		{ src: '~/plugins/cognitoServerPlugin', ssr: false },
		{ src: '~/plugins/telium', ssr: false },
		{ src: '~/plugins/prismicImage', ssr: false },
		{ src: '~/plugins/clyde', ssr: false },
		{ src: '~/plugins/affirm-aurora', ssr: false },

		{ src: '~/plugins/intersection-observer', ssr: false },
		{ src: '~/plugins/affirm', ssr: false },
		// '~/plugins/clyde.client',
		{ src: '~/plugins/newrelic', ssr: false },
		{ src: '~/plugins/tealium', ssr: false },
		{ src: '~/plugins/lazy-loading', ssr: false },
		{ src: '~/plugins/store-client', ssr: false },
		{ src: '~/plugins/focus-visible', ssr: false },
		{ src: '~/plugins/rich-content', ssr: false },
		{ src: '~/plugins/prismic-preview', ssr: false },
	],

	/*
	 ** Nuxt.js modules
	 */
	modules: nuxtModules,
  basic: {
    name: 'mktester',
    pass: 'Fr3sh@ir',
    enabled: process.env.BASIC_ENABLED === 'true' // require boolean value(nullable)
  },

	anatta: {
		pwa: {
			...cacheSettings,
			prismicApiEndpoint: PRISMIC_ENDPOINT,
			prismicAccessToken: PRISMIC_ACCESS_TOKEN,
			prismicLinkResolver,
		},
	},

	sentry: {
		dsn: SENTRY_DSN, // Enter your project's DSN here
		disabled: JSON.parse(SENTRY_DISABLED),
		config: {
			environment: MK_ENV,
			ignoreErrors: ['chrome.loadTimes() is deprecated'],
		},
		lazy: true,
		// we're intentionally not using publishRelease because it is injecting release id into the vendor bundle
		// causing it's hash to change on every release
	},

	/*
	 ** Axios module configuration
	 */
	// See https://github.com/nuxt-community/axios-module#options
	axios: {
		proxy: true,
		proxyHeaders: false,
	},

	/*
	 ** Sitemap module configuration
	 */
	// See https://github.com/nuxt-community/sitemap-module#sitemap-options

	sitemap: {
		routes: getSitemapRoutes,
		hostname: process.env.NODE_ENV,
		routes: [
			{
				url: '/',
				changefreq: 'daily',
				priority: 1,
				lastmod: new Date(),
			},
			{
				url: '/air-mini-plus-reviews',
				changefreq: 'daily',
				priority: 1,
				lastmod: new Date(),
			},
			{
				url: '/air-mini-reviews',
				changefreq: 'daily',
				priority: 1,
				lastmod: new Date(),
			},
			{
				url: '/air-pro-reviews',
				changefreq: 'daily',
				priority: 1,
				lastmod: new Date(),
			},
			{
				url: '/air-purifier-air',
				changefreq: 'daily',
				priority: 1,
				lastmod: new Date(),
			},
			{
				url: '/air-purifier-air-pro',
				changefreq: 'daily',
				priority: 1,
				lastmod: new Date(),
			},
			{
				url: '/air-purifier-mini',
				changefreq: 'daily',
				priority: 1,
				lastmod: new Date(),
			},
			{
				url: '/air-purifier-pro-rx',
				changefreq: 'daily',
				priority: 1,
				lastmod: new Date(),
			},
			{
				url: '/air-purifiers',
				changefreq: 'daily',
				priority: 1,
				lastmod: new Date(),
			},
			{
				url: '/air-reviews',
				changefreq: 'daily',
				priority: 1,
				lastmod: new Date(),
			},
			{
				url: '/business',
				changefreq: 'daily',
				priority: 1,
				lastmod: new Date(),
			},
			{
				url: '/fda-cleared-mini',
				changefreq: 'daily',
				priority: 1,
				lastmod: new Date(),
			},
			{
				url: '/healthcare',
				changefreq: 'daily',
				priority: 1,
				lastmod: new Date(),
			},
			{
				url: '/shop',
				changefreq: 'daily',
				priority: 1,
				lastmod: new Date(),
			},
			{
				url: '/technology',
				changefreq: 'daily',
				priority: 1,
				lastmod: new Date(),
			},
		],
		exclude: [
			'/D2C',
			'/D2C/**',
			'/jp-ja/D2C',
			'/jp-ja/D2C/**',
			'/eu-en/D2C',
			'/eu-en/D2C/**',
			'/ca-en/D2C',
			'/ca-en/D2C/**',
			'/in-en/D2C',
			'/in-en/D2C/**',
			'/kr-ko/D2C',
			'/kr-ko/D2C/**',
			'/gb-en/D2C',
			'/gb-en/D2C/**',
			'/gb-en/air-reviews',
			'/gb-en/air-purifier-pro-rx',
			'/gb-en/air-mini-reviews',
			'/eu-en/air-reviews',
			'/eu-en/air-purifier-pro-rx',
			'/eu-en/air-mini-reviews',
		],
		// i18n: {
		// 	locales: ['en', 'en-ca', 'en-gb'],
		// 	routesNameSeparator: '___',
		// },
		defaults: {
			changefreq: 'daily',
			priority: 0.5,
			lastmod: new Date(),
		},
		filter: ({ routes, options }) => routes.filter((route) => route.lastmod),
		cacheTime: 1000,
	},

	/*
	 ** Robots module configuration
	 */
	// See https://github.com/nuxt-community/robots-module#options
	robots: {
		UserAgent: '*',
		Disallow: '',
		Sitemap: 'https://molekule.com/sitemap.xml',
	},

	proxy: {
		...(JSON.parse(PROXY_PRISMIC_IMAGES) && {
			'/prismic-images': {
				target: 'http://images.prismic.io/',
				pathRewrite: {
					'^/prismic-images': '',
				},
			},
		}),

		...(JSON.parse(PROXY_MAGENTO) && {
			[magentoUrlParsed.pathname]: {
				target: magentoUrlParsed.origin,
				autoRewrite: true,
				protocolRewrite: 'http',

				cookieDomainRewrite: {
					[magentoUrlParsed.host]: '',
					'molekule.com': '',
				},

				onProxyReq: (proxyReq, req, res) => {
					proxyReq.removeHeader('accept-encoding');
				},

				onProxyRes: async (proxyRes, req, res) => {
					const sc = proxyRes.headers['set-cookie'];
					if (Array.isArray(sc)) {
						proxyRes.headers['set-cookie'] = sc
							// remove secure flag from headers
							.map((sc) =>
								sc
									.split(';')
									.filter((v) => v.trim().toLowerCase() !== 'secure')
									.join(';')
							)
							// remove PHPSESSID=deleted cookie
							.filter((cookie) => !cookie.includes('PHPSESSID=deleted'));
					}

					if (proxyRes.headers['content-type'] && proxyRes.headers['content-type'].includes('text/html')) {
						// https://github.com/chimurai/http-proxy-middleware/issues/97
						const _end = res.end;
						res.write = () => {};
						res.end = () => {};
						const body = await require('raw-body')(proxyRes);
						const updatedBody = body
							.toString('utf8')
							.split(magentoUrlParsed.origin)
							.join(`http://${req.headers.host}`)
							.split(`${magentoUrlParsed.protocol}\\/\\/${magentoUrlParsed.host}`)
							.join(`http:\\/\\/${req.headers.host}`);
						const updatedBodyBuffer = Buffer.from(updatedBody, 'utf8');
						res.setHeader('content-length', updatedBodyBuffer.length);
						_end.call(res, updatedBodyBuffer);
					}
				},
			},
		}),

		...(JSON.parse(CUSTOM_APIS_EXTERNAL) && {
			'/custom-apis': {
				target: 'http://localhost:3001/',
				pathRewrite: {
					'^/custom-apis': '',
				},
			},
		}),
	},

	/*
	 ** Build configuration
	 */
	buildModules: [
		// AURORA
		'@nuxtjs/tailwindcss',
	],
	build: {
		analyze: true,
		optimizeCSS: true,
		loaders: {
			cssModules: {
				localsConvention: 'camelCase',
				modules: {
					localIdentName: '[local]_[hash:base64:5]',
				},
			},
			scss: {
				implementation: sass,
			},
		},
		optimization: {
			minimize: true,
			splitChunks: {
				chunks: 'all',
			},
		},
		vueLoader: {
			transformAssetUrls: {
				video: ['src', 'poster'],
				source: 'src',
				img: 'src',
				image: 'xlink:href',
				LazyImg: ['src', 'thumbnail'],
			},
		},
		/*
		 ** You can extend webpack config here
		 */
		extend(config, ctx) {
			validateEnvVariables(requiredBuildEnvVariables);

			config.resolve.extensions.push('.scss');

			config.resolve = {
				...config.resolve,
				alias: {
					...config.resolve.alias,
					...require('./aliases.config').nuxt,
				},
			};

			if (!ctx.isDev) {
				// enable sourcemaps for production
				// based on https://github.com/nuxt-community/sentry-module/blob/v3.3.1/lib/module.js#L172
				config.devtool = ctx.isClient ? 'hidden-source-map' : 'source-map';
			}
		},

		plugins: [
			{
				apply(compiler) {
					compiler.hooks.emit.tap(
						{
							name: 'molekule-fix-pwa-icons',
							stage: 1,
						},
						(compilation) => {
							// icons resized by @nuxtjs/pwa have artifacts, we're replacing them with our icons
							const path = require('path');
							const fs = require('fs');

							Object.keys(compilation.assets)
								.filter((asset) => asset.startsWith('icons/'))
								.forEach((icon) => {
									const size = /icon_(\d+)/.exec(icon)[1];
									const file = path.resolve(__dirname, 'static', 'icons', `icon-${size}.png`);
									const fileBuffer = fs.readFileSync(file);
									compilation.assets[icon] = {
										source: () => fileBuffer,
										size: () => fileBuffer.length,
									};
								});
						}
					);
				},
			},
		],
	},

	render: {
		bundleRenderer: {
			shouldPreload: (file, type) => {
				return ['style', 'font'].includes(type);
			},
			directives: {
				lazy: function lazySsrDirective(node, meta) {
					// console.log(meta, node);
					if (meta.value && meta.value.src && meta.value.critical) {
						const modifiers = meta.modifiers;
						if (modifiers && modifiers.backgroundImage) {
							const style = node.data.style || (node.data.style = {});
							if (Array.isArray(style)) {
								style.push({
									backgroundImage: `url(${meta.value.src})`,
								});
							} else {
								style.backgroundImage = `url(${meta.value.src})`;
							}
						} else if (modifiers && modifiers.picture) {
							throw new Error('v-lazy: critical is not supported with picture modifier');
						} else {
							const domProps = node.data.domProps || (node.data.domProps = {});
							domProps.src = meta.value.src;
						}
					}
				},
			},
		},
	},

	serverMiddleware: [
		...(JSON.parse(CUSTOM_APIS_EXTERNAL)
			? []
			: [
					{
						path: '/custom-apis',
						handler: '~/custom-apis/nuxt-middleware',
					},
			  ]),
	],

	hooks: {
		listen() {
			validateEnvVariables(requiredRuntimeEnvVariables);
		},
	},
	tailwindcss: {
		// AURORA
		cssPath: '~/assets/styles/tailwind.css',
		purge: {
			enabled: true,
		},
	},
};
