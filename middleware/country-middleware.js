export default ({ store, route, redirect, req, res, next }) => {
	if (process.client) {
		const { getLocaleMatchInRoutePath } = require('@/utils/locale-helpers');
		const localeMatchInRoute = getLocaleMatchInRoutePath(route.path) || 'en-us';
console.log('def coun', process.env.CMS_DEFAULT_LOCALE, store.state.locale, route.fullPath)
		if (localeMatchInRoute !== store.state.locale) {
			redirect(store.state.locale === process.env.CMS_DEFAULT_LOCALE ? '' : `/${store.state.locale}` + route.fullPath);
		}
	}
	if (process.server) {
		// console.log(req.url)
		// if (req.url.indexOf('D2C')) {
		// 	console.log(req.url)
		// 	res.ssr = false;
		// 	res.spa = true;
		// }
		// next();
	}
};
