sizes=(64 120 144 152 192 384 512)
dir=`dirname "$0"`
for size in "${sizes[@]}"
do
	convert $dir/../icon.png -resize $size $dir/icon-$size.png
done
