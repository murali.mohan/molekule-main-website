function generateBreakpoint(width) {
	if (process.server) return null;
	return window.matchMedia(`(min-width: ${width}px)`);
}

const breakpoints = {
	small: generateBreakpoint(30 * 16),
	medium: generateBreakpoint(48 * 16),
	large: generateBreakpoint(64 * 16),
	xlarge: generateBreakpoint(75 * 16),
	xxlarge: generateBreakpoint(90 * 16),
};

export const state = () => {
	const ret = {};
	Object.entries(breakpoints).forEach(([bp, matcher]) => {
		ret[bp] = process.server ? false : matcher.matches;
	});
	return ret;
};

export const actions = {
	SETUP_BREAKPOINTS({ commit }) {
		Object.keys(breakpoints).forEach((breakpoint) => {
			breakpoints[breakpoint].addListener(({ matches }) => {
				commit('UPDATE_BREAKPOINT', { breakpoint, matches });
			});
		});

		commit('INITIALIZE_BREAKPOINTS', state());
	},
};

export const mutations = {
	INITIALIZE_BREAKPOINTS(state, breakpoints) {
		Object.assign(state, breakpoints);
	},

	UPDATE_BREAKPOINT(state, { breakpoint, matches }) {
		state[breakpoint] = matches;
	},
};
