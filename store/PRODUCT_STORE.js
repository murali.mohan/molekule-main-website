const state = () => {
	return {
		PRODUCTS: [],
		FILTER_PRODUCTS: [],
		PRODUCT_API_IN_PROGRESS: false,
		PRODUCTS_BY_CATEGORY: [],
	};
};
const actions = {
	async GET_PRODUCTS({ state, commit }, payload) {
		if (state.PRODUCTS.length) {
			return;
		}
		const $customerId = this.state.USER_STORE.CUSTOMERID ? this.state.USER_STORE.CUSTOMERID : '';
		commit('SET_API_IN_PROGRESS', true);
		await this.$repositories.product
			.getAllProducts($customerId)
			.then((res) => {
				if (res.status === 200 && res.data) {
					commit('SET_PRODUCTS', res.data);
				}
			})
			.finally(() => {
				commit('SET_API_IN_PROGRESS', false);
			});
	},
	async GET_FILTERS({ state, commit, dispatch }, payload) {
		if (!state.PRODUCTS.length) {
			await dispatch('GET_PRODUCTS');
		}
		const $products = state.PRODUCTS;
		const $filters = $products.filter(($obj) => {
			if (
				$obj.productBean &&
				$obj.productBean.productType &&
				$obj.productBean.productType.obj &&
				$obj.productBean.productType.obj.key
			) {
				const $productType = $obj.productBean.productType.obj.key;
				if ($productType === 'filter') {
					$obj.qty = 1;
					$obj.cartLoading = false;
					if ($productType.hasSubscription) {
						$obj.purchase_type = 'subscribe';
					} else {
						$obj.purchase_type = 'one-time';
					}
					return $obj;
				}
			}
			return false;
		});
		state.FILTER_PRODUCTS = $filters;
	},
	async GET_FILTERS_WITH_SUBSCRIPTION({ state, commit, dispatch }, payload) {
		if (!state.PRODUCTS.length) {
			await dispatch('GET_PRODUCTS');
		}
		const $products = state.PRODUCTS;
		const $filters = $products.filter(($obj) => {
			if ($obj.hasSubscription) {
				const $productType = $obj?.productBean?.productType?.obj?.key;
				if ($productType === 'filter') {
					$obj.qty = 1;
					$obj.cartLoading = false;
					$obj.purchase_type = 'subscribe';
					return $obj;
				}
				return false;
			}
			return false;
		});
		commit('SET_FILTERS', $filters);
	},
	async GET_PRODUCTS_BY_CATEGORY({ state, commit }, payload) {
		commit('SET_API_IN_PROGRESS', true);
		await this.$repositories.product
			.getProductsByCategory(payload)
			.then((res) => {
				if (res.status === 200 && res.data) {
					commit('SET_PRODUCTS_BY_CATEGORY', res.data);
				}
			})
			.finally(() => {
				commit('SET_API_IN_PROGRESS', false);
			});
	},
};
const mutations = {
	SET_PRODUCTS(state, payload) {
		state.PRODUCTS = payload.productResponseList;
	},
	SET_FILTERS(state, payload) {
		state.FILTER_PRODUCTS = payload;
	},
	SET_API_IN_PROGRESS(state, payload) {
		state.PRODUCT_API_IN_PROGRESS = payload;
	},
	SET_PRODUCTS_BY_CATEGORY(state, payload) {
		state.PRODUCTS_BY_CATEGORY = payload.results;
	},
};
export default {
	namespaced: true,
	state,
	actions,
	mutations,
};
