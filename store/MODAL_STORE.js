
// initial state
const state = () => {
  return {
    IS_ACTIVE: false
  }
};

// mutations
const mutations = {
  TOGGLE_MODAL: (state) => {
    state.IS_ACTIVE = !state.IS_ACTIVE;
  }

};

export default 
{
  namespaced: true,
  state,
  mutations
};