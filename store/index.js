import { processGlobalPrismicDoc } from '@/custom-apis/pages/utils';
import { otherLocales } from '@/utils/locale-helpers';
import localeList from '@/components/json/locale_list.json';

export const state = () => ({
	globalData: null,
	locale: process.env.CMS_DEFAULT_LOCALE,
	isUSWebsite: true, // why is US Website? Because no URL appending needs to happen in this case, otherwise for all countries, /au /ca need to be appended in the URLå
	canadaSlug: 'ca-en',
	currencyDisplay: '$',
	apiLocale: 'en-US',
	apiCountry: 'US',
	paymentGateway: 'stripe',
	isEurope: false,
});

export const actions = {
	async nuxtServerInit({ state, commit, dispatch, getters }, { req }) {
		const { standardLocale: locale } = getters;
		const globalData = await processGlobalPrismicDoc(
			{ url: req.url, params: { locale }, headers: req.headers, prismicUid: 'global' },
			await req.locals.anattaPrismicRequest(`/api/${locale}/global.json`)
		);
		commit('SET_GLOBAL_DATA', globalData);

		// await dispatch('magento/INITIALIZE_SERVER');
	},
};

export const mutations = {
	SET_GLOBAL_DATA(state, data) {
		state.globalData = data;
	},

	IS_US_WEBSITE(state, value) {
		state.isUSWebsite = value;
	},

	SET_LOCALE(state, locale = process.env.CMS_DEFAULT_LOCALE) {
		state.locale = locale;
		const currentLocale = localeList[locale] || localeList['en-us'];
		state.currencyDisplay = currentLocale.symbol;
		state.apiLocale = currentLocale.apiLocale;
		if (locale === 'gb-en' || locale === 'gr-en' || locale === 'en-gb') {
			state.isEurope = true;
			state.paymentGateway = 'adyen';
		}
	},
	SET_API_COUNTRY(state, country = 'US') {
		console.log('apiCountry=', country);
		state.apiCountry = country;
	},
};

export const getters = {
	standardLocale({ locale }) {
		return otherLocales.includes(locale) ? locale.split('-').reverse().join('-') : locale;
	},
	apiCountry({ apiCountry }) {
		return apiCountry;
	},
	currencySymbol({ currencyDisplay }) {
		return currencyDisplay;
	},
	isEurope({ isEurope }) {
		return isEurope;
	},
};
