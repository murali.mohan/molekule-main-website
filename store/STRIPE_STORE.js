// initial state
const state = () => {
  return {
    CALL_IN_PROGRESS: false,
    STRIPE_ERROR_CODE: '',
    STRIPE_ERROR_MSG: '',
    STRIPE_RESPONSE: {
      success: false,
      error: false,
      errors: [],
    },
    CARD_ICON: 'card-icon-generic.png',
  }
}
// actions
const actions = {
  CARD_VALIDATION({ commit }, payload) {
    const { error } = payload
    if (error) {
      commit('SET_STRIPE_ERROR_CODE', error.code)
    } else {
      commit('SET_STRIPE_ERROR_CODE', '')
    }
  },
  async CREATE_TOKEN({ commit }, payload) {
    // await stripe.createToken(card)
  },
  async TOKEN_RESPONSE({ commit }, payload) {},
  CARD_ICON({ commit }, payload) {
    const { brand, error } = payload

    if (error) {
      commit('SET_CARD_ICON', 'error-icon.png')
      return
    }

    if (!brand) {
      return
    }

    const cardImageSet = {
      visa: 'visa1.png',
      mastercard: 'mastercard.png',
      discover: 'discover.png',
      amex: 'amex.png',
      unionpay: 'unionpay.png',
      jcb: 'jcb.png',
      diners: 'diners.png',
      unknown: 'card-icon-generic.png',
    }

    if (cardImageSet[brand]) {
      commit('SET_CARD_ICON', cardImageSet[brand])
    }
  },
}
// mutations
const mutations = {
  SET_CALL_IN_PROGRESS(state, payload) {
    state.CALL_IN_PROGRESS = payload
  },
  SET_STRIPE_RESPONSE(state, payload) {
    state.STRIPE_RESPONSE = payload
  },
  SET_STRIPE_ERROR_CODE(state, payload) {
    state.STRIPE_ERROR_CODE = payload
  },
  SET_STRIPE_ERROR_MSG(state, payload) {
    state.STRIPE_ERROR_MSG = payload
  },
  SET_CARD_ICON(state, payload) {
    state.CARD_ICON = payload
  },
}
export default {
  namespaced: true,
  state,
  actions,
  mutations,
}
