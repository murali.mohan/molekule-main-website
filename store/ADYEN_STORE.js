// initial state
const state = () => {
	return {
		CALL_IN_PROGRESS: false,
		ADYEN_ERROR_MSG: '',
		CARD_ICON: 'card-icon-generic.png',
		VALID_CARD: '',
		CARD_DETAILS: {}
	};
};
// actions
const actions = {
	CARD_VALIDATION({ commit }, payload) {
		const { error, fieldType } = payload;
		const errorMsg = {
			'error.va.gen.01': {
				encryptedCardNumber: 'Your card number is incomplete.',
				encryptedExpiryDate: "Your card's expiration date is incomplete.",
				encryptedSecurityCode: "Your card's security code is incomplete.",
			},
			'error.va.gen.02': 'Field not valid',
			'error.va.sf-cc-num.01': 'Please enter a valid card number.',
			'error.va.sf-cc-num.02': "Typed card number doesn't match card brand",
			'error.va.sf-cc-num.03': 'Unsupported card entered',
			'error.va.sf-cc-dat.01': 'This card has expired.',
			'error.va.sf-cc-dat.02': 'Date too far in the future',
		};
		if (error && errorMsg[error]) {
			if (error === 'error.va.gen.01') {
				commit('SET_ADYEN_ERROR_MSG', errorMsg[error][fieldType]);
			} else {
				commit('SET_ADYEN_ERROR_MSG', errorMsg[error]);
			}
		} else if(error && (error['resultCode'] || error['errorCode'])){
			commit('SET_ADYEN_ERROR_MSG', error['refusalReason'] || error['message']);
		} else {
			commit('SET_ADYEN_ERROR_MSG', '');
		}
	},
	CARD_ICON({ commit }, payload) {
		const { brand, error } = payload;

		if (error) {
			commit('SET_CARD_ICON', 'error-icon.png');
			return;
		}

		if (!brand) {
			return;
		}

		const cardImageSet = {
			visa: 'visa1.png',
			mastercard: 'mastercard.png',
			discover: 'discover.png',
			amex: 'amex.png',
			unionpay: 'unionpay.png',
			jcb: 'jcb.png',
			diners: 'diners.png',
			unknown: 'card-icon-generic.png',
			card: 'card-icon-generic.png',
			mc: 'mastercard.png',
			cup: 'unionpay.png',
		};

		if (cardImageSet[brand]) {
			commit('SET_CARD_ICON', cardImageSet[brand]);
			commit('SET_VALID_CARD', brand);
		}
	},
	ADYEN_CARD_DETAILS({ commit }, payload) {
		commit('SET_CARD_DETAILS', payload);
	}
};
// mutations
const mutations = {
	SET_CALL_IN_PROGRESS(state, payload) {
		state.CALL_IN_PROGRESS = payload;
	},
	SET_ADYEN_ERROR_MSG(state, payload) {
		state.ADYEN_ERROR_MSG = payload;
	},
	SET_CARD_ICON(state, payload) {
		state.CARD_ICON = payload;
	},
	SET_VALID_CARD(state, payload) {
		state.VALID_CARD = payload;
	},
	SET_CARD_DETAILS(state, payload) {
		state.CARD_DETAILS = payload
	}
};
export default {
	namespaced: true,
	state,
	actions,
	mutations,
};
