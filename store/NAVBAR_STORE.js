// initial state
const state = () => {
  return {
    HAMBURGER_TOGGLED: false
  }
};

// mutations
const mutations = {
  TOGGLE_HAMBURGER: (state) => {
    state.HAMBURGER_TOGGLED = !state.HAMBURGER_TOGGLED;
  }

};

export default {
  namespaced: true,
  state,
  mutations,
};