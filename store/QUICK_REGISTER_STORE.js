import { Auth } from 'aws-amplify'

// You can get the current config object
Auth.configure({
  // REQUIRED only for Federated Authentication - Amazon Cognito Identity Pool ID
  identityPoolId: process.env.aws_identity_pool,

  // REQUIRED - Amazon Cognito Region
  region: process.env.aws_region,

  // OPTIONAL - Amazon Cognito User Pool ID
  userPoolId: process.env.aws_userpool,

  // OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
  userPoolWebClientId: process.env.aws_web_client_id,
  //   cookieStorage: {
  // // REQUIRED - Cookie domain (only required if cookieStorage is provided)
  //     domain: 'localhost:3000',
  // // OPTIONAL - Cookie path
  //     path: '/',
  // // OPTIONAL - Cookie expiration in days
  //     expires: 365,
  // // OPTIONAL - See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie/SameSite
  //   //  sameSite: "lax",
  // // OPTIONAL - Cookie secure flag
  // // Either true or false, indicating if the cookie transmission requires a secure protocol (https).
  //     secure: false
  // },
})
// initial state
const state = () => {
  return {
    IS_ACTIVE: false,
    API_CALL_IN_PROGRESS: false,
    FEEDBACK: null,
    REGISTRATION_RESPONSE: {
      success: false,
      error: false,
    },
  }
}
// actions
const actions = {
  async CREATE_COGNITO_USER({ dispatch }, payload) {
    const msg = 'fail'
    try {
      const {
        email,
        password,
        accountId,
        customerId,
        firstName,
        lastName,
        countryCode,
      } = payload
      if (
        (!email || !password || !accountId || !customerId,
        !firstName || !lastName || !countryCode)
      )
        return msg

      // TODO - Map custom attributes properly to avoid this error on new signup
      // code: "SerializationException"
      // message: "Start of structure or map found where not expected."
      // name: "SerializationException"

      // NOTE: SignUp is done with Cognito on BE
      // await Auth.signUp({
      //   username: email,
      //   password: password,
      //   given_name: firstName,
      //   family_name: lastName,
      //   country_code: countryCode,
      //   attributes: {
      //     email,
      //     "custom:accountId": accountId,
      //     "custom:customerId": customerId
      //   }
      // })

      // NOTE MUST ADD CHANEL WHEN MK Verifys
      //   await Auth.signUp({username: email, password : password, given_name : firstName, family_name : lastName, country_code : countryCode ,   "ClientMetadata": {
      //     "custom:accountId": accountId,
      //     "custom:customerId": customerId,
      //     "custom:chanel" : 'b2b'
      //  }});
      const loginPayload = {
        email,
        password,
      }

      await dispatch('USER_STORE/ACCOUNT_SIGN_IN', loginPayload, { root: true })
      return msg
    } catch (e) {
      console.log('Failed to create a new cognito user', e)
      return msg
    }
  },
  async API_REGISTER({ commit, dispatch }, payload) {
    if (state.API_CALL_IN_PROGRESS)
      return commit('SET_FEEDBACK', 'The Last Api Message hasnt finished yet')
    // Pick API Specific params from payload.
    const {
      quick_reg_first_name: firstName,
      quick_reg_last_name: lastName,
      quick_reg_email_address: email,
      quick_reg_password: password,
      quick_reg_company_name: companyName,
      quick_reg_company_category: companyCategoryId,
      quick_reg_phone_number: mobileNumber,
    } = payload
    // Construct API Request param object
    const apiParams = {
      firstName,
      lastName,
      email,
      password,
      companyName,
      companyCategoryId,
      mobileNumber,
      customerSource: 'D2C_WEB', // NOTE: hardcoded, need to change as dynamic
      channel: 'D2C', // NOTE: hardcoded, need to change as dynamic
      countryCode: 'US', // NOTE: hardcoded, need to change as dynamic
    }
    try {
      const toCognito = {
        email,
        password,
        accountId: null,
        customerId: null,
        firstName,
        lastName,
        countryCode: 'US',
      }
      // SET TO LOADING
      commit('SET_PROCESSING', true)
      commit('SET_RESPONSE', {})
      await this.$repositories.user
        .create(apiParams)
        .then(async (res) => {
          const { data } = res
          const { customerId, accountId, errors } = data
          if (accountId || customerId) {
            toCognito.accountId = accountId
            toCognito.customerId = customerId
            toCognito.accountId = accountId
            toCognito.customerId = customerId

            // commit create cognito user pass in new accountId and customerId Additional paramaters should be comming from the origional creante new accoutn/register function

            await dispatch('CREATE_COGNITO_USER', toCognito)
            // await dispatch('CART_STORE/MERGE_CART', toCognito.customerId, { root: true })
            commit('SET_RESPONSE', {
              success: true,
            })
          }
          if (
            errors &&
            errors.code &&
            errors.code === 'DuplicateField' &&
            errors.field === 'email'
          ) {
            commit('SET_RESPONSE', {
              error: true,
              emailDuplicate: true,
              message: errors.message,
            })
          }
        })
        .catch((e) => {
          commit('SET_FEEDBACK', e)
        })
    } catch (e) {
      commit('SET_FEEDBACK', e)
    } finally {
      commit('SET_PROCESSING', false)
    }
  },
}
// mutations
const mutations = {
  TOGGLE_QUICK_REGISTER: (state) => {
    state.IS_ACTIVE = !state.IS_ACTIVE
  },
  SET_PROCESSING(state, payload) {
    state.FEEDBACK = null
    state.API_CALL_IN_PROGRESS = payload
  },
  SET_FEEDBACK(state, payload) {
    state.FEEDBACK = payload
  },
  SET_RESPONSE(state, payload) {
    state.REGISTRATION_RESPONSE = payload
  },
}
export default {
  namespaced: true,
  state,
  actions,
  mutations,
}
