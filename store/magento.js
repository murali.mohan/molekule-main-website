import { getCookie } from 'tiny-cookie';

function locationStore(locale) {
	if (locale === 'ca-en') {
		return {
			name: 'MLK CA - Store',
			currencyCode: 'CAD',
			currencySymbol: 'CA$',
			id: 4,
			urlPrefix: 'ca-en/',
			cartHashKey: 'CA-CartHash',
			store: 'CA',
		};
	} else {
		return {
			name: 'MLK US - Store',
			currencyCode: 'USD',
			currencySymbol: '$',
			id: 1,
			urlPrefix: '',
			cartHashKey: 'US-CartHash',
			store: 'US',
		};
	}
}

export const state = () => ({
	products: {},
	productsTime: -1,
	cartIsLoading: false,
	cartHash: '',
	cartMine: null,
	cart: null,
	couponCodeError: '',
	upsells: [],
	initialFetchCartPromise: null,
	giftCheckboxStorageName: 'sidecart-gift-option',
	isGiftPurchase: false,
});

export const actions = {
	async INITIALIZE_SERVER({ rootState, getters, commit }) {
		const { store } = getters.locationStore;
		commit('SET_PRODUCTS', {
			...(await this.$axios.$get(`/custom-apis/magento/${store}/products`)),
			specialPrices: rootState.globalData.magento.specialPrices,
		});
	},

	async INITIALIZE_CLIENT({ commit, state, dispatch, getters }, { query }) {
		const { cartHashKey, store } = getters.locationStore;
		const cartHash = localStorage.getItem(cartHashKey);
		if (cartHash) {
			if (query.promo) {
				localStorage.removeItem(cartHashKey);
			} else {
				commit('SET_CART_HASH', cartHash);
			}
		}

		// this.$axios
		// 	.get(`/custom-apis/magento/${store}/upsells`)
		// 	.then(({ data: { upsells = [] } }) => {
		// 		upsells
		// 			.filter((upsell) => upsell.discount < 0)
		// 			.forEach((upsell) => {
		// 				commit('SET_UPSELL', upsell);
		// 			});
		// 	})
		// 	.catch((e) => {
		// 		// eslint-disable-next-line no-console
		// 		console.error(e);
		// 		this.$sentry.captureException(e);
		// 	});

		const initialFetchCartPromise = dispatch('FETCH_CART');

		commit('SET_INITIAL_FETCH_CART_PROMISE', initialFetchCartPromise);

		await state.initialFetchCartPromise;

		const cartHasGiftOrderAttribute = getters.cart && (getters.cart.giftOrder || getters.cart.giftOrder === false);
		if (cartHasGiftOrderAttribute) {
			commit('SET_IS_GIFT_PURCHASE', getters.cart.giftOrder);
			localStorage.removeItem(state.giftCheckboxStorageName);
		} else if (localStorage.getItem(state.giftCheckboxStorageName)) {
			commit('SET_IS_GIFT_PURCHASE', localStorage.getItem(state.giftCheckboxStorageName) === 'true');
		}

		if (!query.atc) {
			const atcLinkTiming = localStorage.getItem('atc_link_timing');
			if (atcLinkTiming) {
				// Clear cart if expirationDate is in the past & the cart hasn't been altered
				const { expirationDate, cartUpdatedAt } = JSON.parse(atcLinkTiming);
				await state.initialFetchCartPromise;

				if (new Date(expirationDate) <= new Date() && cartUpdatedAt === getters.cart.updatedAt) {
					dispatch('CLEAR_CART');
				}

				if (new Date(expirationDate) <= new Date() || cartUpdatedAt !== getters.cart.updatedAt) {
					localStorage.removeItem('atc_link_timing');
				}
			}
		}
	},

	async FETCH_CART({ state, getters, commit }) {
		if (state.cartIsLoading) return;
		commit('SET_CART_LOADING', true);

		let cart = null;

		const { cartMine, cartHash } = state;
		const { cartHashKey } = getters.locationStore;

		const fetchCart = async (prefix, options = {}) => {
			let c;
			const getCart = () => this.$axiosMagento.$get(prefix, options);
			if (cartMine === null && prefix.includes('/mine')) {
				// on the first load don't send all three requests, just one first
				try {
					c = await getCart();
				} catch (e) {
					if (e.response && e.response.status === 404) {
						// create mine cart if user is logged in (error is not 401) and it doesn't exist
						await this.$axiosMagento.$post('/carts/mine');
						c = await getCart();
					} else {
						throw e;
					}
				}
			}

			return Promise.all([
				c ? Promise.resolve(c) : getCart(),
				this.$axiosMagento.$get(`${prefix}/items`, options),
				this.$axiosMagento.$get(`${prefix}/totals`, options),
			]).then(([cart, items, totals]) => ({ cart, items, totals }));
		};

		const removeCartHash = () => {
			commit('SET_CART_HASH', '');
			localStorage.removeItem(cartHashKey);
		};

		if (cartMine || cartMine !== null) {
			try {
				cart = await fetchCart('/carts/mine');
			} catch (e) {
				commit('SET_CART_MINE', false);
			}
		}

		if (cart) {
			commit('SET_CART_MINE', true);
			removeCartHash();
		} else if (cartHash) {
			try {
				cart = await fetchCart(`/guest-carts/${cartHash}`);
			} catch (e) {
				removeCartHash();
			}
		}

		if (cart) {
			commit('SET_CART', cart);
		}

		commit('SET_CART_LOADING', false);

		return cart;
	},

	async ADD_TO_CART(
		{ rootState, state, getters, commit, dispatch },
		{ sku, qty = 1, items = [], addToCartSource = '', promoCode }
	) {
		if (items.length === 0) {
			items.push({ sku, qty });
		}

		const cartItems = items.map(({ sku, qty }) => {
			const product = state.products[sku];
			if (!product) throw new Error(`Product not found: ${sku}`);
			return { sku, qty, product_option: product.productOption };
		});

		const { cartHash, cartMine, cart: currentCart } = state;
		const { cartHashKey, store } = getters.locationStore;
		const { cartHashCreate } = process.env.magento;

		const start = performance.now();
		window.$nuxt.$loading.start();

		let res;
		let headers;

		let couponCode =
			promoCode || (this.app.$isNotFirstPageLoad ? getCookie('_promo_code') : this.app.router.currentRoute.query.promo);

		if (!couponCode || couponCode === 'undefined') {
			couponCode = undefined;
		}
		if (couponCode) {
			const { autoApplyCouponCodes } = rootState.globalData.magento;
			const hasMatchingProduct = cartItems.some((item) => autoApplyCouponCodes[item.sku]);

			if (!hasMatchingProduct) {
				couponCode = undefined;
			}
		}
		let couponCodeError = false;

		try {
			if (cartMine) {
				// this should ge in sync with code in /custom-apis/magento/items-post.js
				let lastItem;

				for (const item of cartItems) {
					lastItem = await this.$axiosMagento.$post(`/carts/mine/items`, {
						cartItem: {
							...item,
							quote_id: String(currentCart.cart.id),
						},
					});
				}

				await this.$axiosMagento.$put(`/carts/mine/items/${lastItem.item_id}`, {
					cartItem: {
						qty: lastItem.qty,
						quote_id: lastItem.quote_id,
					},
				});

				if (couponCode) {
					const couponCodeEncoded = encodeURIComponent(couponCode);
					await this.$axiosMagento.$put(`/carts/mine/coupons/${couponCodeEncoded}`).catch((e) => {
						couponCodeError = true;
					});
				}

				const cart = await dispatch('FETCH_CART');

				res = { cart };
			} else {
				({ data: res, headers } = await this.$axios.post(
					`/custom-apis/magento/${store}/cart/${cartHash || cartHashCreate}/items`,
					{ cartItems, couponCode },
					{ progress: false }
				));

				if (res.couponCodeError !== undefined) {
					couponCodeError = res.couponCodeError;
				}

				commit('SET_CART', res.cart);
			}
		} catch (e) {
			window.$nuxt.$loading.fail();
			window.$nuxt.$loading.finish();

			// here we show only errors coming from our api
			if (e && e.config && e.config.baseURL === '/' && e.response && e.response.data && e.response.data.message) {
				alert(e.response.data.message);
			}

			throw e;
		}

		commit(
			'SET_COUPON_CODE_ERROR',
			couponCodeError ? rootState.globalData.magento.autoApplyCouponCodesErrorMessage : ''
		);

		window.$nuxt.$loading.finish();

		const time = performance.now() - start;

		const cartCreated = res.cartHash && res.cartHash !== cartHash;
		const cartCreatedNotBackground = Boolean(
			cartCreated && headers && headers['server-timing'].includes('"post /guest-carts"')
		);
		if (cartCreated) {
			try {
				localStorage.setItem(cartHashKey, res.cartHash);
			} catch (e) {
				// ignore errors to make sure cart works even when saving cart failed
			}

			commit('SET_CART_HASH', res.cartHash);
		}

		const defaultTealiumData = { eventName: 'cart_add', eventAction: 'Added to Cart' };
		const tealiumData =
			{
				'': defaultTealiumData,
				banner: {
					eventName: 'cart_add_banner',
					eventAction: 'Added to Cart Banner',
					eventLabel: 'Added to Cart Banner',
				},
				topnav: {
					eventName: 'cart_add_topnav',
					eventAction: 'Added to Cart Topnav',
				},
				compare: {
					eventName: 'cart_add_compare',
					eventAction: 'Added to Cart Compare',
				},
				sidecart: {
					eventName: 'cart_add_sidecart',
					eventAction: 'Added from Sidecart Upsell',
					eventLabel: 'Added to Cart Banner',
				},
			}[addToCartSource || ''] || defaultTealiumData;

		this.$tealiumEvent(tealiumData.eventName, {
			event_category: 'Ecommerce',
			event_action: tealiumData.eventAction,
			event_label: tealiumData.eventLabel || `Added from ${this.app.router.currentRoute.path}`,
			event_value: cartItems.map(({ sku }) => sku),
			productsData: cartItems.map(({ sku, qty }) => ({ sku, qty })),
		});

		this.$tealiumWait.then(() => {
			if (window.gtag) {
				const timingLabel = (res.cartPreCreated
					? ['pre-created', couponCode && 'coupon code']
					: [
							cartCreatedNotBackground && 'cart created',
							`${cartItems.length} item${cartItems.length !== 1 ? 's' : ''}`,
							couponCode && 'coupon code',
					  ]
				)
					.filter(Boolean)
					.join(', ');

				if (res.customApisServerTime) {
					window.gtag('event', 'timing_complete', {
						event_category: 'FE Magento Integration',
						name: 'Add to cart server',
						value: res.customApisServerTime,
						event_label: timingLabel,
					});
				}

				window.gtag('event', 'timing_complete', {
					event_category: 'FE Magento Integration',
					name: 'Add to cart client',
					value: time,
					event_label: timingLabel,
				});
			}
		});

		return res.cart;
	},

	async REMOVE_FROM_CART({ getters, dispatch }, { itemId }) {
		const magentoRequests = [() => this.$axiosMagento.$delete(`${getters.cartRequestPath}/items/${itemId}`)];

		const itemWarranties = getters.cart.items.filter(
			(item) => item.associatedProductSku === getters.cart.items.find((item) => item.item_id === itemId).sku
		);
		if (itemWarranties.length) {
			magentoRequests.push(
				...itemWarranties.map((item) => () =>
					this.$axiosMagento.$delete(`${getters.cartRequestPath}/items/${item.item_id}`)
				)
			);
		}

		await Promise.all(magentoRequests.map((request) => request()));

		return dispatch('FETCH_CART');
	},

	async CLEAR_CART({ getters, dispatch }) {
		const { cart, cartRequestPath } = getters;
		if (!(cart && cart.items && cart.items.length)) return;

		await Promise.all(cart.items.map((item) => this.$axiosMagento.$delete(`${cartRequestPath}/items/${item.item_id}`)));
		return dispatch('FETCH_CART');
	},

	async UPDATE_QUANTITY({ state, getters, dispatch }, { itemId, qty }) {
		const item = state.cart.items.find((item) => item.item_id === itemId);
		if (!item) return;
		const requestObj = {
			cartItem: {
				qty,
				quote_id: item.quote_id,
			},
		};

		const magentoRequests = [() => this.$axiosMagento.$put(`${getters.cartRequestPath}/items/${itemId}`, requestObj)];

		if (!item.isWarranty && qty < item.qty) {
			const itemWarranties = getters.cart.items
				.filter((warrantyItem) => warrantyItem.associatedProductSku === item.sku)
				.filter((warrantyItem) => warrantyItem.qty >= item.qty);
			if (itemWarranties.length) {
				magentoRequests.push(
					...itemWarranties.map((warrantyItem) => () =>
						this.$axiosMagento.$put(`${getters.cartRequestPath}/items/${warrantyItem.item_id}`, {
							cartItem: { qty, quote_id: warrantyItem.quote_id },
						})
					)
				);
			}
		}

		await Promise.all(magentoRequests.map((request) => request()));

		return dispatch('FETCH_CART');
	},

	async UPDATE_GIFT_ATTRIBUTE({ getters }, bool) {
		return await this.$axiosMagento.$post(`${getters.cartRequestPath}/gift`, { gift_order: bool });
	},
};

export const mutations = {
	SET_PRODUCTS(state, { products, productsTime, specialPrices }) {
		const prod = {};
		Object.entries(products).forEach(([sku, product]) => {
			prod[sku] = {
				...product,
				price: specialPrices[product.sku] || product.price,
				priceRegular: product.price,
			};
		});
		state.products = prod;
		state.productsTime = productsTime;
	},

	SET_UPSELL(state, upsell) {
		state.upsells = [...state.upsells, upsell];
	},

	SET_CART_HASH(state, cartHash) {
		state.cartHash = cartHash;
	},

	SET_CART_MINE(state, cartMine) {
		state.cartMine = cartMine;
	},

	SET_CART_LOADING(state, isLoading) {
		state.cartIsLoading = isLoading;
	},

	SET_CART(state, cart) {
		state.cart = cart;
	},

	SET_COUPON_CODE_ERROR(state, errorMessage) {
		state.couponCodeError = errorMessage;
	},

	SET_INITIAL_FETCH_CART_PROMISE(state, promise) {
		state.initialFetchCartPromise = promise;
	},

	SET_IS_GIFT_PURCHASE(state, bool) {
		state.isGiftPurchase = bool;
	},
};

export const getters = {
	locationStore(_, __, rootState) {
		return locationStore(rootState.locale);
	},

	currencySymbol(_, getters) {
		return getters.locationStore.currencySymbol;
	},

	cartRequestPath({ cartMine, cartHash }) {
		return cartMine ? '/carts/mine' : `/guest-carts/${cartHash}`;
	},

	skusWithWarranties({ products }) {
		return [
			...new Set(
				Object.entries(products)
					.filter(([sku, value]) => value.type === 'warranty')
					.map(([sku, value]) => value.associatedProductSku)
			),
		];
	},

	cart({ cart, products, cartHash, cartMine }, { locationStore }, { globalData }) {
		if (!cart) return null;

		const { cartDescriptions, cartSubscriptionInfo } = globalData.magento;
		const rowTotals = {};

		cart.totals.items.forEach(({ item_id: itemId, row_total: rowTotal }) => {
			rowTotals[itemId] = rowTotal;
		});

		const items = cart.items.map((item) => {
			const product = products[item.sku] || {};
			return {
				...item,
				associatedProductSku: product.associatedProductSku,
				image: product.image,
				hasSubscription: product.hasSubscription,
				description: cartDescriptions[item.sku],
				subscriptionInfo: cartSubscriptionInfo[item.sku],
				price: rowTotals[item.item_id] || item.price,
				productId: product.id,
				productPrice: product.price,
				isWarranty: product.type === 'warranty',
				maxQuantity:
					product.type === 'warranty'
						? (() => {
								const foundAssociatedItem = cart.items.find((item) => item.sku === product.associatedProductSku);
								return (foundAssociatedItem || {}).qty;
						  })()
						: null,
			};
		});

		let sortedItems = [];
		const warranties = items.filter(({ sku }) => products[sku].type === 'warranty');

		for (let i = 0; i < items.length; i++) {
			const itemProduct = products[items[i].sku];

			if (itemProduct.type !== 'warranty') {
				const associatedWarrantyIndex = warranties.findIndex(
					({ associatedProductSku }) => associatedProductSku === itemProduct.sku
				);
				sortedItems.push([items[i], ...(associatedWarrantyIndex > -1 ? [warranties[associatedWarrantyIndex]] : [])]);
			}
		}

		sortedItems = sortedItems.reduce((final, item) => [...final, ...item], []);

		return {
			id: cart.cart && cart.cart.id,
			hash: cartHash,
			connectUrl: `${process.env.magento.pathname}${locationStore.urlPrefix}checkout${
				cartMine ? '' : `/cart/connect?cartId=${cartHash}`
			}`,
			giftOrder: cart.cart && cart.cart.extension_attributes && cart.cart.extension_attributes.gift_order,
			items: sortedItems,
			totals: cart.totals.total_segments.reduce((obj, { code, value }) => {
				obj[code] = value;
				return obj;
			}, {}),
			couponCode: cart.totals.coupon_code,
			itemsQty: sortedItems.length,
			updatedAt: cart.cart && cart.cart.updated_at,
		};
	},
};
