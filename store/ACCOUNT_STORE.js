const apiVersion = 0;
// initial state
const state = () => {
  return {
    API_CALL_IN_PROGRESS: false,
    FEEDBACK: null,
    SERVICE_RESPONSE: {
      success: false,
      error: false
    }
  }
};
// actions
const actions = {
  async API_COMPNAY_INFO_UPDATE({
    commit
  }, payload) {
    if (state.API_CALL_IN_PROGRESS) return commit('SET_FEEDBACK', 'The Last Api Message hasnt finished yet');
    //Pick API Specific params from payload.
    const {
      reg_ci_no_office_employees: numberOfEmployees,
      reg_ci_no_office_spaces: numberOfOfficeSpaces,
      reg_ci_opt_tax_exemption: taxExempt,
      files: taxCertificate,
    } = payload;
    //Construct API Request param object
    const apiParams = {
      numberOfEmployees,
      numberOfOfficeSpaces,
      taxExempt,
      taxCertificateList: [],
      accountId: this.state['USER_STORE']['ACCOUNTID'],
      customerId: this.state['USER_STORE']['CUSTOMERID']
    };
    await makeApiCall(commit, this.$repositories.user.update_company_info, apiParams, {
      success: true
    }, {
      error: true
    })
  },

  async API_SHIPPING_INFO_UPDATE({
    commit
  }, payload) {
    if (state.API_CALL_IN_PROGRESS) return commit('SET_FEEDBACK', 'The Last Api Message hasnt finished yet');

    let requestPayload = {};
    requestPayload['shippingAddress'] = this.$utility.makeAddressModel(payload['shipping_address'], 'shipping_address_');
    const {
      prefered_shipping_address: preferredShippingAddress,
      use_own_carrier: myCarrierInfo
    } = payload;
    requestPayload['primaryShipping'] = preferredShippingAddress;
    if (myCarrierInfo) {
      const {
        carrier_name: carrierName,
        carrier_account_number: carrierAccountNumber,
        delivery_type: deliveryType
      } = payload;
      requestPayload['carriers'] = [
        {
          'billingAddress': {},
          carrierName:carrierName,
          carrierAccountNumber:carrierAccountNumber,
          type:deliveryType,
          defaultCarrier:false
        },
      ]
      requestPayload['carriers'][0]['billingAddress'] = this.$utility.makeAddressModel(payload['carrier_billing_address'], 'carrier_billing_address_');
    }
    requestPayload = {
      accountId: this.state['USER_STORE']['ACCOUNTID'],
      customerId: this.state['USER_STORE']['CUSTOMERID'],
      ...requestPayload
    }
    // delete requestPayload['phoneNumber'];
    await makeApiCall(commit, this.$repositories.user.update_shipping_choices, requestPayload, {
      success: true
    }, {
      error: true
    })
  },

  async API_PAYMENT_INFO_UPDATE({
    commit,
    state
  }, payload) {
    if (state.API_CALL_IN_PROGRESS) return commit('SET_FEEDBACK', 'The Last Api Message hasnt finished yet');
    let {
      billing_address_as_shipping: billingAddressAsShipping,
      payment_preference: paymentPreferences,
      shippingAddress
    } = payload;
    let requestPayload = { };
    let continueServiceCall = true;
    let nonProfitFiles = [];

    switch (paymentPreferences) {
      case 'CREDITCARD':
        commit('SET_PROCESSING', true)
        const intentCallResponse = await this.$repositories.checkout.setupIntent({
          paymentKeyOnly: true,
          customerId: this.state['USER_STORE']['CUSTOMERID']
        });
        const clientSecret = (intentCallResponse.data && intentCallResponse.data.clientSecret) || '';
        const paymentIntentId = await getPaymentIntent(clientSecret);
        commit('SET_PROCESSING', false)
        requestPayload['paymentToken'] = '';
        if (paymentIntentId) {
          requestPayload['paymentToken'] = paymentIntentId;
        } else {
            this.dispatch('STRIPE_STORE/CARD_VALIDATION', {error: true});
            continueServiceCall = false;
            commit('SET_RESPONSE', {})
        }
        break;
      case 'PAYMENTTERMS':
        if(this.state['USER_STORE']['PAYMENT_TERMS_ID']) {
          console.log('INFO: paymentTerms already submitted')
          return;
        }
        let {
          business_type: businessType
        } = payload;
        switch (businessType) {
          case 'PUBLIC':
            const {
              stock_exchange: stockExchange, stock_ticker_name: stockTickerName
            } = payload;
            requestPayload = {
              ...requestPayload,
              stockExchange,
              stockTickerName
            }
            break;
          case 'GOVERNMENT':
            const {
              government_email: governmentEmail, department_phone_number: deptPhoneNumber
            } = payload;
            requestPayload['govtVerification'] = {
              governmentEmail,
              deptPhoneNumber
            };
            break;
          case 'NONPROFIT':
            let {
              files
            } = payload;
            const fileLocation = await this.$utility.taxFileUpload(this, files, this.state['USER_STORE']['CUSTOMERID'])
            nonProfitFiles = (fileLocation.length && fileLocation) || [];
            requestPayload = {
              files
            };
            requestPayload['documentProof'] = nonProfitFiles;
            break;
          case 'PRIVATE':
            requestPayload['bankInformation'] = this.$utility.makeAddressModel(payload['private_type_bank_address'], 'private_type_bank_');
            requestPayload['businessReferences1'] = this.$utility.makeAddressModel(payload['business_reference_one'], 'business_reference_one_', true)
            requestPayload['businessReferences2'] = this.$utility.makeAddressModel(payload['business_reference_two'], 'business_reference_two_', true)
            break;
        }
        requestPayload = {
          ...requestPayload,
          businessType
        }
        break;
      case 'ACH':
        if(this.state['USER_STORE']['PAYMENT_ACH_ID']) {
          requestPayload['paymentId'] = this.state['USER_STORE']['PAYMENT_ACH_ID'];
        }
        break;
    }
    requestPayload = {...requestPayload, paymentPreferences};
 
    if(billingAddressAsShipping) {
      requestPayload['shippingAsBillingAddress'] = true;
      requestPayload['billingAddress'] = shippingAddress; 
    } else {
      requestPayload['shippingAsBillingAddress'] = false
      requestPayload['billingAddress'] = this.$utility.makeAddressModel(payload['new_ba_address'], 'new_ba_');
    }
    requestPayload['defaultPayment'] = true;

    if (!continueServiceCall) {
      console.log('BREAK: continueServiceCall')
      return;
    }
    requestPayload = {
      accountId: this.state['USER_STORE']['ACCOUNTID'],
      customerId: this.state['USER_STORE']['CUSTOMERID'],
      ...requestPayload
    }
    await makeApiCall(commit, this.$repositories.user.update_payment_option, requestPayload, { success: true }, { error: true })
  }
}
// mutations
const mutations = {
  SET_PROCESSING(state, payload) {
    state.FEEDBACK = null;
    state.API_CALL_IN_PROGRESS = payload;
  },
  SET_FEEDBACK(state, payload) {
    state.FEEDBACK = payload
  },
  SET_RESPONSE(state, payload) {
    state.SERVICE_RESPONSE = payload;
  }
};
export default
  {
    namespaced: true,
    state,
    actions,
    mutations
  };


  // //Move to common 
// const makeAddressModel = (addressDetails = {}, prefix, emailPhone = false) => {
//   let address = {};

//   if (addressDetails[prefix + 'name']) {
//     address['name'] = addressDetails[prefix + 'name'] || ''
//   } else {
//     address['firstName'] = addressDetails[prefix + 'first_name'] || ''
//     address['lastName'] = addressDetails[prefix + 'last_name'] || ''
//   }

//   if (emailPhone) {
//     address['email'] = addressDetails[prefix + 'email'] || ''
//     address['phoneNumber'] = addressDetails[prefix + 'phone_number'] || ''
//     return address;
//   }
//   if(addressDetails[prefix+ 'company_name']) {
//     address['companyName'] = addressDetails[prefix + 'company_name'] || ''
//   }
//   if(addressDetails[prefix+ 'phone_number']) {
//     address['phoneNumber'] = addressDetails[prefix + 'phone_number'] || ''
//   }
//   address['streetAddress1'] = addressDetails[prefix + 'address_1'] || ''
//   address['streetAddress2'] = addressDetails[prefix + 'address_2'] || ''
//   address['city'] = addressDetails[prefix + 'city'] || ''
//   address['state'] = addressDetails[prefix + 'state'] || ''
//   address['postalCode'] = addressDetails[prefix + 'postal_code'] || ''
//   address['country'] = addressDetails[prefix + 'country'] || ''
//   return address;
// }

const makeApiCall = async (commit, service, params, successResponse, errorResponse) => {
  try {
    //SET TO LOADING
    commit('SET_PROCESSING', true)
    commit('SET_RESPONSE', {})

    await service(params)
      .then(res => {
        commit('SET_RESPONSE', {...successResponse, ...res.data})
      }).catch(e => {
        console.log(e)
        commit('SET_RESPONSE', errorResponse)
        // commit('SET_RESPONSE', successResponse)
        commit('SET_FEEDBACK', e)
        console.log('Getting Error - Skipping till Login is implemented')
      })
  } catch (e) {
    commit('SET_FEEDBACK', e)
  } finally {
    commit('SET_PROCESSING', false)
  }
}
