import GlobalHeaderMenuDropdown from './global-header-menu-dropdown.vue';

describe('@components/GlobalHeaderMenuDropdown', () => {
	it('exports a valid component', () => {
		expect(GlobalHeaderMenuDropdown).toBeAComponent();
	});
});
