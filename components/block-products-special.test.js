import BlockProductsSpecial from './block-products-special.vue';

describe('@components/BlockProductsSpecial', () => {
	it('exports a valid component', () => {
		expect(BlockProductsSpecial).toBeAComponent();
	});
});
