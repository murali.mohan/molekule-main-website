import BlockAnimatedGraphics from './block-animated-graphics.vue';

describe('@components/BlockAnimatedGraphics', () => {
	it('exports a valid component', () => {
		expect(BlockAnimatedGraphics).toBeAComponent();
	});
});
