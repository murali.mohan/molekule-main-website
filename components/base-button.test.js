import BaseButton from './base-button.vue';

describe('@components/BaseButton', () => {
	it('exports a valid component', () => {
		expect(BaseButton).toBeAComponent();
	});
});
