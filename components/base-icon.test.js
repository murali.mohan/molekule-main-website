import BaseIcon from './base-icon.vue';

describe('@components/BaseIcon', () => {
	it('exports a valid component', () => {
		expect(BaseIcon).toBeAComponent();
	});
});
