import FormSpecRenderer from './form-spec-renderer';

describe('@components/FormSpecRenderer', () => {
	it('exports a valid component', () => {
		expect(FormSpecRenderer).toBeAComponent();
	});
});
