import BlockAlert from './block-alert.vue';

describe('@components/BlockAlert', () => {
	it('exports a valid component', () => {
		expect(BlockAlert).toBeAComponent();
	});
});
