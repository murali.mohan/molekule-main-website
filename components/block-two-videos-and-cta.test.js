import BlockTwoVideosAndCta from './block-two-videos-and-cta.vue';

describe('@components/BlockTwoVideosAndCta', () => {
	it('exports a valid component', () => {
		expect(BlockTwoVideosAndCta).toBeAComponent();
	});
});
