<template>
	<div
		v-if="hasImages"
		:class="[
			$style.image,
			{
				[$style.fallback]: fallback,
				[$style.contain]: contain,
				[$style.absolute]: absolute,
				[$style.autoSize]: autoSize,
			},
		]"
	>
		<div v-if="useSizer" :class="$style.sizer" :style="sizerStyles"></div>
		<img
			v-if="fallback && fallbackImage && !autoSize"
			:data-bg="fallbackImage.url"
			:src="onePxGif"
			:class="['lazyload', $style.img]"
			:alt="fallbackImage.alt"
		/>
		<img
			v-else-if="fallback && fallbackImage && autoSize"
			:data-src="fallbackImage.url"
			:class="['lazyload', $style.img]"
			:alt="fallbackImage.alt"
		/>
		<picture v-if="!fallback">
			<source
				v-for="(source, index) in sources"
				:key="source.breakpoint"
				:media="imagesMedia[index]"
				:srcset="criticalConst ? imagesSrcset[index] || source.url : onePxGif"
				:data-srcset="criticalConst ? null : imagesSrcset[index] || source.url"
				:sizes="sizesNormalized"
			/>
			<img
				:class="['lazyload', $style.img]"
				:alt="imgImage.alt"
				:src="criticalConst ? setUrlPath(imgImage.url) : onePxGif"
				:srcset="criticalConst && imgImage.srcset ? imgImage.srcset : null"
				:data-src="criticalConst ? null : setUrlPath(imgImage.url)"
				:data-srcset="criticalConst ? null : imgImage.srcset || null"
				:sizes="sizesNormalized"
				:width="imgImage.width"
				:height="imgImage.height"
			/>
		</picture>
		<noscript v-if="!fallback && !criticalConst" v-html="noscriptInner" />
		<GlobalEvents v-if="watchSize" target="window" @resize="debouncedResize" />
	</div>
</template>

<script>
import debounce from 'lodash/debounce';

const onePxGif = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';

// https://stackoverflow.com/a/9756789
const escapeAttr = (str) =>
	('' + str)
		.replace(/&/g, '&amp;')
		.replace(/'/g, '&apos;')
		.replace(/"/g, '&quot;')
		.replace(/</g, '&lt;')
		.replace(/>/g, '&gt;');

const breakpoints = {
	default: 0,
	small: 480,
	medium: 768,
	large: 1024,
	xlarge: 1200,
	xxlarge: 1440,
};

const cleanUrls = {};
function cleanUrl(url) {
	if (cleanUrls[url]) {
		return cleanUrls[url];
	}
	let clean = url;
	let paramsArr;
	if (url.includes('?')) {
		const [urlBase, ...urlRest] = url.split('?');
		clean = urlBase;
		const params = {};
		urlRest
			.join('?')
			.split('&')
			.forEach((param) => {
				const paramParts = param.split('=');
				params[paramParts[0]] = paramParts[1];
			});
		delete params.w;
		delete params.h;
		delete params.auto;
		paramsArr = Object.entries(params).map(([k, v]) => `${k}=${v}`);
	}

	const retArr = [clean, paramsArr || []];
	cleanUrls[url] = retArr;

	return retArr;
}

const compressionPresets = {
	high: { q: 45, cs: 'strip' },
	medium: { q: 60 },
	low: { q: 75 },
};

let observer = null;
const observerMap = new WeakMap();

function observerCallback(entries) {
	entries.forEach(({ isIntersecting, target }) => {
		if (!isIntersecting) return;
		observer.unobserve(target);
		observerMap.get(target).intersects();
	});
}

export default {
	props: {
		// if first image has no url next image will be used automatically at 0 breakpoint
		image: {
			type: [Object, String],
			default: null,
		},

		images: {
			type: Object,
			default: () => ({}),
		},

		dev: {
			type: String,
			default: '',
		},

		// position absloutely (fit to parent)
		fit: {
			type: Boolean,
			default: false,
		},

		critical: {
			type: Boolean,
			default: false,
		},

		contain: {
			type: Boolean,
			default: false,
		},

		aspect: {
			type: Number,
			default: 0,
		},

		alt: {
			type: String,
			default: '',
		},

		sizes: {
			type: String,
			default: '',
		},

		compressionLevel: {
			type: String,
			default: 'low',
			validator: (v) => ['high', 'medium', 'low'].includes(v),
		},

		params: {
			type: Object,
			default: () => ({}),
		},
	},

	data() {
		return {
			fallback: false,
			windowWidth: 0,
			elementWidth: 0,
			elementHeight: 0,
			debouncedResize: debounce(this.handleResize, 100),
			criticalConst: this.critical,
			preloaded: false,
		};
	},

	computed: {
		onePxGif: () => onePxGif,

		normalizedImages() {
			const { alt } = this;
			const images = {
				default: this.image,
				...this.images,
			};

			const entries = Object.entries(images)
				.map(([breakpoint, image]) => {
					if (typeof image === 'string') {
						image = { url: image };
					} else if (image === null) {
						image = { url: onePxGif, width: 0, height: 0 };
					}

					const sizeUndefined = image.height === undefined || image.width === undefined;
					const sizeZero = image.height === 0 || image.width === 0;

					return {
						breakpoint: breakpoints[breakpoint],
						breakpointName: breakpoint,
						url: image.url,
						srcset: undefined,
						width: image.width,
						height: image.height,
						alt: image.alt || alt || '',
						aspectCss: !sizeUndefined ? `${sizeZero ? 0 : ((image.height / image.width) * 100).toFixed(4)}%` : '',
					};
				})
				.filter(({ breakpoint, url }) => typeof breakpoint === 'number' && url)
				.sort((a, b) => a.breakpoint - b.breakpoint);

			return entries;
		},

		useSizer() {
			return Boolean(!this.fit && (this.aspect || !this.hasUnknownSize));
		},

		autoSize() {
			return Boolean(!this.fit && !this.aspect && this.hasUnknownSize);
		},

		absolute() {
			return Boolean(this.fit);
		},

		additionalParams() {
			return Object.entries({ ...compressionPresets[this.compressionLevel], ...this.params }).map(
				([key, val]) => `${key}=${val}`
			);
		},

		imagesSrcset() {
			const { criticalConst } = this;

			return this.normalizedImages.map((image) => {
				const { url } = image;
				if (!url.startsWith('https://images.prismic.io/')) {
					return undefined;
				}

				const isJPG = url.includes('.jpg') || url.includes('.jpeg');

				if (!(isJPG || url.includes('.png') || url.includes('.gif'))) {
					return undefined;
				}

				const cleanedUrl = cleanUrl(url);
				const paramsInitial = cleanedUrl[1];
				let newUrl = cleanedUrl[0];

				newUrl = newUrl.replace('https://images.prismic.io/', '/prismic-images/');

				const params = [...paramsInitial, ...this.additionalParams];
				const auto = [];
				if (isJPG && criticalConst) {
					params.push('fm=pjpg');
				} else {
					auto.push('format');
				}

				params.push(`auto=${auto.join(',')}`);

				return `${newUrl}?${params.join('&')}`;
			});
		},

		imagesMedia() {
			return this.normalizedImages
				.map((image, index, arr) => (arr[index + 1] ? `(max-width: ${arr[index + 1].breakpoint - 0.02}px)` : ''))
				.slice(0, -1);
		},

		sizerStyles() {
			const { imgImage, sources, normalizedImages, fallback, aspect } = this;

			if (aspect) {
				const aspectCss = `${((1 / aspect) * 100).toFixed(4)}%`;
				return { paddingBottom: aspectCss, '--padding-bottom-default': aspectCss };
			} else if (fallback) {
				return { paddingBottom: this.fallbackImage && this.fallbackImage.aspectCss };
			} else if (!imgImage.aspectCss) {
				return {};
			}

			const styles = { paddingBottom: imgImage.aspectCss, '--padding-bottom-default': imgImage.aspectCss };

			sources.forEach((source, index) => {
				styles[`--padding-bottom-below-${normalizedImages[index + 1].breakpointName}`] = source.aspectCss;
			});

			return styles;
		},

		hasImages() {
			return this.normalizedImages.length > 0;
		},

		hasUnknownSize() {
			return !this.sizerStyles.paddingBottom;
		},

		imgImage() {
			const { normalizedImages } = this;
			const index = normalizedImages.length - 1;
			return {
				...normalizedImages[index],
				srcset: this.imagesSrcset[index],
			};
		},

		sources() {
			return this.normalizedImages.slice(0, -1).reverse();
		},

		sizesNormalized() {
			const { sizes } = this;

			if (this.fit || this.aspect) {
				const { elementWidth, elementHeight, fallbackImageBase, contain } = this;
				const { image } = fallbackImageBase || {};

				if (elementWidth && elementHeight && image && image.width && image.height) {
					const imageRatio = image.width / image.height;
					const displayRatio = elementWidth / elementHeight;

					if ((!contain && displayRatio < imageRatio) || (contain && displayRatio > imageRatio)) {
						const width = elementWidth * (imageRatio / displayRatio);
						return `${width}px`;
					}
				}
			}

			if (sizes) return sizes;

			return undefined;
		},

		noscriptInner() {
			const { sources, imgImage, imagesMedia, $style, imagesSrcset, sizesNormalized } = this;
			const sizesEscaped = escapeAttr(sizesNormalized || '');

			const sourcesHtml = sources.map(
				(source, index) => /* HTML */ `
					<source
						media="${imagesMedia[index]}"
						srcset="${escapeAttr(imagesSrcset[index] || source.url)}"
						sizes="${sizesEscaped}"
					/>
				`
			);
			return /* HTML */ `
				<picture>
					${sourcesHtml.join('')}
					<img
						class="${$style.img}"
						alt="${escapeAttr(imgImage.alt || '')}"
						src="${escapeAttr(imgImage.url)}"
						srcset="${escapeAttr(imgImage.srcset || imgImage.url)}"
						sizes="${sizesEscaped}"
					/>
				</picture>
			`;
		},

		fallbackImageBase() {
			const { normalizedImages, windowWidth } = this;
			let image = normalizedImages[0];
			let imageIndex = 0;

			for (let i = normalizedImages.length - 1; i >= 0; i -= 1) {
				if (windowWidth >= normalizedImages[i].breakpoint) {
					imageIndex = i;
					image = normalizedImages[i];
					break;
				}
			}

			return { image, imageIndex };
		},

		fallbackImage() {
			const {
				fallbackImageBase: { image, imageIndex },
				imagesSrcset,
				elementWidth,
			} = this;
			const srcset = imagesSrcset[imageIndex];

			if (!srcset) return image;

			if (!elementWidth) {
				return {
					...image,
					url: onePxGif,
				};
			}

			const urls = srcset.split(', ').map((el) => {
				const [url, width] = el.trim().split(' ');
				return [url, parseInt(width)];
			});

			const url = (urls.find(([, width]) => width > elementWidth) || urls[urls.length - 1])[0];

			return {
				...image,
				url: url || image.url,
			};
		},

		criticalConstOrPreloaded() {
			const { criticalConst, preloaded } = this;
			if (criticalConst) return criticalConst;
			if (preloaded) return preloaded;
			return false;
		},

		watchSize() {
			return this.fallback || this.fit || this.aspect;
		},
	},

	beforeCreate() {
		if ((process.client || process.env.STORYBOOK_ENV) && !observer) {
			observer = new IntersectionObserver(observerCallback, { rootMargin: '1000px' });
		}
	},

	mounted() {
		this.fallback = !(window.CSS && window.CSS.supports);
		// this.fallback = true;

		this.handleResize();

		if (this.aspect && this.fit) {
			throw new Error('BaseImage: aspect and fit props cannot be used at the same time');
		}
	},

	beforeDestroy() {
		if (observer && this.$el && this.el instanceof Element) {
			observer.unobserve(this.$el);
		}
	},

	methods: {
		handleResize() {
			this.windowWidth = window.innerWidth;

			if (this.watchSize && this.$el && this.$el instanceof Element) {
				const { offsetWidth, offsetHeight } = this.$el;
				if (offsetWidth) {
					this.elementWidth = offsetWidth;
					this.elementHeight = offsetHeight;
				} else {
					observer.observe(this.$el);
					observerMap.set(this.$el, this);
				}
			}
		},

		setUrlPath(url) {
			const clearedUrl = cleanUrl(url);
			const imgParamsInitial = clearedUrl[1];
			// TODO: Let's set this as an env var so that we can reference it instead since this is used other places.
			if (!url.startsWith('https://images.prismic.io/')) {
				return url;
			}

			let newUrl = clearedUrl[0];
			newUrl = newUrl.replace('https://images.prismic.io/', '/prismic-images/');

			const imgParams = [...imgParamsInitial, ...this.additionalParams];

			return `${newUrl}?${imgParams.join('&')}`;
		},

		intersects() {
			this.handleResize();
		},
	},
};
</script>

<style lang="scss" module>
@import '@design';

@function reverse($list, $recursive: false) {
	$result: ();

	@for $i from length($list) * -1 through -1 {
		@if type-of(nth($list, abs($i))) == list and $recursive {
			$result: append($result, reverse(nth($list, abs($i)), $recursive));
		} @else {
			$result: append($result, nth($list, abs($i)));
		}
	}

	@return $result;
}

.image {
	position: relative;
	width: 100%;
	overflow: hidden;

	&.absolute {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
	}
}

.sizer {
	.image:not(.fallback) & {
		--padding-bottom-below-xxlarge2: var(--padding-bottom-below-xxlarge, var(--padding-bottom-default));
		--padding-bottom-below-xlarge2: var(--padding-bottom-below-xlarge, var(--padding-bottom-below-xxlarge2));
		--padding-bottom-below-large2: var(--padding-bottom-below-large, var(--padding-bottom-below-xlarge2));
		--padding-bottom-below-medium2: var(--padding-bottom-below-medium, var(--padding-bottom-below-large2));
		--padding-bottom-below-small2: var(--padding-bottom-below-small, var(--padding-bottom-below-medium2));

		@each $breakpoint in reverse(map-keys($breakpoints)) {
			@include below($breakpoint) {
				padding-bottom: var(--padding-bottom-below-#{$breakpoint}2) !important;
			}
		}
	}
}

.img {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	object-fit: cover;

	.image.autoSize & {
		position: relative;
		height: auto;
	}

	.image.fallback & {
		background-repeat: no-repeat;
		background-position: center;
		background-size: cover;
	}

	.image.contain & {
		object-fit: contain;
		background-size: contain;
	}
}
</style>
