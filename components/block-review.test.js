import BlockReview from './block-review.vue';

describe('@components/BlockReview', () => {
	it('exports a valid component', () => {
		expect(BlockReview).toBeAComponent();
	});
});
