import BlockCaptionCopy from './block-caption-copy.vue';

describe('@components/BlockCaptionCopy', () => {
	it('exports a valid component', () => {
		expect(BlockCaptionCopy).toBeAComponent();
	});
});
