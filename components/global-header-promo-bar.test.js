import GlobalHeaderPromoBar from './global-header-promo-bar.vue';

describe('@components/GlobalHeaderPromoBar', () => {
	it('exports a valid component', () => {
		expect(GlobalHeaderPromoBar).toBeAComponent();
	});
});
