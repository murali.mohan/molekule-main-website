import MoleculeCarouselPagination from './molecule-carousel-pagination.vue';

describe('@components/MoleculeCarouselPagination', () => {
	it('exports a valid component', () => {
		expect(MoleculeCarouselPagination).toBeAComponent();
	});
});
