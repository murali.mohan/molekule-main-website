import GlobalFooter from './global-footer.vue';

describe('@components/GlobalFooter', () => {
	it('exports a valid component', () => {
		expect(GlobalFooter).toBeAComponent();
	});
});
