import MoleculeAutoplayVideoPlayPause from './molecule-autoplay-video-play-pause.vue';

describe('@components/MoleculeAutoplayVideoPlayPause', () => {
	it('exports a valid component', () => {
		expect(MoleculeAutoplayVideoPlayPause).toBeAComponent();
	});
});
