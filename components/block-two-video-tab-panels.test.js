import BlockTwoVideoTabPanels from './block-two-video-tab-panels.vue';

describe('@components/BlockTwoVideoTabPanels', () => {
	it('exports a valid component', () => {
		expect(BlockTwoVideoTabPanels).toBeAComponent();
	});
});
