import MoleculeReviewSnippet from './molecule-review-snippet.vue';

describe('@components/MoleculeReviewSnippet', () => {
	it('exports a valid component', () => {
		expect(MoleculeReviewSnippet).toBeAComponent();
	});
});
