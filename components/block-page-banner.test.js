import BlockPageBanner from './block-page-banner.vue';

describe('@components/BlockPageBanner', () => {
	it('exports a valid component', () => {
		expect(BlockPageBanner).toBeAComponent();
	});
});
