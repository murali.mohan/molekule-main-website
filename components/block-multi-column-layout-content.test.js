import BlockMultiColumnLayoutContent from './block-multi-column-layout-content.vue';

describe('@components/BlockMultiColumnLayoutContent', () => {
	it('exports a valid component', () => {
		expect(BlockMultiColumnLayoutContent).toBeAComponent();
	});
});
