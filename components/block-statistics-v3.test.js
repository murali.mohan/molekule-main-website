import BlockStatisticsV3 from './block-statistics-v3.vue';

describe('@components/BlockStatisticsV3', () => {
	it('exports a valid component', () => {
		expect(BlockStatisticsV3).toBeAComponent();
	});
});
