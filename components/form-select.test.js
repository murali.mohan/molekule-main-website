import FormSelect from './form-select.vue';

describe('@components/FormSelect', () => {
	it('exports a valid component', () => {
		expect(FormSelect).toBeAComponent();
	});
});
