import BaseImage from './base-image.vue';

describe('@components/BaseImage', () => {
	it('exports a valid component', () => {
		expect(BaseImage).toBeAComponent();
	});
});
