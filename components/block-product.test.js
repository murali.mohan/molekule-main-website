import BlockProduct from './block-product.vue';

describe('@components/BlockProduct', () => {
	it('exports a valid component', () => {
		expect(BlockProduct).toBeAComponent();
	});
});
