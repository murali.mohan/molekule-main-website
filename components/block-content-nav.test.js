import BlockContentNav from './block-content-nav.vue';

describe('@components/BlockContentNav', () => {
	it('exports a valid component', () => {
		expect(BlockContentNav).toBeAComponent();
	});
});
