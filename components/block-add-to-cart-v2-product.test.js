import BlockAddToCartV2Product from './block-add-to-cart-v2-product.vue';

describe('@components/BlockAddToCartV2Product', () => {
	it('exports a valid component', () => {
		expect(BlockAddToCartV2Product).toBeAComponent();
	});
});
