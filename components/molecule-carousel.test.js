import MoleculeCarousel from './molecule-carousel.vue';

describe('@components/MoleculeCarousel', () => {
	it('exports a valid component', () => {
		expect(MoleculeCarousel).toBeAComponent();
	});
});
