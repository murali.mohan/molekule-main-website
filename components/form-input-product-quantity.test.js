import FormInputProductQuantity from './form-input-product-quantity.vue';

describe('@components/FormInputProductQuantity', () => {
	it('exports a valid component', () => {
		expect(FormInputProductQuantity).toBeAComponent();
	});
});
