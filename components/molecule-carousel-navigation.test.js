import MoleculeCarouselNavigation from './molecule-carousel-navigation.vue';

describe('@components/MoleculeCarouselNavigation', () => {
	it('exports a valid component', () => {
		expect(MoleculeCarouselNavigation).toBeAComponent();
	});
});
