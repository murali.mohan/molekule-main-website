import BlockFeatureVariations from './block-feature-variations.vue';

describe('@components/BlockFeatureVariations', () => {
	it('exports a valid component', () => {
		expect(BlockFeatureVariations).toBeAComponent();
	});
});
