import AtomReviewStars from './atom-review-stars.vue';

describe('@components/AtomReviewStars', () => {
	it('exports a valid component', () => {
		expect(AtomReviewStars).toBeAComponent();
	});
});
