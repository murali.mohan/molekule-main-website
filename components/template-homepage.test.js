import TemplateHomepage from './template-homepage';

describe('@components/TemplateHomepage', () => {
	it('exports a valid component', () => {
		expect(TemplateHomepage).toBeAComponent();
	});
});
