import MoleculeCarouselPlayPause from './molecule-carousel-play-pause.vue';

describe('@components/MoleculeCarouselPlayPause', () => {
	it('exports a valid component', () => {
		expect(MoleculeCarouselPlayPause).toBeAComponent();
	});
});
