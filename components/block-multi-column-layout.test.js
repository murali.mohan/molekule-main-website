import BlockMultiColumnLayout from './block-multi-column-layout.vue';

describe('@components/BlockMultiColumnLayout', () => {
	it('exports a valid component', () => {
		expect(BlockMultiColumnLayout).toBeAComponent();
	});
});
