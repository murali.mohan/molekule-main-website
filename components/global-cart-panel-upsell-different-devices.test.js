import GlobalCartPanelUpsellDifferentDevices from './global-cart-panel-upsell-different-devices.vue';

describe('@components/GlobalCartPanelUpsellDifferentDevices', () => {
	it('exports a valid component', () => {
		expect(GlobalCartPanelUpsellDifferentDevices).toBeAComponent();
	});
});
