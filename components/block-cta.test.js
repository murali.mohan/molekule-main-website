import BlockCta from './block-cta.vue';

describe('@components/BlockCta', () => {
	it('exports a valid component', () => {
		expect(BlockCta).toBeAComponent();
	});
});
