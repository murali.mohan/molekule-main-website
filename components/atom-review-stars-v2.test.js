import AtomReviewStarsV2 from './atom-review-stars-v2.vue';

describe('@components/AtomReviewStarsV2', () => {
	it('exports a valid component', () => {
		expect(AtomReviewStarsV2).toBeAComponent();
	});
});
