import BlockVideo from './block-video.vue';

describe('@components/BlockVideo', () => {
	it('exports a valid component', () => {
		expect(BlockVideo).toBeAComponent();
	});
});
