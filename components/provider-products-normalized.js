//import { createdBuffersPromise } from 'custom-apis/magento/items-post';
import { mapState } from 'vuex';

const generateCtaLink = (ctaLink, sku) =>
	ctaLink && ctaLink.url
		? ctaLink
		: { url: `https://molekule.com/add-to-cart?skus=${sku}` };
	//	: { url: `https://molekule.com/add-to-cart?skus=${products.map((product) => product.sku).join(',')}` };

export default {
	props: {
		items: {
			type: Array,
			default: () => [],
		},

		isProductsGrouped: {
			type: Boolean,
			default: false,
		},
	},
	computed: {
		// ...mapState('magento', ['products']),
    ...mapState('PRODUCT_STORE', ['PRODUCTS']),
		...mapState({
			isUSWebsite: 'isUSWebsite',
			specialPrices: (state) => state.globalData.magento.specialPrices,
		}),
		productsNormalized() {
			const { isUSWebsite } = this;
			let $filterProduct =  this.items.map((item) => {
				const { product, productsFromPrismic, sku } = item;
				if (productsFromPrismic && productsFromPrismic.length) {
					const bundleProducts = productsFromPrismic.reduce((final, { multiplier, product }) => {
						multiplier = multiplier && multiplier > 0 ? multiplier : 1;

						for (let i = 0; i < multiplier; i++) {
							final.push(product);
						}

						return final;
					}, []);
					const listPrice = bundleProducts.reduce((acc, product) => acc + product.price, 0);
					const currentPrice = listPrice - (item.discount || 0);

					return {
						...item,
						cta_link: generateCtaLink(item.cta_link, sku),
						ctaLink: generateCtaLink(item.ctaLink, sku),
						enableAffirm: item.enableAffirm && isUSWebsite,
						productOrBundle: { isBundle: true, bundleProducts, listPrice, currentPrice },
					};
				}

				// Single with product from prismic
				if (product && Object.keys(product).length) {
					const currentPrice = (Number(this.specialPrices[product.sku]) || product.price) - (item.discount || 0);

					return {
						...item,
						cta_link: generateCtaLink(item.cta_link, sku),
						ctaLink: generateCtaLink(item.ctaLink, sku),
						enableAffirm: item.enableAffirm && isUSWebsite,
						productOrBundle: {
							isBundle: false,
							product,
							sku: product.sku,
							listPrice: product.price,
							currentPrice,
						},
					};
				}


				// No sku in Products Grouped
				if (this.isProductsGrouped) {
					if (!sku) {
						// TODO: Remove 'oldData' when products grouped is fully updated in Prismic
						const oldData = {
							...item,
							hasLabel: true,
							price: item.labelPrice || 0,
							priceRegular: item.labelPriceStrikethrough || 0,
						};

						return { ...item, enableAffirm: false, oldData };
					}
				}

				// No sku "regular"
				if (!sku) return { ...item, enableAffirm: false };
				const activeProduct = this.PRODUCTS.filter($obj =>{
					 if($obj.productBean.masterData.current.masterVariant.sku === sku){
						 return $obj
					 }
				})
				if(activeProduct[0] && activeProduct[0].productBean && activeProduct[0].productBean.masterData &&
					activeProduct[0].productBean.masterData.current &&
					activeProduct[0].productBean.masterData.current.masterVariant){
					const skusArray = (sku || '').split(',').map((sku) => sku.trim());
					const isBundle = activeProduct[0].productBean.productType.obj.name === 'MarketingBundle' //skusArray.length > 1;
					// Single
					const product = {
						sku: sku,
						"id": activeProduct[0].productBean.id,
						"type": "simple",
						"associatedProductSku": null,
						"name": item.name || item.titleDesktop,
						"image":  item.image || item.imageDesktop || item.imageMobile,
						"hasSubscription": activeProduct[0].hasSubscription,
						descriptionMobileTop:'',
						// discount: activeProduct[0].productBean.masterData.current.masterVariant.prices[0] &&
						// activeProduct[0].productBean.masterData.current.masterVariant.prices[0].discounted &&
						// activeProduct[0].productBean.masterData.current.masterVariant.prices[0].discounted.value ?
						// this.$options.filters.centsToDollars((activeProduct[0].productBean.masterData.current.masterVariant.prices[0].value &&
						// 	activeProduct[0].productBean.masterData.current.masterVariant.prices[0].value.centAmount) -
						// 	(activeProduct[0].productBean.masterData.current.masterVariant.prices[0].discounted.value.centAmount)) : 0,
						powerreviewsPageId: activeProduct[0].productBean.masterData.current.slug && activeProduct[0].productBean.masterData.current.slug['en-US'],
						ctaText: item.ctaText ? item.ctaText : 'Add to cart',
						ctaLink: {
							url: 'https://molekule.com/add-to-cart?skus='+sku,
						},
					}
					let listPrice = activeProduct ? this.$options.filters.centsToDollars(activeProduct[0].productBean.masterData.current.masterVariant.prices[0].value.centAmount) : undefined;
					let  currentPrice = activeProduct[0].productBean.masterData.current.masterVariant.prices[0].discounted ?
					this.$options.filters.centsToDollars(activeProduct[0].productBean.masterData.current.masterVariant.prices[0].discounted.value.centAmount) : undefined;
					if(currentPrice === undefined){
						currentPrice = listPrice
						listPrice = undefined
					}
					product.price = listPrice
					product.priceRegular = currentPrice
					product.image.label = product.name
					product.image.resized_width = product.image.width
					product.image.resized_height = product.image.height
					if (!isBundle) {
						//products[sku];
						let $discount = listPrice != undefined ? (listPrice - currentPrice).toFixed(2) : undefined
						let productOrBundle = { isBundle, product, sku, listPrice, currentPrice }
						let $json = {
							...item,
							productOrBundle,
							ctaText: item.ctaText ? item.ctaText : 'Add to cart',
							cta_link: generateCtaLink(item.cta_link, sku),
							ctaLink: generateCtaLink(item.ctaLink, sku),
							discount: $discount,
							enableAffirm: item.enableAffirm && isUSWebsite,
						//	productOrBundle: { isBundle, product, sku, listPrice, currentPrice },
						}
						return $json
					//	return $json;
					}

					// Bundle
					const bundleProducts = [product]
				// 	skusArray.map((sku) => PRODUCTS.filter($obj =>{
				// 		if($obj.productBean.masterData.current.masterVariant.sku === sku){
				// 			return $obj
				// 		}
				//  })
				//  );
					// const listPrice = bundleProducts.reduce((acc, product) => acc + product.priceRegular, 0);
					// const currentPrice = listPrice - (item.discount || 0);
					// const listPrice = activeProduct ? activeProduct[0].productBean.masterData.current.masterVariant.prices[0].value.centAmount : undefined;
					// const currentPrice = activeProduct ? activeProduct[0].productBean.masterData.current.masterVariant.prices[0].discounted &&
					// activeProduct[0].productBean.masterData.current.masterVariant.prices[0].discounted.value.centAmount : undefined;
					let productOrBundle = { isBundle, bundleProducts, sku, skusArray, listPrice, currentPrice }
					let $json = {
						...item,
						productOrBundle,
						ctaText: item.ctaText ? item.ctaText : 'Add to cart',
						cta_link: generateCtaLink(item.cta_link, sku),
						ctaLink: generateCtaLink(item.ctaLink, sku),
						enableAffirm: item.enableAffirm && isUSWebsite,
					//	productOrBundle: { isBundle, bundleProducts, sku, skusArray, listPrice, currentPrice },
					}
				return $json
				//	return $json;
				}

			}).filter(item => item !== undefined); ;
			return $filterProduct
		},

		hasAnyAvailableItems() {
			return this.productsNormalized.some(
				(item) =>
				item && item.productOrBundle &&  item.productOrBundle.isBundle
				// item && !item.productOrBundle ||
				// 	(item && item.productOrBundle.isBundle
				// 		? item.productOrBundle.bundleProducts.length > 0
				// 		: item.productOrBundle.product)
			);
		},
	},

	  render() {
		console.log('data', this.productsNormalized)
			return this.$scopedSlots.default({
				productsNormalized: this.productsNormalized,
				hasAnyAvailableItems: true //this.hasAnyAvailableItems,
			});
	},
};
