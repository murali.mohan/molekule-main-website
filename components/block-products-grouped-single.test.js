import BlockProductsGroupedSingle from './block-products-grouped-single.vue';

describe('@components/BlockProductsGroupedSingle', () => {
	it('exports a valid component', () => {
		expect(BlockProductsGroupedSingle).toBeAComponent();
	});
});
