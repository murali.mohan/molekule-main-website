import ProviderElementActivityTracker from './provider-element-activity-tracker.js';

describe('@components/ProviderElementActivityTracker', () => {
	it('exports a valid component', () => {
		expect(ProviderElementActivityTracker).toBeAComponent();
	});
});
