import GlobalPromoModal from './global-promo-modal.vue';

describe('@components/GlobalPromoModal', () => {
	it('exports a valid component', () => {
		expect(GlobalPromoModal).toBeAComponent();
	});
});
