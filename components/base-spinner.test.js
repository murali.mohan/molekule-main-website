import BaseSpinner from './base-spinner';

describe('@components/BaseSpinner', () => {
	it('exports a valid component', () => {
		expect(BaseSpinner).toBeAComponent();
	});
});
