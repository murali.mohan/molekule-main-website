import BaseButtonArrow from './base-button-arrow.vue';

describe('@components/BaseButtonArrow', () => {
	it('exports a valid component', () => {
		expect(BaseButtonArrow).toBeAComponent();
	});
});
