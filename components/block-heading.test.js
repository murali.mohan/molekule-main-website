import BlockHeading from './block-heading.vue';

describe('@components/BlockHeading', () => {
	it('exports a valid component', () => {
		expect(BlockHeading).toBeAComponent();
	});
});
