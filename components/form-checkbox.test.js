import FormCheckbox from './form-checkbox';

describe('@components/FormCheckbox', () => {
	it('exports a valid component', () => {
		expect(FormCheckbox).toBeAComponent();
	});
});
