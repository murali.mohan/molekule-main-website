import GlobalHeaderMenuDropdownMobile from './global-header-menu-dropdown-mobile.vue';

describe('@components/GlobalHeaderMenuDropdownMobile', () => {
	it('exports a valid component', () => {
		expect(GlobalHeaderMenuDropdownMobile).toBeAComponent();
	});
});
