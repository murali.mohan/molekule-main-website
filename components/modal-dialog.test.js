import AtomModalDialog from './atom-modal-dialog.vue';

describe('@components/AtomModalDialog', () => {
	it('exports a valid component', () => {
		expect(AtomModalDialog).toBeAComponent();
	});
});
