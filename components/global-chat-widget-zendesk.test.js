import GlobalChatWidgetZendesk from './global-chat-widget-zendesk.vue';

describe('@components/GlobalChatWidgetZendesk', () => {
	it('exports a valid component', () => {
		expect(GlobalChatWidgetZendesk).toBeAComponent();
	});
});
