import BlockContent from './block-content.vue';

describe('@components/BlockContent', () => {
	it('exports a valid component', () => {
		expect(BlockContent).toBeAComponent();
	});
});
