const Wrapper = {
	data: () => ({ isActive: false, isFocused: false, isHovered: false }),

	methods: {
		handleEnter(e) {
			if (!this.isActive) {
				this.isActive = true;

				if (e.type === 'focusin') {
					this.isFocused = true;
				}

				if (e.type === 'mouseover') {
					this.isHovered = true;
				}
			}
		},

		handleLeave(e) {
			if (!this.$el.contains(e.relatedTarget)) {
				this.isActive = false;

				if (e.type === 'focusout') {
					this.isFocused = false;
				}

				if (e.type === 'mouseleave') {
					this.isHovered = false;
				}
			}
		},

		addOrRemoveEventListeners(type) {
			this.$el[`${type}EventListener`]('focusin', this.handleEnter);
			this.$el[`${type}EventListener`]('focusout', this.handleLeave);
			this.$el[`${type}EventListener`]('mouseover', this.handleEnter);
			this.$el[`${type}EventListener`]('mouseleave', this.handleLeave);
		},
	},

	mounted() {
		this.addOrRemoveEventListeners('add');
		this.$parent.$once('hook:mounted', this.$forceUpdate);
	},

	beforeDestroy() {
		this.addOrRemoveEventListeners('remove');
	},

	render() {
		return this.$scopedSlots.default({
			isActive: this.isActive,
			isFocused: this.isFocused,
			isHovered: this.isHovered,
		});
	},
};

const EmptyWrapper = {
	render() {
		return this.$scopedSlots.default();
	},
};

export default {
	props: {
		isEnabled: {
			type: Boolean,
			default: true,
		},
	},

	render(createElement) {
		const vm = this;
		const config = this.isEnabled
			? {
					scopedSlots: {
						default(props) {
							return vm.$scopedSlots.default({
								isActive: props.isActive,
								isFocused: props.isFocused,
								isHovered: props.isHovered,
							});
						},
					},
			  }
			: {};
		const children = this.isEnabled ? [] : this.$scopedSlots.default();

		return createElement(this.isEnabled ? Wrapper : EmptyWrapper, config, children);
	},
};
