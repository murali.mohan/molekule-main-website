#!/bin/bash
set -eo pipefail

export SENTRY_RELEASE=$CIRCLE_SHA1

if [[ -n $SENTRY_RELEASE && -n $SENTRY_ORG && -n $SENTRY_PROJECT && -n $SENTRY_AUTH_TOKEN ]]; then
  echo "Upload Source Maps to Sentry"

  yarn sentry-cli releases new $SENTRY_RELEASE --finalize
  yarn sentry-cli releases files $SENTRY_RELEASE upload-sourcemaps --rewrite --url-prefix '~/_nuxt/' ./.nuxt/dist/client
  yarn sentry-cli releases files $SENTRY_RELEASE upload-sourcemaps --rewrite ./.nuxt/dist/server
  rm .nuxt/dist/client/*.map
else
  echo "Source Maps uploading to Sentry skipped"
fi

exit
