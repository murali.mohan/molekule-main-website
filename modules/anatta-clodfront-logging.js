export default function AnattaCloudfrontLoggingModule() {
	if (this.nuxt.anattaClearCloudfrontCache) {
		const original = this.nuxt.anattaClearCloudfrontCache;
		this.nuxt.anattaClearCloudfrontCache = (...args) => {
			try {
				console.log(`ACF: Requesting to clear CF cache with ${args[0] && args[0].length} paths`);
				console.log(args);

				const promise = original(...args);

				console.log('ACF: Request sent');

				promise
					.then((res) => {
						console.log(`ACF: Got response`);
						console.log(res);
					})
					.catch((e) => {
						console.error(e);
						if (process.sentry) {
							process.sentry.withScope((scope) => {
								scope.setExtra('Are paths array?', Array.isArray(args[0]));
								scope.setExtra('Paths length', args[0] && args[0].length);
								process.sentry.captureException(e);
							});
						}
					});

				return promise;
			} catch (e) {
				console.log('ACF: error when calling function');
				console.error(e);
			}
		};
	}
}
