let recaptchaPromise = null;

export function loadRecaptcha() {
	if (!recaptchaPromise) {
		recaptchaPromise = new Promise((resolve) => {
			window.recaptchaLoaded = () => {
				resolve(window.grecaptcha);
			};

			const script = document.createElement('script');
			script.src = 'https://www.google.com/recaptcha/api.js?onload=recaptchaLoaded&render=explicit';
			script.async = true;

			document.head.appendChild(script);
		});
	}

	return recaptchaPromise;
}
