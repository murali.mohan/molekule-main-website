let loadingPromise = null;

async function loadIframeAfterLoad(callback) {
	await loadingPromise;
	if (!window.SheerID) return;

	window.SheerID.load('iframe', '1.2', {
		config: {
			lightbox: true,
			mobileRedirect: false,
			mobileThreshold: 600,
			height: 'fluid',
			top: '25px',
			closeBtnRight: '25px',
		},
	});

	if (callback) callback();
}

export default function loadSheerId() {
	if (loadingPromise) {
		// we're loading the iframe even if it was already loaded to connect the event listener to new links
		loadIframeAfterLoad();
		return;
	}

	loadingPromise = new Promise((resolve) => {
		const script = document.createElement('script');
		script.src = 'https://services.sheerid.com/jsapi/SheerID.js';

		script.onload = () => {
			if (!window.SheerID) return;
			window.SheerID.setBaseUrl('https://services.sheerid.com/jsapi');
			resolve();
			loadIframeAfterLoad(() => {
				const verificationScript = document.createElement('script');
				verificationScript.src = 'https://cdn.jsdelivr.net/npm/@sheerid/jslib@1/sheerid.js';
				verificationScript.onload = () => {
					window.sheerId.setOptions({
						logLevel: 'info',
						restApi: {
							serviceUrl: 'https://services.sheerid.com/',
						},
						cookies: {
							enabled: true,
							secure: true, // Set to `true` in production when your page.
							domain: 'molekule.com',
						},
					});
					window.sheerId.conversion.listenForVerificationId();
				};
				document.head.appendChild(verificationScript);
			});
		};

		document.head.appendChild(script);
	});
}
