export default function extractAndAdjustVideoUrl(
	video,
	{ api = true, autoplay = true, useNocookieUrl = false, playerParams = '' } = {}
) {
	const isHtml = video.startsWith('<');
	let url = '';

	if (isHtml) {
		const el = document.createElement('div');
		el.innerHTML = video;
		const iframe = el.querySelector('iframe');
		if (!iframe) return '';
		url = iframe.src || '';
	} else {
		url = video;
	}

	if (!url) return url;

	const isYoutubeVideo = url.includes('youtube.com/embed');

	if (isYoutubeVideo) {
		if (autoplay && !url.includes('autoplay')) {
			url += `${url.includes('?') ? '&' : '?'}autoplay=1`;
		}

		if (api && !url.includes('enablejsapi')) {
			url += `${url.includes('?') ? '&' : '?'}enablejsapi=1`;
		}

		if (useNocookieUrl) {
			url = url.replace('youtube.com', 'youtube-nocookie.com');
		}

		if (playerParams) {
			url += `${url.includes('?') ? '&' : '?'}${playerParams}`;
		}
	}

	return { isYoutubeVideo, url };
}
