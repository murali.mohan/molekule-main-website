export const europeanCountries = [
	'BG',
	'CH',
	'HR',
	'DK',
	'AT',
	'BE',
	'CY',
	'CZ',
	'EE',
	'FI',
	'FR',
	'DE',
	'GR',
	'IE',
	'IT',
	'LV',
	'LT',
	'LU',
	'MT',
	'NL',
	'PT',
	'SK',
	'SI',
	'ES',
	'HU',
	'NO',
	'PL',
	'RO',
	'SE',
];

export const europeanLocale = 'gr-en';

export const europeanPrismicLocale = 'en-gr';

const europeanLocales = () => {
	const euCountries = {};
	europeanCountries.forEach((code) => {
		euCountries[code] = europeanLocale;
	});
	return euCountries;
};

export const alpha2ToDefaultLocale = {
	US: 'en-us',
	CA: 'ca-en',
	IN: 'in-en',
	JP: 'jp-ja',
	KR: 'kr-ko',
	GB: 'gb-en',
	EU: 'eu-en',
	...europeanLocales(), // locale lookup by country code, append European countries in the same format above
};

export const supportedCountryAlpha2Values = ['US', 'CA', 'IN', 'JP', 'KR', 'GB', 'EU', ...europeanCountries];

export const otherLocales = ['ca-en', 'in-en', 'jp-ja', 'kr-ko', 'gb-en', 'eu-en', 'gr-en'];

export const countryDomains = ['US', 'IN', 'GB'];

export const defaultLocaleToAlpha2 = Object.fromEntries(
	Object.entries(alpha2ToDefaultLocale).map(([alpha2, locale]) => {
		if (locale !== europeanLocale) {
			return [locale, alpha2];
		} else return ['en-us', 'US']; // Temp fix to skip country lookup by gr-en locale.
	})
);

export const locales = Object.values(alpha2ToDefaultLocale);

export const getLocaleMatchInRoutePath = (path) => {
	const routeMatch = path.match(/^\/([a-z]{2}-[a-z]{2})(\/|$)/);
	const localeMatchInRoute =
		Array.isArray(routeMatch) && routeMatch[1] && Object.values(alpha2ToDefaultLocale).includes(routeMatch[1])
			? routeMatch[1]
			: '';

	return localeMatchInRoute;
};
