import { asArray } from '@/utils';

const getCommonMeta = (prefix, { title, description, openGraphImage, twitterImage }) => {
	const img = prefix === 'twitter' ? twitterImage : openGraphImage;
	let [image, imageAlt] = ['', ''];
	if (img && img.url) {
		image = img.url;
		const [maxW, maxH] = prefix === 'twitter' ? [4096, 4096] : [1200, 1200];
		image += `${image.includes('?') ? '&' : '?'}max-w=${maxW}&max-h=${maxH}&fit=crop`;
		imageAlt = img.alt || '';
	}

	const keyName = prefix === 'twitter' ? 'name' : 'property';
	return [
		title && { hid: `${prefix}:title`, [keyName]: `${prefix}:title`, content: title },
		description && {
			hid: `${prefix}:description`,
			[keyName]: `${prefix}:description`,
			content: description,
		},
		image && { hid: `${prefix}:image`, [keyName]: `${prefix}:image`, content: image },
		image && imageAlt && { hid: `${prefix}:image:alt`, [keyName]: `${prefix}:image:alt`, content: imageAlt },
	].filter(Boolean);
};

const getAdditionalMetaTags = (additionalMetaTags) => {
	return additionalMetaTags
		.filter(({ property, value }) => property && value)
		.map(({ property, value }) => {
			const keyName = property.startsWith('og:') ? 'property' : 'name';

			return {
				hid: property,
				[keyName]: property,
				content: value,
			};
		});
};

const getLinkedData = (linkedData, $sentry) => {
	const getValue = (value) => {
		if (!value) return value;
		const values = value.split('\n');
		return values.length > 1 ? values : values[0];
	};

	const createNestedObject = (keys, key, value, currentObj, depth = 0) => {
		if (keys.length - 1 <= depth) {
			return { [keys[depth]]: { ...currentObj[keys[depth]], [key]: value } };
		}

		return depth
			? {
					[keys[depth]]: {
						...currentObj[keys[depth]],
						...createNestedObject(keys, key, value, currentObj[keys[depth]] || {}, depth + 1),
					},
			  }
			: {
					...currentObj[keys[depth]],
					...createNestedObject(keys, key, value, currentObj[keys[depth]] || {}, depth + 1),
			  };
	};

	return linkedData
		.map((slice) => {
			if (slice.props.jsonLd) {
				try {
					return { type: 'application/ld+json', json: JSON.parse(slice.props.jsonLd) };
				} catch (e) {
					console.error(e);
					$sentry.captureException(e);
				}
			}

			const items = asArray(slice.props.items);

			if (!(items.length && items.some((item) => item.key && item.value))) {
				return null;
			}

			return {
				type: 'application/ld+json',
				json: items.reduce((final, item) => {
					const { key, value, parent_key: parentKey } = item;

					if (parentKey && key && value) {
						if (parentKey.includes('->')) {
							const parentKeys = parentKey
								.split('->')
								.map((key) => key.trim())
								.filter(Boolean);

							if (parentKeys.length) {
								final[parentKeys[0]] = createNestedObject(parentKeys, key, value, final);
							}
						} else {
							final[parentKey] = final[parentKey]
								? { ...final[parentKey], [key]: getValue(value) }
								: { [key]: getValue(value) };
						}
					} else if (key && value) {
						final[key] = getValue(value);
					}

					return final;
				}, {}),
			};
		})
		.filter(Boolean);
};

export default (
	{
		title,
		description,
		path,
		openGraphImage,
		twitterImage,
		type = 'website',
		canonicalPath,
		additionalMetaTags,
		slices,
	},
	$sentry
) => {
	additionalMetaTags = asArray(additionalMetaTags);
	slices = Array.isArray(slices) ? slices : [];
	const commonProps = {
		title,
		description,
		openGraphImage,
		twitterImage,
	};
	const meta = [];
	const scripts = [];
	const linkedData = slices.filter((slice) => slice.sliceType === 'LinkedData');

	// Add meta tags
	if (additionalMetaTags && additionalMetaTags.length) meta.push(...getAdditionalMetaTags(additionalMetaTags));

	const [ogLocale, ogLocaleAlternate] = (path &&
		(path.startsWith('/ca-en/') ? ['en_CA', 'en_US'] : ['en_US', 'en_CA'])) || ['', ''];

	const openGraph = [
		// Required properties: og:title, og:type, og:image, og:url
		...getCommonMeta('og', commonProps),
		{ hid: 'og:type', property: 'og:type', content: type },
		path && { hid: 'og:url', property: 'og:url', content: `https://molekule.com${path.replace(/\/$/, '')}` },
		ogLocale && { hid: 'og:locale', property: 'og:locale', content: ogLocale },
		ogLocaleAlternate && { hid: 'og:locale:alterenate', property: 'og:locale:alternate', content: ogLocaleAlternate },
	].filter(Boolean);

	meta.push(...openGraph, ...getCommonMeta('twitter', commonProps));

	if (description) meta.push({ hid: 'description', name: 'description', content: description });

	// Add scripts/linked data
	if (linkedData && linkedData.length) scripts.push(...getLinkedData(linkedData, $sentry));

	const link = [];
	if (canonicalPath) {
		link.push({
			hid: 'canonical',
			rel: 'canonical',
			href: canonicalPath,
		});
	}

	return {
		title,
		meta,
		script: scripts,
		link,
	};
};
