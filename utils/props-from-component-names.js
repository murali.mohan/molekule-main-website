export default {
	BlockVideo: (slice) => ({
		id: slice.props.id,
		layout: slice.props.layout,
		heading: slice.props.heading,
		headingColor: slice.props.headingColor,
		headingColorDesktop: slice.props.headingColorDesktop,
		headingDesktop: slice.props.headingDesktop,
		headingDesktopAnimated: slice.props.headingDesktopAnimated,
		headingFadeOutEnabled: slice.props.headingFadeOutEnabled,
		headingFadeOutDelay: slice.props.headingFadeOutDelay,
		copyColor: slice.props.copyColor,
		copyColorDesktop: slice.props.copyColorDesktop,
		description: slice.props.description,
		videoBackgroundMobile: slice.props.videoBackgroundMobile,
		videoBackground: slice.props.videoBackground,
		videoAspectRatioMobile: slice.props.videoAspectRatioMobile,
		videoAspectRatioDesktop: slice.props.videoAspectRatioDesktop,
		videoModal: slice.props.videoModal,
		imageBackground: slice.props.imageBackground,
		playButtonPositionOnDesktop: slice.props.playButtonPositionOnDesktop,
		playButtonText: slice.props.playButtonText,
		playButtonTextColor: slice.props.playButtonTextColor,
		playButtonTextVariant: slice.props.playButtonTextVariant,
		playButtonLink: slice.props.playButtonLink,
		useNocookieUrl: slice.props.useNocookieUrl,
		playerParameters: slice.props.playerParameters,
		embedVideo: slice.props.embedVideo,
		embedVideoBackground: slice.props.embedVideoBackground,
		embedVideoBackgroundMobile: slice.props.embedVideoBackgroundMobile,
		placeholderImage: slice.props.placeholderImage,
	}),

	BlockFaq: (slice) => ({
		id: slice.props.id,
		heading: slice.props.heading,
		questions: slice.props.items,
	}),

	BlockTimeline: (slice) => ({
		id: slice.props.id,
		timeline: slice.props.items,
		heading: slice.props.heading,
	}),

	BlockProductBanner: (slice) => ({
		id: slice.props.id,
		items: slice.props.items,
		transitionTime: slice.props.transitionTime,
	}),

	BlockReview: (slice) => ({
		id: slice.props.id,
		imageMobile: slice.props.imageMobile,
		imageDesktop: slice.props.imageDesktop,
		productName: slice.props.productName,
		powerReviewsPageId: slice.props.powerreviewsPageId,
		reviewsLink: slice.props.reviewsLink,
		productInfoColor: slice.props.productInfoColor,
		quote: slice.props.quote,
		quoteAuthor: slice.props.quoteAuthor,
		quoteColor: slice.props.quoteColor,
		quoteSize: slice.props.quoteSize,
		ctaLink: slice.props.ctaLink,
		ctaText: slice.props.ctaText,
		ctaColor: slice.props.ctaColor,
		ctaVariant: slice.props.ctaVariant,
		backgroundColor: slice.props.backgroundColor,
		reviewProduct: slice.props.reviewOrProduct,
	}),

	BlockAwards: (slice) => ({
		id: slice.props.id,
		heading: slice.props.heading,
		headingColor: slice.props.headingColor,
		copyColor: slice.props.copyColor,
		awards: slice.props.items,
		backgroundColor: slice.props.backgroundColor,
		maxWidthSingleMobile: slice.props.maxWidthSingleMobile,
		maxWidthSingleDesktop: slice.props.maxWidthSingleDesktop,
		maxWidthContainer: slice.props.maxWidthContainer,
		hasExtraPadding: slice.props.hasExtraPadding,
	}),

	BlockTestimonials: (slice) => ({
		id: slice.props.id,
		heading: slice.props.heading,
		headingColor: slice.props.headingColor,
		copyColor: slice.props.copyColor,
		featured: slice.props,
		testimonials: slice.props.items,
	}),

	BlockTwoVideosAndCTA: (slice) => ({
		id: slice.props.id,
		headingColor: slice.props.headingColor,
		copyColor: slice.props.copyColor,
		videos: slice.props.items,
		mobileAspectRatio: slice.props.mobileAspectRatio,
		desktopAspectRatio: slice.props.desktopAspectRatio,
		description: slice.props.description,
		ctaText: slice.props.ctaText,
		ctaLink: slice.props.ctaLink,
	}),

	BlockReports: (slice) => ({
		id: slice.props.id,
		content: slice.props.content,
		ctaText: slice.props.ctaText,
		ctaLink: slice.props.ctaLink,
		files: slice.props.items,
		reportsMarginLeft: slice.props.reportsMarginLeft,
		reportsMarginRight: slice.props.reportsMarginRight,
		ctaVariant: slice.props.ctaVariant,
	}),

	BlockDownloads: (slice, { track }) => ({
		items: slice.props.items,
		vueOn: track ? { click: track } : null,
	}),

	BlockListing: (slice, { pageId, reviewsListing, reviewsSortBy, sortByChanged, loadMore }) => ({
		id: slice.props.id,
		heading: slice.props.heading,
		pageId,
		reviews: reviewsListing,
		sortBy: reviewsSortBy,
		vueOn: {
			sortByChanged: sortByChanged || null,
			loadMore: loadMore || null,
		},
	}),

	BlockSocialReviews: (slice) => ({
		id: slice.props.id,
		heading: slice.props.heading,
		buttonText: slice.props.buttonText,
		buttonLink: slice.props.buttonLink,
		shouldTrack: true,
		posts: slice.props.items,
	}),

	BlockFeaturesNeverHydrate: (slice) => ({
		showOnlyMiddleOnMobile: slice.props.showOnlyMiddleOnMobile !== `No`,
		features: slice.props.items,
		hidden: slice.props.hidden,
		id: slice.props.id,
	}),

	BlockBannerInfoNeverHydrate: (slice) => ({
		id: slice.props.id,
		heading: slice.props.heading,
		headingColor: slice.props.headingColor,
		copyColor: slice.props.copyColor,
		description: slice.props.description,
		backgroundColor: slice.props.backgroundColor,
		content: slice.props.content,
		contentSecondary: slice.props.contentSecondary,
		mobileImage: slice.props.mobileImage,
		desktopImage: slice.props.desktopImage,
		imageCaption: slice.props.imageCaption,
		ctaLink: slice.props.ctaLink,
		ctaText: slice.props.ctaText,
		layoutMobile: slice.props.layoutMobile,
		layoutDesktop: slice.props.layoutDesktop,
		imageSizeDesktop: slice.props.imageSizeDesktop,
		appButtons: slice.props.appButtons,
		video: slice.props.video,
		videoOverlay: slice.props.videoOverlay,
		useNocookieUrl: slice.props.useNocookieUrl,
		playerParameters: slice.props.playerParameters,
	}),

	BlockHeadingNeverHydrate: (slice) => ({
		id: slice.props.id,
		heading: slice.props.heading,
		headingLevel: slice.props.headingLevel,
		description: slice.props.description,
	}),

	BlockFoundersNeverHydrate: (slice) => ({
		id: slice.props.id,
		heading: slice.props.heading,
		description: slice.props.description,
		content: slice.props.content,
		imageMobile: slice.props.imageMobile,
		imageDesktop: slice.props.imageDesktop,
	}),

	BlockReviewsNeverHydrate: (slice, { productReviews, multipleProductReviews }) => {
		const reviews =
			(multipleProductReviews && multipleProductReviews[slice.props.powerreviewsPageId]) || productReviews;

		return {
			id: slice.props.id,
			reviews,
			powerreviewsPageId: (reviews && reviews.pageId) || '',
			usesAutoProps: true,
			renderCondition: Boolean(reviews && reviews.results.length),
		};
	},

	BlockSpecsNeverHydrate: (slice) => ({
		id: slice.props.id,
		heading: slice.props.heading,
		headingColor: slice.props.headingColor,
		copyColor: slice.props.copyColor,
		headingColorDesktop: slice.props.headingColorDesktop,
		copyColorDesktop: slice.props.copyColorDesktop,
		image: slice.props.image,
		items: slice.props.items,
	}),

	BlockContentNeverHydrate: (slice, { index }) => ({
		content: slice.props,
		index,
	}),

	BlockImagesGridNeverHydrate: (slice) => ({
		id: slice.props.id,
		images: slice.props.items,
		heading: slice.props.heading,
		description: slice.props.description,
	}),

	BlockHeaderBannerNeverHydrate: (slice) => ({
		id: slice.props.id,
		heading: slice.props.headerTitle,
		subheading: slice.props.subheading,
		backgroundColor: slice.props.backgroundColor,
		copyColor: slice.props.copyColor,
	}),
};
