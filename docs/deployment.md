# Deployment and Server Infrastructure

- [Server Infrastructure](#server-infrastructure)
  - [Lambda@Edge](#lambdaedge)
- [Deployment](#deployment)

## Server Infrastructure

CloudFront and Elastic Beanstalk are the primary building blocks of the Molekule site infrastructure.

- CloudFront is not just cache and proxy but plays other important roles:
  - Sends `/go/*` requests to Magento
  - Handles `/_nuxt/*` (static files) requests with Lambda@Edge and sends them to S3
  - Handles `/prismic-images/*` with Lambda@Edge
  - Handles `/storybook/index.html` and sends the request to S3
  - Has separate caching configuration for `/custom-apis/*` requests - it's caching based on all query parameters, not just whitelisted ones like default configuration.

### Lambda@Edge

We're using Lambda@Edge in two ways:

- For `/_nuxt/*` requests we're rewriting request path to send it to proper file (gzip, brotli compressed) based on headers.
- For `/prismic-images/*` we're using CloudFront as a proxy to Prismic/Imgix images to serve critical images without penalty of connecting to separate server.

Code for the functions is not in repository, it's set in AWS Console.

**Important Note!** AWS is creating executions logs for the functions in CloudWatch in regions where they are executed (all over the globe). It creates separate "Log Group" for each function. By default those Log Groups have no expiration date. In case you create new functions, the `/aws-log-retention.sh` script is in the repository and that looks for Log Groups without expiration and sets it to 30 days.

## Deployment

We have something called tag based deployment, which means our code doesn't get deployed every time we push a commit to a branch, but rather the code gets deployed every time we push a specific tag.

We have three environments:

1. [**Test**, **Dev Anatta**](https://dev-anatta.molekule.com/) - Primary testing/preview environment, connected to Test Magento (sometimes called also _m2dev_), using Sandbox PowerReviews and possibly some other "test"
2. [**Test Magento Anatta**, **Staging Anatta**](https://staging-anatta.molekule.com/) - Development environment
3. [**Production**](https://molekule.com/) - Do I need to say anything here?

To deploy something just push the tag using the appropriate command below:

```bash
# Test
git tag -d test; git tag -a test -m deploy && git push origin test --no-verify -f

# Test Magento Anatta
git tag -d test-magento-anatta; git tag -a test-magento-anatta -m deploy && git push origin test-magento-anatta --no-verify -f

# Production
git tag -d production; git tag -a production -m deploy && git push origin production --no-verify -f
```

This is how the command works:

1. `git tag -d test` Deletes an existing tag called test, this will give an error the first time you run it because the tag doesn't exist.
1. `git tag -a test -m deploy` Adds the tag and commits it
1. `git push origin test --no-verify -f` Force pushes the tag to replace it

In case you are having doubts what is the difference between branch and tag. Please go [here](https://hashnode.com/post/git-tag-vs-branch-how-do-you-explain-it-cju9lp37b000d25s1g1a7isce).

Once you push the tags, Circle CI has webhooks to GitHub, which inform Circle CI in case anything happens on that repo. Circle CI then checks your repo for config.yaml and ascertains whether to take any action or not? Like in our case we have mentioned that any push to a branch will be ignored.

Once it detects that a tag was pushed and it sees the name of the tag in our `config.yaml` CircleCI will start running the workflow.

A Circle CI workflow consists of jobs. A job consists of multiple steps.

We have one workflow (`build-and-test`) that has 2 jobs:

1. **`build-and-deploy`** - This is the most important jobs, it:
   1. Generates `.env` file based on mix of variables that are hard-coded in the repository (`/.circleci/export-env-var.sh`) and set in Circle CI.
   2. Installs dependencies
   3. Runs unit tests
   4. Builds nuxt app
   5. Builds Storybook
   6. Prepares environment for deployment
   7. Uploads source maps to Sentry
   8. Runs deployment (requires AWS credentials) that consist of:
      1. Setting ElasticBeanstalk configuration
      2. Create gzipped and brotli compressed static files and upload them to S3
      3. Install production dependencies
      4. Deploy to ElasticBeanstalk
      5. Create CloudFront Invalidation
      6. Wait for invalidation to finish (only test environments)
2. **`test-deployed-on-test`** - Runs only for test deployments, runs E2E tests on deployed site (note: as of writing this tests are quite unstable so don't be worried when they fail).
