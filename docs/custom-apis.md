# Custom APIs

- [Introduction](#introduction)
- [Development](#development)
- [Configuration](#configuration)
- [cachedRequest](#cachedrequest)
- [Endpoints](#endpoints)
  - [`/pages`](#pages)
  - [`/magento`](#magento)
  - [`/prismic-preview`](#prismic-preview)
  - [`/prismic-share`](#prismic-share)
  - [`/careers-postings`](#careers-postings)
  - [`/power-reviews`](#power-reviews)
  - [`/forms`](#forms)
  - [`/subscribe`](#subscribe)

## Introduction

Custom APIs is a [Koa](https://koajs.com) app with various endpoints that help the application with optimizations, logic abstraction, and reducing the amount of processing on the frontend/client.

## Development

Note that preloading for empty carts and popular configurations is non starting when dev server starts (either custom or bundled). It starts only after first request for given cart/product is made. To change it for testing you can set `isDev` in `custom-apis/magento/utils.js` to `false`.

## Configuration

The Koa app is added as serverMiddleware in `nuxt.config.js` on the `/custom-apis` path. All requests to `/custom-apis/*` are served by the Koa app which uses [`@koa/router`](https://github.com/koajs/router) to handle requests in different routes.

The Koa app is declared in [`custom-apis/server.js`](../custom-apis/server.js). There you can find all the router routes and general server configuration.

## cachedRequest

Cached Requests is a mechanism allowing for data loading and processing with cached responses. This uses [`lru-cache`](https://github.com/isaacs/node-lru-cache) and it's located at [`custom-apis/cached-request.js`](custom-apis/cached-request.js).

This is currently being used for [`/careers-postings`](#/careers-postings) and [`/power-reviews`](#/power-reviews).

## Endpoints

### `/pages`

Endpoint to fetch page data which mainly consists of Prismic data.

| Path                                             | Method | Description                 |
| ------------------------------------------------ | ------ | --------------------------- |
| /pages/:locale(en-us\|en-ca\|ja-jp)?/:type?/:uid | GET    | Content for a specific page |

**Parameters:**

| Parameter | Description                                                       |
| --------- | ----------------------------------------------------------------- |
| locale    | Locale of the page data. Current options: en-us, en-ca, and ja-jp |
| type      | The API ID of the document's custom type                          |
| uid\*     | unique identifier of the document                                 |

Simple example: /pages/about

Kitchen sink example: /pages/en-us/page/business----air-purifier-air-pro

\*Required

### `/magento`

Useful endpoints for Magento store data.

| Path                         | Method | Description                                            |
| ---------------------------- | ------ | ------------------------------------------------------ |
| /:store/products             | GET    | Get products available for the `:store` value provided |
| /:store/cart/:cartHash/items | POST   | Add items to the cart                                  |
| /:store/upsells              | GET    | Get the upsells data                                   |

**Parameters:**

| Parameter  | Description                                                              |
| ---------- | ------------------------------------------------------------------------ |
| store\*    | Locale for the Magento store. Only 'US' and 'CA' are currently supported |
| cartHash\* | The current cart's unique identifier                                     |

\*Required

### `/prismic-preview`

This endpoint is only meant to be used by Prismic.

| Path             | Method | Description                                                           |
| ---------------- | ------ | --------------------------------------------------------------------- |
| /prismic-preview | GET    | Sets a cookie for the preview script and redirects to the correct url |

### `/prismic-share`

This allows us to use Prismic public share links without having to add the Prismic preview script in all pages of the site.

| Path                 | Method | Description                                                                    |
| -------------------- | ------ | ------------------------------------------------------------------------------ |
| /prismic-share/(.\*) | GET    | Sets a cookie to add the preview script before passing control back to Prismic |

**Parameters:**

| Parameter | Description                                                                           |
| --------- | ------------------------------------------------------------------------------------- |
| (.\*)\*   | This value should be a Prismic public share link, such as `https://prismic.link/{id}` |

\*Required

### `/careers-postings`

Fetches careers postings from an external API and uses [cachedRequest](#cachedRequest) to optimize this process.

| Path              | Method | Description             |
| ----------------- | ------ | ----------------------- |
| /careers-postings | GET    | Fetches career postings |

### `/power-reviews`

Fetches reviews from PowerReviews and uses [cachedRequest](#cachedRequest) to optimize this process.

| Path                   | Method | Description     |
| ---------------------- | ------ | --------------- |
| /power-reviews/(.\*)\* | GET    | Fetches reviews |

**Parameters:**

| Parameter | Description |
| --- | --- |
| (.\*)\* | This value should be an endpoint for [PowerReviews's Read API](https://developers.powerreviews.com/Content/reference/read.html) |

\*Required

### `/forms`

| Path            | Method | Description                                                                 |
| --------------- | ------ | --------------------------------------------------------------------------- |
| /forms/purchase | POST   | Validates and posts purchase forms to Zendesk, a Google Sheet, and SendGrid |

**Parameters:**

| Parameter | Description                                    |
| --------- | ---------------------------------------------- |
| form\*    | A JSON object that contains purchase form data |

\*Required

### `/subscribe`

| Path       | Method | Description                                                                            |
| ---------- | ------ | -------------------------------------------------------------------------------------- |
| /subscribe | POST   | Subscribes users to Molekule's marketing emails using [Iterable](https://iterable.com) |

**Parameters:**

| Parameter | Description |
| --------- | ----------- |
| email\*   | An email    |

\*Required
