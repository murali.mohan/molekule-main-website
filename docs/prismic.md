# Prismic

- [Summary](#summary)
- [Implementation](#implementation)

## Summary

Our content management system is Prismic. You can read more about Prismic [here](https://prismic.io). Most of the pages on the site use Prismic for their content or at least for their SEO data.

## Implementation

We use two systems to implement Prismic on the frontend. First, we use the [`@anatta/prismic`](https://github.com/anattadesign/npm-prismic-nuxt#readme) npm package to add specific functionality for apps designed by Anatta on the Nuxt framework. The second system we use is the Custom APIs' [`/pages`](custom-apis#/pages) endpoint. This endpoint normalizes the data we receive from prismic and it also makes additional requests for data that a page may need, like reviews for example. This helps to reduce data processing on the client and it consolidates the requests for all the page data.
