# Storybook

- [Introduction](#introduction)
- [Configuration](#configuration)
- [MDX files](#mdx-files)
- [Development](#development)
- [Build & Deployment Process](#build-and-deployment-process)

## Introduction

We're using the Storybook tool for its ability to write documentation for components using [MDX](https://storybook.js.org/docs/vue/writing-docs/mdx). In our Storybook app we document the components that correspond to slices in Prismic.

## Configuration

All the configuration for Storybook is in the [`.storybook` folder](/.storybook). You can read more about configuring storybook by reading the [documentation](https://storybook.js.org/docs/vue/get-started/introduction).

## MDX Files

We use mdx files to write the stories. The files are in the [`components`](/components) folder and they're named the same as the component they're documenting but with the `.stories.mdx` file extension.

We also write these stories as described in the [MDX documentation](https://storybook.js.org/docs/vue/writing-docs/mdx), but, since we're using Vue instead of React, we write the template differently than how it is shown in the documentation.

React:

```js
export const Template = (args) => <Checkbox {...args} />;
```

**Vue**:

```js
export const Template = (args, { argTypes }) => ({
	props: Object.keys(argTypes),
	components: { Checkbox },
	template: `<checkbox v-bind="$props" />`,
});
```

## Development

```bash
# Launch the dev server
yarn storybook
```

This will launch the dev server on `localhost:6006` and it will only show the documentation for the stories since that's what we're using Storybook for in this project.

You can configure the `storybook` (or `storybook:build`) command on `package.json`.

## Build and Deployment Process

```bash
# Build storybook
yarn storybook:build
```

This command will build the static Storybook application in the `storybook` folder. For more on buildling the Storybook docs go [here](https://storybook.js.org/docs/vue/writing-docs/build-documentation#publish-storybooks-documentation).

We deploy this static folder to S3 during the CircleCI workflow. We create gzip and brotli compressed static files and upload them to S3.

Using CloudFront we send `/storybook` requests to S3 and we use Lambda@Edge to serve the gzip or brotli compressed files depending in the request headers. That allows us to have the Storybook documentation under the site's domain: [https://molekule.com/storybook/index.html](https://molekule.com/storybook/index.html).
