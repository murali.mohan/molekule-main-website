module.exports = {
	root: true,
	env: {
		browser: true,
		node: true,
	},
	parserOptions: {
		parser: 'babel-eslint',
	},
	extends: [
		'@nuxtjs',
		'prettier',
		'prettier/vue',
		'plugin:prettier/recommended',
		'plugin:nuxt/recommended',
		'plugin:vuejs-accessibility/recommended',
		'plugin:cypress/recommended',
	],
	plugins: ['vuejs-accessibility'],
	rules: {
		'vuejs-accessibility/media-has-caption': 'warn',
		'vue/custom-event-name-casing': 'warn',
	},
	overrides: [
		{
			files: ['custom-apis/**/*'],
			rules: {
				'no-console': 'off',
			},
		},
	],
};
