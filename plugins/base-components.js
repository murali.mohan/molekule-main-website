import Vue from 'vue';

export default function BaseComponentPlugin() {
	// https://webpack.js.org/guides/dependency-management/#require-context
	const requireComponent = require.context(
		// Look for files in the current directory
		'../components/',
		// Do not look in subdirectories
		false,
		// Only include "base-" prefixed .vue files
		/base-[\w-]+\.vue$/
	);

	// For each matching file name...
	requireComponent.keys().forEach((filename) => {
		// Get the component
		const options = requireComponent(filename);
		// Get the PascalCase version of the component name
		const name = formatComponentName(filename.replace('./', '').replace(/\.\w+$/, ''));
		// Globally register the component.
		Vue.component(name, options.default || options);
	});
}

function formatComponentName(name) {
	return name.replace(/(?:^|-)([a-z])/g, (_, char) => char.toUpperCase());
}
