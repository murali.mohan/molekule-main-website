async function getUser(context) {
	let user = null; // Promise.resolve({});
	try {
		let tokens = localStorage.getItem('token');
		if (tokens) {
			tokens = JSON.parse(tokens);
			const { AuthenticationResult } = tokens;
			const { AccessToken, RefreshToken = '' } = AuthenticationResult;
			if (AccessToken) {
				await context.$repositories.user
					.cognitoAttributes({
						accessToken: AccessToken,
					})
					.then((response) => {
						const { data } = response;
						const { UserAttributes } = data;
						const attributes = {};
						if (UserAttributes && Array.isArray(UserAttributes)) {
							UserAttributes.forEach((prop) => {
								attributes[prop.Name] = prop.Value;
							});
							attributes.accessToken = AccessToken;
						}
						user = Promise.resolve({ attributes });
					})
					.catch(async (error) => {
						if (error && RefreshToken) {
							const $res = await context.dispatch('USER_STORE/RESET_ACCESS_TOKEN', RefreshToken);
							if ($res) {
								getUser(context);
							} else {
								user = null;
								context.commit('USER_STORE/RESET_USER');
							}
						}
					});
			}
		} else {
			user = null;
		}
	} catch (e) {
		console.log(e);
	}
	return user;
}

export default function ({ store, route }) {
	if (process.server) {
		return;
	}
	const isUserLoggedIn = store.getters['USER_STORE/AUTHENTICATED'];
	const customerId = store.getters['USER_STORE/CUSTOMERID'];
	if (!isUserLoggedIn || !customerId) {
		getUser(store)
			.then(async (userData) => {
				if (userData) {
					await store.dispatch('USER_STORE/USER_LOGGED_STATE', userData);
					const isUserLoggedIn = store.getters['USER_STORE/AUTHENTICATED'];
					if (!isUserLoggedIn && route.path.includes('/account/')) {
						let backUrl = '/';
						try {
							backUrl = route.path;
						} catch (e) {
							console.log(e);
						}
						window.location.href = '/D2C/login/?back=' + encodeURIComponent(backUrl);
					}
				} else {
					store.dispatch('USER_STORE/USER_LOGGED_STATE', null);
					store.commit('USER_STORE/RESET_USER');
					if (route.path.includes('/account/')) {
						let backUrl = '/';
						try {
							backUrl = route.path;
						} catch (e) {
							console.log(e);
						}
						window.location.href = '/D2C/login/?back=' + encodeURIComponent(backUrl);
					}
				}
			})
			.catch((e) => {
				console.log(e);
			});
	}
}
