export default ({ query, $config, $sentry, store }, inject) => {
	if ((query.perf && query.perf.split(',').includes('no-clyde')) || store.state.locale !== 'en-us') {
		inject('clyde', () => {});
		inject('clydeLoaded', () => Promise.resolve(false));

		return;
	}

	const requestIdleCallback = (cb) => (window.requestIdleCallback ? window.requestIdleCallback(cb) : setTimeout(cb, 1));
	const wait = (ms) =>
		new Promise((resolve) =>
			setTimeout(() => {
				resolve(false);
			}, ms)
		);

	let clyde;
	const skuListByLocale = {
		'en-us': ['MinimoP_US', 'MinimoB_US', 'MH1_Device_US', 'airpro-us'],
		'ca-en': ['MinimoP_CA', 'MH1_Device_CA'],
	};
	const initConfig = {
		key: $config.clydeKey,
		skipGeoIp: false,
		skuList: skuListByLocale[store.state.locale] || skuListByLocale['en-us'],
	};

	let clydeLoadedPromiseResolve;
	const clydeLoadedPromise = new Promise((resolve) => (clydeLoadedPromiseResolve = resolve)).then((res) => res);

	let clydeInitPromiseResolve;
	let isInitializing = false;
	const clydeInitPromise = new Promise((resolve) => (clydeInitPromiseResolve = resolve)).then((res) => {
		isInitializing = false;
		return res !== false;
	});

	// Wait 15 seconds before setting the fulfilled value to "false"
	let clydeLoadedPromiseRace = Promise.race([wait(10000), clydeLoadedPromise]);

	const script = document.createElement('script');
	script.src = $config.clydeUrl;
	script.async = true;
	script.onload = () => {
		clyde = window.Clyde;
		clydeLoadedPromiseResolve(true);
		clydeLoadedPromiseRace = Promise.resolve(true);

		requestIdleCallback(() => {
			if (isInitializing) return;

			isInitializing = true;
			clyde.init(initConfig, clydeInitPromiseResolve);
		});
	};
	script.onerror = (e) => {
		$sentry.captureException(e);
		clydeLoadedPromiseResolve(false);
	};

	document.head.append(script);

	inject('clyde', async (action, ...args) => {
		const loadedSuccessfully = await clydeLoadedPromise;
		if (!loadedSuccessfully) return;

		const didInitialize = await clydeInitPromise;
		if (didInitialize !== true && !isInitializing) {
			clyde.init(initConfig, clydeInitPromiseResolve);
			await clydeInitPromise;
		}

		if (!clyde) return;

		if (action === 'init') {
			args = [
				{
					...initConfig,
					...args[0],
				},
				...args.slice(1),
			];
		}

		try {
			return args.length ? clyde[action](...args) : clyde[action]();
		} catch (e) {
			$sentry.withScope((scope) => {
				scope.setLevel('warning');
				$sentry.captureException(e);
			});
		}
	});
	inject('clydeLoaded', () => clydeLoadedPromiseRace);
};
