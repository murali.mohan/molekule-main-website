import {
  Hub,
  Auth
} from 'aws-amplify';

function getUser() {
  return Auth.currentAuthenticatedUser()
    .then((userData) => userData)
    .catch(() => console.log("Not signed in"));
}

export default (context) => {

  Hub.listen("auth", ({
    payload: {
      event,
      data
    }
  }) => {
    switch (event) {
      case "signIn":
        console.log('User is signed in');
        getUser().then((userData) => context.store.dispatch('USER_STORE/USER_LOGGED_STATE', userData));
        break;
      case "signOut":
        console.log('User Signed out');
        context.store.dispatch('USER_STORE/USER_LOGGED_STATE', null);
        // context.store.dispatch('USER_STORE/USER_LOGOUT', null);
        break;
      case "signIn_failure":
        console.log("Sign in failure", data);
        break;
      case 'configured':
        console.log('the Auth module is configured');

    }
  });

  getUser().then((userData) => {
    if (userData) {
      context.store.dispatch('USER_STORE/USER_LOGGED_STATE', userData);
      // context.store.dispatch('USER_STORE/USER_LOGGED_STATE', userData.signInUserSession.accessToken.payload["cognito:groups"]);
    } else {

      context.store.dispatch('USER_STORE/USER_LOGGED_STATE', null)
    }
  });

}
