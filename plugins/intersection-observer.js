export default async function polyfillIntersectionObserver() {
	if (!window.IntersectionObserver) {
		return import('intersection-observer');
	}
}
