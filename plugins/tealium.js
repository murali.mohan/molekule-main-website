import debounce from 'lodash/debounce';
import { getLocaleMatchInRoutePath } from '@/utils/locale-helpers';

const wait = (n) =>
	new Promise((resolve) => {
		setTimeout(resolve, n);
	});

window.utag_data = {};

const sharedData = {
	customer_email: '',
	page_url: window.location.href,
};

let waitForTealiumResolve = null;
const waitForTealium = new Promise((resolve) => {
	waitForTealiumResolve = resolve;
});

const PRODUCT_DATA_EVENTS = Object.freeze([
	'cart_add',
	'cart_add_banner',
	'cart_add_topnav',
	'cart_add_compare',
	'cart_add_sidecart',
	'quantity_increase',
	'quantity_decrease',
	'cart_remove',
	'begin_checkout_click',
]);

const CART_DATA_EVENTS = Object.freeze([
	...PRODUCT_DATA_EVENTS,
	'cta_affirmLearnMore_click',
	'whitepaper_view',
	'whitepaper_download',
	'reviews_detail',
	'learn_more_click',
	'clicked_topnav_cta',
	'clicked_external_link',
	'clicked_pureair_module',
	'pro_rx_signup',
	'rto_signup',
	'crrsa_signup',
	'sheerid_click',
	'clicked_gift_on',
	'clicked_gift_off',
	'clicked_shop_partner',
	'clicked_cta',
	'form_submit',
	'form_submit_fail',
	'clicked_warranty_sidecart',
	'clicked_warranty_no',
]);

const GIFT_PURCHASE_DATA_EVENTS = Object.freeze([
	'clicked_gift_on',
	'clicked_gift_on',
	'clicked_shop_partner',
	'clicked_cta',
	'clicked_warranty_sidecart',
	'clicked_warranty_no',
]);

function cartData(name, properties) {
	const store = this.state && this.getters ? this : this.$store || this.store;
	if (!store) return;
	const cart = store.getters['magento/cart'];
	if (!cart) return;
	const { products } = store.state.magento;

	const cartSKUs = cart.items.map((item) => item.sku);
	const cartIds = cart.items.map((item) => String(item.productId));
	const cartPrices = cart.items.map((item) => (item.productPrice || 0).toFixed(2));
	const cartQtys = cart.items.map((item) => String(item.qty));

	const ret = {};

	if (CART_DATA_EVENTS.includes(name)) {
		Object.assign(ret, {
			cart_product_id: cartIds,
			cart_product_price: cartPrices,
			cart_product_list_price: cartPrices,
			cart_product_quantity: cartQtys,
			cart_product_sku: cartSKUs,
			cart_total_items: cart.items.length,
			cart_total_value: (cart.totals.grand_total || 0).toFixed(2),
			cart_discount: cart.totals.discount ? [Math.abs(cart.totals.discount).toFixed(2)] : [],
		});
	}

	let { productsData } = properties;
	delete properties.productsData;
	if (!productsData && name === 'begin_checkout_click') {
		productsData = cart.items.map(({ sku, qty }) => ({ sku, qty }));
	}
	if (PRODUCT_DATA_EVENTS.includes(name) && Array.isArray(productsData)) {
		productsData = productsData
			.filter(({ sku }) => products[sku])
			.map((product) => ({ ...product, product: products[product.sku] }));
		const productsPrices = productsData.map(({ product }) => (product.price || 0).toFixed(2));
		Object.assign(ret, {
			// product_brand: [],
			// product_category: [],
			product_discount_amount: productsData.map(() => '0.00'),
			product_image_url: productsData.map(({ product }) => product.image && product.image.url),
			product_list_price: productsPrices,
			product_name: productsData.map(({ product }) => product.name),
			product_price: productsPrices,
			product_promo_code: productsData.map(() => ''),
			product_quantity: productsData.map(({ qty }) => String(qty)),
			product_sku: productsData.map(({ sku }) => sku),
			product_id: productsData.map(({ product }) => String(product.id)),
		});
	}

	if (GIFT_PURCHASE_DATA_EVENTS.includes(name)) {
		ret.gift_purchase = store.state.magento.isGiftPurchase;
	}

	return ret;
}

async function tealiumEvent(name, properties = {}) {
	await waitForTealium;

	const store = this.state && this.getters ? this : this.$store || this.store;
	const data = {
		...sharedData,
		country: store.state.apiCountry,
		...cartData.bind(this)(name, properties),
		tealium_event: name,
		...properties,
	};

	try {
		window.utag.link(data);
	} catch (e) {
		// ignore errors but report to sentry as warnings
		if (window.$nuxt.$sentry) {
			window.$nuxt.$sentry.withScope((scope) => {
				scope.setLevel('warning');
				window.$nuxt.$sentry.captureException(e);
			});
		}
	}
	// eslint-disable-next-line no-console
	// console.log(data);
	if (properties.customer_email) {
		sharedData.customer_email = properties.customer_email;
	}
}

function setupScrollTracking() {
	const steps = [25, 50, 75, 100];
	const scrollListener = () => {
		const scrolled = window.pageYOffset;
		const pageHeight = document.body.scrollHeight - window.innerHeight;
		const progress = (scrolled / pageHeight) * 100;

		while (steps[0] && progress >= steps[0]) {
			const step = steps.shift();
			this.$tealiumEvent('scroll_tracking', {
				event_category: 'Scroll Tracking',
				event_action: `Scrolled ${location.protocol + '//' + location.host + location.pathname}`,
				event_label: `${step}%`,
				scroll_threshold: String(step),
				scroll_units: String(pageHeight),
				scroll_direction: 'vertical',
			});

			if (steps.length === 0) {
				window.removeEventListener('scroll', debouncedScrollListener);
			}
		}
	};

	const debouncedScrollListener = debounce(scrollListener, 100);
	window.addEventListener('scroll', debouncedScrollListener);
}

function setupSessionTracking() {
	// Session tracking on current site is using session storage to store time.
	// I don't believe we need it here as navigations between pages are not browser navigations

	const track = (event, action) =>
		this.$tealiumEvent(event, { event_category: 'Session Duration', event_action: action });

	const minute = 60 * 1000;

	setTimeout(() => track('1minute', 'One Minute on Site'), 1 * minute);
	setTimeout(() => track('8minutes', 'Eight Minutes on Site'), 8 * minute);
}

function setupAffirmTracking() {
	document.addEventListener('click', (e) => {
		const { target } = e;
		if (target && target.tagName === 'A' && target.classList && target.classList.contains('affirm-modal-trigger')) {
			this.$tealiumEvent('cta_affirmLearnMore_click', {
				event_category: 'Affirm',
				event_action: 'Clicked Affirm Learn More',
			});
		}
	});
}

function setupReferralTracking() {
	document.addEventListener('click', async (e) => {
		let { pathname } = new URL(window.location);
		const mainContentElement = document.querySelector('#maincontent');
		const localeMatchInRoute = getLocaleMatchInRoutePath(pathname);
		if (localeMatchInRoute) {
			pathname = pathname.replace(`/${localeMatchInRoute}/`, '/');
		}
		if (pathname !== '/where-to-buy' || !mainContentElement.contains(e.target)) return;

		e.preventDefault();
		let { target } = e;

		while (target.tagName !== 'A' && target !== document.body) {
			target = target.parentElement;
		}

		if (target.tagName === 'A') {
			const { href, target: linkTarget } = target;
			const { host } = new URL(href);

			const referralPartner = {
				'www.apple.com': 'Apple',
				'www.bestbuy.com': 'Best Buy',
				'www.amazon.com': 'Amazon',
				'b8ta.com': 'b8ta',
				'www.aloyoga.com': 'alo',
				'store.moma.org': 'MoMA',
				'gopremco.com': 'Premium Incentive',
			}[host];

			if (referralPartner) {
				const { 'magento/cart': cart } = this.store.getters;
				const cartTotal = ((cart && cart.totals && cart.totals.grand_total) || 0).toFixed(2);

				await this.$tealiumEvent('clicked_shop_partner', {
					event_category: 'Where to Buy',
					event_action: 'Clicked to shop at retail partner site',
					event_label: [referralPartner],
					event_value: [cartTotal],
					referral_partner: referralPartner,
					referral_url: href,
				});
			}

			if (linkTarget === '_blank') {
				const win = window.open(href, '_blank');
				win.focus();
			} else {
				window.location = href;
			}
		}
	});
}

window.utag_cfg_ovrd = { noview: true }; // don't automatically track initial page load

export default (ctx, inject) => {
	const { app, query, store, $config } = ctx;
	const skipTealiumAsync =
		query.perf && (query.perf.split(',').includes('no-tealium') || query.perf.split(',').includes('no-tealium-async'));
	if (!skipTealiumAsync) {
		(function (a, b, c, d) {
			a = $config.tealium.asyncUrl;
			b = document;
			c = 'script';
			d = b.createElement(c);
			d.src = a;
			d.type = 'text/java' + c;
			d.async = true;
			d.onload = waitForTealiumResolve;
			a = b.getElementsByTagName(c)[0];
			a.parentNode.insertBefore(d, a);
		})();
	}

	app.router.afterEach(async (to, from) => {
		// console.log(to, from);
		await waitForTealium;
		await wait(200); // wait for title to update

		let pageType = to.name;
		const localeMatchInRoute = getLocaleMatchInRoutePath(to.path);
		const standardLocale = (localeMatchInRoute || '').split('-').reverse().join('-');
		if (pageType === 'all') {
			pageType = (localeMatchInRoute ? to.path.replace(`/${localeMatchInRoute}/`, '/') : to.path)
				.slice(1)
				.replace('/', '-');
		} else if (localeMatchInRoute) {
			pageType = to.name.replace(`___${standardLocale}`, '');
		}

		pageType = pageType === 'index' ? 'homepage' : pageType;

		Object.assign(sharedData, {
			page_type: pageType,
			page_url: window.location.href,
			country: store.state.apiCountry,
		});

		try {
			window.utag.view({ ...sharedData });
		} catch (e) {
			// ignore errors but report to sentry as warnings
			if (app.$sentry) {
				app.$sentry.withScope((scope) => {
					scope.setLevel('warning');
					app.$sentry.captureException(e);
				});
			}
		}
	});

	inject('tealiumEvent', tealiumEvent);
	inject('tealiumWait', waitForTealium);
	setupScrollTracking.call(ctx);
	setupSessionTracking.call(ctx);
	setupAffirmTracking.call(ctx);
	setupReferralTracking.call(ctx);
};
