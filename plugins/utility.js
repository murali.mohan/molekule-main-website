let recaptchaPromise = null;

export default (context, inject) => {
  // common functions

  //function to get location of uploaded file
  const taxFileUpload = async (self, {files}, customerId) => {
      let TempFormData = new FormData();
      let fileLocation = []; // file location on server
      // const files = self.files.files

      if (files.length && customerId) {
          for (let i = 0; i < files.length; i++) {
              TempFormData.append(`file`, files[i]);
              // try {
                await self.$repositories.user.upload_tax_documents({
                  customerId,
                  data: TempFormData
              }).then(res => {
                  fileLocation.push(res.data)
                  TempFormData.delete(`file`);
              }).catch((error) => {
                if(error.response && error.response.data) {
                  console.log(error.response.data.errorMessage);
                  }
                fileLocation = [];
                })
          }
      }
      return fileLocation
  }
  const makeAddressModel = (addressDetails = {}, prefix, emailPhone = false,$key) => {
      let address = {};
      if (addressDetails[prefix + 'name']) {
        address['name'] = addressDetails[prefix + 'name'] || ''
      } else {
        address['firstName'] = addressDetails[prefix + 'first_name'] || ''
        address['lastName'] = addressDetails[prefix + 'last_name'] || ''
      }

      if (emailPhone) {
        address['email'] = addressDetails[prefix + 'email'] || ''
        address['phoneNumber'] = addressDetails[prefix + 'phone_number'] || ''
        return address;
      }
      if(addressDetails[prefix+ 'company_name']) {
        address['companyName'] = addressDetails[prefix + 'company_name'] || ''
        address['company'] = addressDetails[prefix + 'company_name'] || ''
      }
      if(addressDetails[prefix+ 'phone_number']) {
        address['phoneNumber'] = addressDetails[prefix + 'phone_number'] || ''
      }
      address['streetAddress1'] = addressDetails[prefix + 'address_1'] || ''
      address['streetName'] = addressDetails[prefix + 'address_1'] || ''

      address['streetAddress2'] = addressDetails[prefix + 'address_2'] || ''
      address['city'] = addressDetails[prefix + 'city'] || ''
      address['state'] = addressDetails[prefix + 'state'] || ''
      address['postalCode'] = addressDetails[prefix + 'postal_code'] || ''
      address['country'] = addressDetails[prefix + 'country'] || ''
      //set params for editID
      if($key){
        if(addressDetails[$key]!=undefined){
          address[$key] = addressDetails[$key];
        }
      }
      return address;
  }
  const setAddress=($prefix, $value, $key, $tealium)=>{
    let $address = {}
    $address[$prefix+'first_name']=$value.firstName,
    $address[$prefix+'last_name']=$value.lastName
    if($tealium) {
      $address[$prefix+'email'] = $value.email,
      $address[$prefix+'phone']=$value.phone || '',
      $address[$prefix+'id']= $key,
      $address[$prefix+'uid']= $key
    } else {
    $address[$prefix+'address_1']=$value.streetAddress1,
    $address[$prefix+'address_2']=$value.streetAddress2 || '',
    $address[$prefix+'city']=$value.city,
    $address[$prefix+'state']=$value.state,
    $address[$prefix+'postal_code']=$value.postalCode
    $address[$prefix+'country']=$value.country || 'US'
    $address[$prefix+'phone_number']=$value.phoneNumber || ''
    $address[$prefix+'company_name']=$value.companyName || $value.company || ''
    if($value.name){
      $address[$prefix+'name']=$value.name
    }
    if($value.email){
      $address[$prefix+'email'] = $value.email
    }
    if($key){
      if($value[$key]!=undefined){
        $address[$key] = $value[$key];
      }
    }
  }
    return $address
  }
  const makeFreightModel=(freightDetails,$key)=>{
      let $freight ={};
      $freight.forkLift=freightDetails.forkLiftAvailable;
      $freight.loadingDock=freightDetails.dockAvailable;
      $freight.instructions= freightDetails.deliveryInformation;
      $freight.receivingHours={
        "from" :freightDetails.selectedFromTime,
        "to":freightDetails.selectedToTime
      }
      $freight.deliveryAppointmentRequired=freightDetails.appointmentAvailable;
      if($key){
        if(freightDetails[$key]!=undefined){
          $freight[$key] = freightDetails[$key];
        }
      }
      return $freight;
  }

const quoteFlowCheck = (isAuth, isShowQuoteFlow, type, data, store) => {
  // const isShowQuoteFlow =  self.state.CART_STORE.SHOW_QUOTE_FLOW || self.$store.state['CART_STORE']['SHOW_QUOTE_FLOW']
  if (isAuth || isShowQuoteFlow) {
    return data;
  }
  store.dispatch('CART_STORE/TOGGLE_QUOTE_FLOW', true);
  if (type === 'PRODUCT') {
        return  [];
  } else if (type === 'LINE_ITEM') {
    for (let i = 0; i < data.length; i++) {
      const price = data[i]['price'];

      if (price && price['tiers']) {
        price['tiers'] = [];
      }
    }
  }
}

const parseCartForSubscription = async (thisObject, payload) => {
  const _getAttr = (nodeVal, itemName) => {
    let retVal = null;
    const { attributes = [] } = nodeVal || {};
    if(attributes.length) {
      const matchedNode = attributes.filter( item => item.name === itemName );
      if(matchedNode.length === 1) {
        retVal = matchedNode[0].value;
      }
    }
    return retVal;
  }
  let res = payload;
  const isGift = !!res?.custom?.fields?.gift;
  if(!res.lineItems) {
    return payload;
  }
  let originalLineItems = res.lineItems;
  let newLineItems = [];
  let subscriptionProductsList = [];
  let currentLineItem;
  let i, n;
  res['hasSubscription'] = false;
  res['db_cache_products'] = {};
  const locale = 'en-US';
  // const protections = {};
  // for (i = 0, n = originalLineItems.length; i < n; i++ ) {
  //   //clyde demo
  //   if(originalLineItems[i]?.custom?.fields?.protectionPlanLineItem){
  //     protections[originalLineItems[i][id]] = originalLineItems[i].custom.fields.protectionPlanLineItem
  //   }
  //    //clyde demo
  // }
  for (i = 0, n = originalLineItems.length; i < n; i++ ) {
      currentLineItem = originalLineItems[i];
      //Subscription enabled product, skip in lineItems
      if(currentLineItem?.custom?.fields?.protectionPlanLineItem){
        let clydeItem = originalLineItems.filter($obj =>{
          return currentLineItem.custom.fields.protectionPlanLineItem == $obj.id
        })
        currentLineItem.protectionPlan = Object.assign({}, clydeItem[0])
        currentLineItem.hasProtection = true
      } else {
        currentLineItem.hasProtection = false
      }
      if(currentLineItem.custom && currentLineItem.custom.fields && currentLineItem.custom.fields.subscriptionEnabled) {
        subscriptionProductsList.push(currentLineItem);
        continue;
      }
      if(currentLineItem.productType && currentLineItem.productType.obj && currentLineItem.productType.obj.name == 'Warranty'){
        continue
      }
      currentLineItem.priceDisplay = {
        strikePrice: 0,
        price: currentLineItem.price?.value['centAmount'] * currentLineItem.quantity
      }
      //set strike through price
      let { discounted } = currentLineItem.price;
      if(discounted){
        currentLineItem.priceDisplay.price = discounted?.value['centAmount'] * currentLineItem.quantity;
          let variantPrice = currentLineItem.variant.prices;
          for (let i = 0; i < variantPrice.length; i++) {
            if (variantPrice[i].id === currentLineItem.price.id) {
                currentLineItem.priceDisplay.strikePrice = variantPrice[i]['value']['centAmount'] * currentLineItem.quantity;
                break;
            }
          }
      }
      newLineItems.push(currentLineItem);
      if(!isGift){
        //Check for product mapped with Subscription;
        const subscriptionData = _getAttr(currentLineItem.variant, 'SubscriptionIdentifier');
        //Product has subscription product, proceed
        if(subscriptionData && subscriptionData.id && subscriptionData.obj) {
          const subscriptionProductData = subscriptionData.obj;
          try {
            const current = subscriptionProductData.masterData.current;
            let filterProductValue = _getAttr(current.masterVariant, 'filterProduct');
            let period = _getAttr(current.masterVariant, 'period');
            let filterProduct = filterProductValue;
            let filterProductId = filterProductValue.id;
            let filterProductData = filterProductValue && filterProductValue.obj ? filterProductValue.obj : {};
            //Initialize FE Subscription Identifiers & Data
            res['hasSubscription'] = true;
            currentLineItem['cartItemHasSubscription'] = true;
            let $productLineItem = originalLineItems.filter($obj=>{
              if($obj.productId == subscriptionData.id){
                return $obj
              }
            })
            currentLineItem['cartItemSubscriptionData'] = {
              selected: false,
              period,
              price: {
                centAmount: 0
              },
              productName: current.name?.[locale] || '',
              productDescription: current.description?.[locale] || '',
              productPlan: 'product_plan',
              nextRefill: 'next_refill',
              id: $productLineItem[0]?.id
            };

            //Search subscription added in cart
            const subscriptionId = subscriptionData.id;
            const searchSubscriptionInLineItem = originalLineItems.filter(item => item.productId === subscriptionId);
            if(searchSubscriptionInLineItem && searchSubscriptionInLineItem.length) {
              const temp = searchSubscriptionInLineItem[0];
              filterProduct = _getAttr(temp.masterVariant, 'filterProduct');
              //filter exists in line items.
              currentLineItem['cartItemSubscriptionData']['selected'] = true;
            }
            if(filterProduct && filterProduct.obj) {
              filterProductData = filterProduct.obj;
              filterProductId = filterProduct.id;
              thisObject.state['CART_STORE']['DB_CACHE']['products'][filterProductId] = filterProductData;
            }
            //Filter product id exists,
            if(filterProductId) {
              //FilterProduct data not existing in lineItems & subscriptionIdentifier.
              if(!filterProductData.id) {
                //Search if it exists in cache
                if (thisObject.state['CART_STORE']['DB_CACHE']['products'][filterProductId]) {
                  filterProductData = thisObject.state['CART_STORE']['DB_CACHE']['products'][filterProductId];
                }
                else {
                  //Call api & get product detail
                  const filterProdResponse = await thisObject.$repositories.product.get(filterProductId, thisObject.state['USER_STORE']['CUSTOMERID']);
                  if(filterProdResponse && filterProdResponse.data && filterProdResponse.data.productBean) {
                    filterProductData = filterProdResponse.data.productBean;
                    thisObject.state['CART_STORE']['DB_CACHE']['products'][filterProductId] = filterProductData;
                  }
                }
              }
              //Filter product data found, get its price;
              if(filterProductData && filterProductData.id) {
                const masterVariant = filterProductData.masterData.current.masterVariant;
                const price = masterVariant.price?.value || masterVariant.prices[0]?.value || {centAmount: 0};
                currentLineItem['cartItemSubscriptionData'] = {
                  ...currentLineItem['cartItemSubscriptionData'],
                  price,
                };
              }
            }
          }catch(e) {
            console.log('utility.js - ERROR @ filterProduct', e);
          }
        }
      }
  }
  res['lineItems'] = newLineItems;
  res['lineItemsOriginal'] = originalLineItems;
  return res;
}

  const processOrderStatus = (orderItem) => {
    let processedOrderItem = orderItem;
    if(processedOrderItem && processedOrderItem.orderStatus) {
      let { orderState = [], orderStatus = '', holds = []} = orderItem;
      if(!Array.isArray(orderState)){
        orderState = [orderState]
      }
      let $greenIndicator = true
      if(orderStatus === 'Pending' || orderStatus === 'Cancelled' || orderStatus === 'On hold'){
        $greenIndicator = false
      }
      let orderBulletColor = $greenIndicator ? 'bg-secondary': 'bg-lightviolet'
      if(orderState.includes('Customer Hold')){
        orderState = holds ? holds : []
      }
      if(orderState && orderState.length) {
        processedOrderItem.isOrderPendingFreightMissing = false;
        processedOrderItem.isOrderTermsPending = false;
        processedOrderItem.isOrderTaxPending = false;
        orderState.forEach(eachHold => {
              switch(eachHold) {
                  case 'FreightHold':
                    processedOrderItem.isOrderPendingFreightMissing = true;
                      break;
                  case 'CreditHold':
                    processedOrderItem.isOrderTermsPending = true;
                      break;
                  case 'TaxHold':
                    processedOrderItem.isOrderTaxPending = true;
                      break;
                  default:
                      break;
              }
          });
        const isHold = orderState.includes('FreightHold') || orderState.includes('CreditHold') || orderState.includes('TaxHold');
        if(isHold) {
          processedOrderItem.orderStatus = 'On hold';
          orderBulletColor = 'bg-lightviolet';
        } else {
          processedOrderItem.orderStatus = orderStatus
        }
      }
      processedOrderItem.orderBulletColor = orderBulletColor;
    }
  return processedOrderItem;
}

const getProductDetail = async (self, productId = '', customerId  = '') => {
  const _parseProductData = (response) => {
    const { data } = response;
    const { productBean } = data; //pick productBean,
    const { id, masterData } = productBean;
    const { current: currentNode } = masterData;
    const { masterVariant } = currentNode;
    const { price, key:productKey, sku:productSku } = masterVariant;
    const { value: productPriceMSRP } = price;
    const locale = 'en-US'
    return {
      productId: id,
      productKey,
      productSku,
      productName: currentNode['name'][locale],
      productDescription: currentNode['description'][locale],
      productSlug: currentNode['slug'][locale],
      productPriceMSRP,
    };
  }
  let productDetail = {};
  try {
    const prodResponse = await self.$repositories.product.get(productId, customerId);
    productDetail = _parseProductData(prodResponse)
  }catch(e){
    console.log(e);
  }
  return productDetail;
}
//Cart data for tealium
const setCartTealium=(self,$value,total_price,cartId,$key)=>{
  let $cart = {
    cart_id:cartId,
    cart_product_id:[],
    cart_product_price:[],
    cart_product_quantity:[],
    cart_product_sku:[],
    cart_discount:[],
    cart_total_items:"",
    cart_total_value:"",
    cart_product_list_price:[]
  }
  $value.forEach(element => {
    $cart.cart_product_id.push(element.productId);
    $cart.cart_product_quantity.push(element.quantity.toString());
    let sku=element.variant ? element.variant.sku : '';
    $cart.cart_product_sku.push(sku);
    //Cart product discount
    if(element.discountedPricePerQuantity.length > 0){
      let discount = element.discountedPricePerQuantity[0].toString();
      $cart.cart_discount.push(discount);
    }
    //Cart product price
    if(element.price.value){
      let price=element.price.value.centAmount;
      price = self._vm.$options.filters.centsToDollars(price);
      $cart.cart_product_price.push(price);
      $cart.cart_product_list_price.push(price);
    }
    //Cart total price
    $cart.cart_total_value = self._vm.$options.filters.centsToDollars(total_price).toString();
  });
  let total_items = $value.length;
  $cart.cart_total_items = total_items.toString();
  return $cart
}
//subscription data for tealium
const setFilterTealium=(self,$value,$key)=>{
  let $filter = {
    device_id:[],
    device_name:[],
    filter_refill_end_date:'',
    payment_on_file:"false",
    auto_refill:"false",
    product_plan_details:"N/A",
    product_renewal_date:"N/A",
    free_refill_eligible:"false",
    filter_next_charge_amount:[]
  }
  $value.forEach(element => {
    if(element.cartItemHasSubscription == true){
      $filter.auto_refill = element.cartItemSubscriptionData['selected'].toString();
      //filter next charge amount
      let price=element.cartItemSubscriptionData.price.centAmount;
      price = self._vm.$options.filters.centsToDollars(price);
      $filter.filter_next_charge_amount.push(price);
    }
  });
  return $filter
}

 //Tealium for checkout page
 const checkoutTealium=(self,tealiumEvent,eventAction,eventLabel,clickLocation,cartDetails,customerDetails,companyCategory)=>{
  let tealiumPayload ={};
  if(cartDetails && customerDetails){
    tealiumPayload = {
      event_action: eventAction,
      event_label: eventLabel,
      event_value: eventLabel,
      customer_type: 'Mo-registered',
      customer_tos_opt_in: true,...cartDetails,...customerDetails
    }
    if(eventAction == "Select Shipping Method"){
      tealiumPayload.order_shipping_type =eventLabel;
    }
    if(eventAction == "Select Payment Method"){
      tealiumPayload.order_payment_type =eventLabel;
    }
    if(eventAction == "Clicked Auto-Refills on Product"){
      tealiumPayload.auto_refill ="true";
    }
    if(eventAction == "Accept Auto-Refills"){
      tealiumPayload.customer_refill_opt_in = false;
      tealiumPayload.customer_marketing_opt_in= true;
      tealiumPayload.customer_industry_category= companyCategory;
      tealiumPayload.customer_email_preference= customerDetails.email
    }
    if(eventAction == "Clicked Begin Checkout"){
      tealiumPayload.event_category= "Cart";
      tealiumPayload.page_type = "Cart";
    }else{
      tealiumPayload.event_category= "Checkout";
      tealiumPayload.page_type = "Checkout";
    }
    if(eventAction == 'Save' || eventAction =='Edit' || eventAction == 'Clicked Begin Checkout'){
      tealiumPayload.click_text =eventLabel;
      tealiumPayload.click_url = 'Checkout';
      tealiumPayload.click_location = clickLocation;
    }
  }
   self.$telium.setTealiumPayload(tealiumEvent, tealiumPayload);
}
const getPageViewedPercentage = (previousScroll) => {
  let scrollPosition = window.scrollY;
  let contentHeight = document.body.scrollHeight;
  let contentViewHeight = document.documentElement.clientHeight;
  let scrollPercent = Math.round((scrollPosition) / (contentHeight - contentViewHeight) * 100);

  if (scrollPercent > 0 && scrollPercent <= 25 && previousScroll <= 25) {
      return 25
  } else if (scrollPercent > 25 && scrollPercent <= 50 && previousScroll <= 50) {
      return 50
  } else if (scrollPercent > 50 && scrollPercent <= 75 && previousScroll <= 75) {
      return 75
  } else if (scrollPercent > 75 && previousScroll <= 100) {
      return 100
  }
}

const centsToDollarsConvert = (value,decimal) =>{
  if (!value) return 0;
  if(!decimal) decimal = 0;
  value = (value + '').replace(/[^\d.-]/g, '');
  value = parseFloat(value);
  const final = value ? value / 100 : 0;
  return final.toFixed(decimal);
}
const loadRecaptcha = () => {
	if (!recaptchaPromise) {
		recaptchaPromise = new Promise((resolve) => {
			window.recaptchaLoaded = () => {
				resolve(window.grecaptcha);
			};

			const script = document.createElement('script');
			script.src = 'https://www.google.com/recaptcha/api.js?onload=recaptchaLoaded&render=explicit';
			script.async = true;

			document.head.appendChild(script);
		});
	}

	return recaptchaPromise;
}
const makeFormPayload = (formDetails, formValue, recapchaValue, marketingOptIn, organization) => {
  let formPayload = {}
  formPayload['recaptchaResponse'] = recapchaValue
  formPayload['form'] = {}
  let labels = {
    'first_name': 'First Name *',
    'last_name': 'Last Name *',
    'email': 'Your work email *',
    'organization_name': organization,
    'phone': 'Phone Number *',
    'industry': 'Category'
  }
  let purchaseLabels = {
    'line1': 'Mailing address',
    'city': 'City',
    'state': 'State',
    'postal_code': 'Postal code',
    'air_pro_rx_quantity': 'Molekule Air Pro RX Quantity',
    'air_pro_quantity': 'Molekule Air Pro Quantity',
    'air_quantity': 'Molekule Air Quantity',
    'air_mini_plus_quantity': 'Molekule Air Mini+ Quantity',
    'air_mini_quantity': 'Molekule Air Mini Quantity',
    'description': 'Message (optional)'
  }
  if(formValue == 'Purchase Inquiry') {
    let newLabels = {...labels, ...purchaseLabels}
    labels = newLabels
  }
  formPayload['form']['Lead Type'] = {
    "label": "Lead Type",
    "value": formValue
  }
  formPayload['form']['Marketing Opt-in'] = {
    "label": "Marketing Opt-in",
    "value": marketingOptIn
  }
  for (let key in formDetails) {
    if(key) {
      formPayload['form'][key] = {
        "label": labels[key] || '',
        "value": formDetails[key]
      }
    }
  }
  return formPayload
}

//Tealium for forms
const formsTealium=(self,tealiumEvent,eventAction,cartDetails,customerDetails,formDetails,pageType,opt_in,formType)=>{
  let tealiumPayload ={};
  let formData = formDetails.form;
  if(cartDetails && customerDetails && formData){
    tealiumPayload = {
      page_type: pageType,
      ab_test_group: '',
      offer_name: '',
      event_action: eventAction,
      event_label: eventAction,
      event_value: eventAction,
      event_category: eventAction,
      event_error: 'False',
      error_message: 'Success : False',
      customer_type: 'Mo-registered',
      customer_tos_opt_in: 'true',
      customer_refill_opt_in:'false',
      customer_business_name:formData.organization_name.value,
      customer_marketing_opt_in: opt_in,
      customer_industry_category:formData.industry.value,
      customer_email_preference:formData.email.value,
      customer_email:formData.email.value,
      customer_first_name:formData.first_name.value,
      customer_last_name:formData.last_name.value,
      customer_phone:formData.phone.value,
      customer_id : customerDetails.id ? customerDetails.id : "",
      customer_uid : customerDetails.id ? customerDetails.id : ""
    }
    //Sing-in user
    if(tealiumPayload.customer_id!=""){
      tealiumPayload={
        ...cartDetails,...tealiumPayload
      }
    }else{
      tealiumPayload.cart_id="",
      tealiumPayload.cart_product_id=[],
      tealiumPayload.cart_product_price=[],
      tealiumPayload.cart_product_quantity=[],
      tealiumPayload.cart_product_sku=[],
      tealiumPayload.cart_discount=[],
      tealiumPayload.cart_total_items="",
      tealiumPayload.cart_total_value="",
      tealiumPayload.cart_product_list_price=[]
    }
    if(formType == "long"){
      tealiumPayload.customer_state_billing =formData.state.value
      tealiumPayload.customer_state_shipping =formData.state.value
      tealiumPayload.customer_zip_billing =formData.postal_code.value
      tealiumPayload.customer_zip_shipping =formData.postal_code.value
      tealiumPayload.product_brand= ['Molekule'];
      tealiumPayload.product_sku= [];
      tealiumPayload.product_id= [];
      tealiumPayload.product_category= [];
      tealiumPayload.product_name= [];
      tealiumPayload.product_quantity= [];
      if(formData.air_pro_rx_quantity){
        tealiumPayload.product_quantity.push(formData.air_pro_rx_quantity.value);
        tealiumPayload.product_name.push(formData.air_pro_rx_quantity.label);
      }
      if(formData.air_pro_quantity){
        tealiumPayload.product_quantity.push(formData.air_pro_quantity.value);
        tealiumPayload.product_name.push(formData.air_pro_quantity.label);
      }
      if(formData.air_mini_plus_quantity){
        tealiumPayload.product_quantity.push(formData.air_mini_plus_quantity.value);
        tealiumPayload.product_name.push(formData.air_mini_plus_quantity.label);
      }
      if(formData.air_quantity){
        tealiumPayload.product_quantity.push(formData.air_mini_plus_quantity.value);
        tealiumPayload.product_name.push(formData.air_mini_plus_quantity.label);
      }
      if(formData.air_mini_quantity){
        tealiumPayload.product_quantity.push(formData.air_mini_plus_quantity.value);
        tealiumPayload.product_name.push(formData.air_mini_plus_quantity.label);
      }
    }
  }
  self.$telium.setTealiumPayload(tealiumEvent, tealiumPayload);
}

const elementFocus=(self, $refId)=>{
  self.$nextTick().then(function(){
    if(self.$refs && self.$refs[$refId]){
      self.$refs[$refId].focus()
    }
  })
}
const fieldFocus=(self, $refId)=>{
  self.$nextTick().then(function(){
    if(self.$refs && self.$refs[$refId]){
      self.$refs[$refId].$el.focus()
    }
  })
}

const getPaymentToken = () => {

}


  //Export as utility
  const utility = {
      taxFileUpload,
      makeAddressModel,
      setAddress,
      makeFreightModel,
      quoteFlowCheck,
      parseCartForSubscription,
      processOrderStatus,
      getProductDetail,
      setCartTealium,
      setFilterTealium,
      checkoutTealium,
      getPageViewedPercentage,
      centsToDollarsConvert,
      loadRecaptcha,
      makeFormPayload,
      formsTealium,
      elementFocus,
      fieldFocus,
      getPaymentToken
  }
  inject('utility', utility)
}
