import Prismic from 'prismic-javascript';

export default function prismicPreview({ $config }) {
	// Warning: cookie name is normally coming from Prismic library so it may change
	if (document.cookie.includes(Prismic.previewCookie) || document.cookie.includes('io.prismic.molekulePreview=')) {
		window.prismic = {
			endpoint: $config.prismicApiEndpoint,
		};
		const s = document.createElement('script');
		s.src = 'https://static.cdn.prismic.io/prismic.min.js';
		s.onload = () => {
			const { removeCookie } = require('tiny-cookie');
			removeCookie('io.prismic.molekulePreview');
		};
		document.head.appendChild(s);
	}
}
