import Prismic from 'prismic-javascript';
const LOCATION_COOKIE = 'molekuleLocation';
let cookie;
let alpha2ToDefaultLocale;
let defaultLocaleToAlpha2;
let supportedCountryAlpha2Values;
let getLocaleMatchInRoutePath;
let countryDomains;

if (process.server) {
	cookie = require('cookie');
	({
		alpha2ToDefaultLocale,
		defaultLocaleToAlpha2,
		supportedCountryAlpha2Values,
		countryDomains,
		getLocaleMatchInRoutePath,
	} = require('@/utils/locale-helpers'));
}

export default ({ app, req, res, redirect, store, query, route }) => {
	if (!process.server) return;

	const { router } = app;
	const countryAlpha2 =
		req.headers['cf-ipcountry'] || req.headers['cloudfront-viewer-country'] || process.env.DEFAULT_COUNTRY_CODE;
	const localeFromHeaders = alpha2ToDefaultLocale[countryAlpha2];
	const hasSupportedLocale = Boolean(localeFromHeaders);
	const localeMatchInRoute = getLocaleMatchInRoutePath(route.path);
	const parsedCookie = cookie.parse(req.headers.cookie || '');
	let { [LOCATION_COOKIE]: locationCookie } = parsedCookie;
	let hasLocationCookie = supportedCountryAlpha2Values.includes(locationCookie);
	const cookiesToSet = [];

	const { fromCustomApisPrismicPreview } = parsedCookie;
	const prismicPreviewCookie = parsedCookie[Prismic.previewCookie] || parsedCookie['io.prismic.molekulePreview'];
	const oldPrismicPreviewCookie = decodeURIComponent(parsedCookie.oldPrismicPreviewCookie);

	// Set 'locationCookie' on initial preview load or on prismic preview button click
	if (
		prismicPreviewCookie &&
		!query.setLocation &&
		(fromCustomApisPrismicPreview === 'true' || oldPrismicPreviewCookie !== prismicPreviewCookie)
	) {
		const newLocation = defaultLocaleToAlpha2[localeMatchInRoute] || process.env.DEFAULT_COUNTRY_CODE;

		// Only set 'locationCookie' if necessary
		if (locationCookie !== newLocation) {
			query.setLocation = newLocation;
		}

		// 'oldPrismicPreviewCookie' prevents resetting locationCookie based on 'prismicPreviewCookie'
		// if the preview that's being viewed hasn't changed
		cookiesToSet.push(
			cookie.serialize('oldPrismicPreviewCookie', prismicPreviewCookie, {
				httpOnly: true,
				path: '/',
			})
		);

		// 'fromCustomApisPrismicPreview' let's the code know whether the prismic preview button was clicked
		// (i.e. /custom-apis/prismic-preview was visited recently). If the preview button was clicked, this if block
		// is able to run and set the location again even if still viewing the same preview as before.
		// 'fromCustomApisPrismicPreview' is removed when this if block runs
		if (fromCustomApisPrismicPreview) {
			cookiesToSet.push(
				cookie.serialize('fromCustomApisPrismicPreview', null, {
					httpOnly: true,
					expires: new Date(new Date(0).toUTCString()),
					path: '/',
				})
			);
		}
	}

	// Remove cookie if the preview was closed
	if (oldPrismicPreviewCookie && !prismicPreviewCookie) {
		cookiesToSet.push(
			cookie.serialize('oldPrismicPreviewCookie', null, {
				httpOnly: true,
				expires: new Date(new Date(0).toUTCString()),
				path: '/',
			})
		);
	}

	const redirectAllowed = query.redirect !== '0';
	const hasSetLocation = supportedCountryAlpha2Values.includes(query.setLocation);
	const setLocation = query.setLocation;
	const locale = localeMatchInRoute || process.env.CMS_DEFAULT_LOCALE;

	const routeClean = {
		path: localeMatchInRoute ? route.path.substring(localeMatchInRoute.length + 1) || '/' : route.path,
		params: route.params,
		query: { ...route.query, setLocation: undefined },
	};
	let targetLocation = process.env.DEFAULT_COUNTRY_CODE;
	const targetRoute = { ...routeClean };

	if (hasSetLocation) {
		cookiesToSet.push(
			cookie.serialize(LOCATION_COOKIE, setLocation, {
				httpOnly: true,
				maxAge: 60 * 60 * 24 * 7, // 1 week
				path: '/',
			})
		);

		hasLocationCookie = true;
		locationCookie = setLocation;
	}

	if (cookiesToSet.length) {
		res.setHeader('Set-Cookie', cookiesToSet);
	}

	if (hasSupportedLocale) {
		targetLocation = countryAlpha2;
	}

	if (hasLocationCookie) targetLocation = locationCookie;

	if (!countryDomains.includes(targetLocation)) {
		targetRoute.path = `/${alpha2ToDefaultLocale[targetLocation]}${routeClean.path}`;
	}

	if (route.path !== targetRoute.path || hasSetLocation) {
		targetRoute.query = { ...targetRoute.query }; // in case cookies are not working

		if (hasSetLocation) {
			targetRoute.query.redirect = '0';
		}
		if (!redirectAllowed && defaultLocaleToAlpha2[localeMatchInRoute]) {
			targetLocation = defaultLocaleToAlpha2[localeMatchInRoute];
		}
		if (redirectAllowed) {
			redirect(router.resolve(targetRoute).href);
			return;
		}
	}

	store.commit('IS_US_WEBSITE', locale === 'en-us');
	store.commit('SET_LOCALE', locale);
	store.commit('SET_API_COUNTRY', targetLocation);
};
