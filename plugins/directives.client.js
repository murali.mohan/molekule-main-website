// Based on this example: https://www.w3.org/TR/wai-aria-practices-1.2/examples/dialog-modal/dialog.html
import Vue from 'vue';

let focusAfterClosedElement = document.body;
let ignoreUtilFocusChanges = false;
const openDialogs = new WeakMap();
const dialogElements = [];

const saveLastFocusableElement = (e) => (focusAfterClosedElement = e.target);
document.addEventListener('focus', saveLastFocusableElement, true);

const getCurrentDialog = () => openDialogs.get(dialogElements[dialogElements.length - 1]);

const isFocusable = (element) => {
	if (element.tabIndex > 0 || (element.tabIndex === 0 && element.getAttribute('tabIndex') !== null)) {
		return true;
	}

	if (element.disabled) {
		return false;
	}

	switch (element.nodeName) {
		case 'A':
			return !!element.href && element.rel !== 'ignore';
		case 'INPUT':
			return element.type !== 'hidden' && element.type !== 'file';
		case 'BUTTON':
			return true;
		case 'SELECT':
			return true;
		case 'TEXTAREA':
			return true;
		default:
			return false;
	}
};

const attemptFocus = (element) => {
	if (!isFocusable(element)) {
		return false;
	}

	ignoreUtilFocusChanges = true;
	try {
		element.focus();
	} catch (e) {
		// continue regardless of error
	}
	ignoreUtilFocusChanges = false;

	return document.activeElement === element;
};

const focusFirstDescendant = (element) => {
	for (let i = 0; i < element.childNodes.length; i++) {
		const child = element.childNodes[i];
		if (attemptFocus(child) || focusFirstDescendant(child)) {
			return true;
		}
	}

	return false;
};

const focusLastDescendant = (element) => {
	for (let i = element.childNodes.length - 1; i >= 0; i--) {
		const child = element.childNodes[i];
		if (attemptFocus(child) || focusLastDescendant(child)) {
			return true;
		}
	}

	return false;
};

class Dialog {
	constructor(node) {
		this.dialogNode = node.childNodes[1];
		this.lastFocus = null;
		this.handleDialogFocus = null;
		this.focusAfterClosedElement = document.body;
	}

	open(focusAfterClosedElement) {
		this.focusAfterClosedElement = focusAfterClosedElement;
		this.handleDialogFocus = this.trapFocus();
		// If this modal is opening on top of one that is already open,
		// get rid of the document focus listener of the open dialog.
		if (dialogElements.length > 1) getCurrentDialog().removeListeners();
		this.addListeners();
		focusFirstDescendant(this.dialogNode);
		this.lastFocus = document.activeElement;
	}

	close() {
		this.removeListeners();
		this.focusAfterClosed();
		openDialogs.delete(this.dialogNode);
		dialogElements.pop();

		// If a dialog was open underneath this one, restore its listeners.
		if (dialogElements.length > 0) {
			getCurrentDialog().addListeners();
		}
	}

	addListeners() {
		this.dialogNode.parentNode.addEventListener('focus', this.handleDialogFocus, true);
	}

	removeListeners() {
		this.dialogNode.parentNode.removeEventListener('focus', this.handleDialogFocus, true);
	}

	trapFocus() {
		return (event) => {
			if (ignoreUtilFocusChanges) return;

			if (this.dialogNode.contains(event.target)) {
				this.lastFocus = event.target;
			} else {
				focusFirstDescendant(this.dialogNode);
				if (this.lastFocus === document.activeElement) {
					focusLastDescendant(this.dialogNode);
				}
				this.lastFocus = document.activeElement;
			}
		};
	}

	focusAfterClosed() {
		this.focusAfterClosedElement && this.focusAfterClosedElement.tagName && this.focusAfterClosedElement.focus();
	}
}

Vue.directive('focusTrap', {
	bind(el, { value, oldValue }) {
		Vue.nextTick(() => {
			const dialog = new Dialog(el);
			openDialogs.set(el, dialog);
			dialogElements.push(el);

			if (value.isOpen) {
				dialog.open(focusAfterClosedElement);
			}
		});
	},

	update(el, { value, oldValue }) {
		if (value.isOpen === oldValue.isOpen) return;

		const dialog = openDialogs.get(el);

		if (value.isOpen) {
			dialog.open(focusAfterClosedElement);
		} else {
			dialog.close();
		}
	},

	unbind(el) {
		openDialogs.get(el) && openDialogs.get(el).close();
	},
});

// AURORA: Register a global custom directive called `v-focus`
Vue.directive('focus', {
	// directive definition
	inserted: function (el,binding) {
	 if(binding.value == "true"){
		 el.focus()
	 }
 }
})
