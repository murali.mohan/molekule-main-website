import createRepository from '~/repositories/Repository';

export default (ctx, inject) => {
	let apiCountry = ctx.store.getters.apiCountry;
	if (!apiCountry) {
		apiCountry = process.env.DEFAULT_COUNTRY_CODE;
	}
	ctx.$axios.interceptors.request.use((config) => {
		config.params = config.params || {};
		if (!config.params.country && !config.url.includes('country=') && !config.url.includes('custom-apis/')) {
			// do-not-pass country code to custom-apis
			config.params.country = apiCountry;
		}
		config.headers.country = apiCountry;
		return config;
	});
	ctx.$axios.interceptors.response.use(
		(res) => {
			const requestUrl = res.config.url;
			const requestPayload = res.config.data;
			if (!requestUrl.includes('api/v1/users?customerSource=')) {
				if (res.data.statusCode === 404) {
					ctx.store.dispatch('USER_STORE/SERVICE_ERROR', { message: res.data.message, data: requestPayload });
				}
			}
			return res;
		},
		(error) => {
			const requestUrl = error.config.url;
			const requestPayload = error.config.data;
			if (
				!requestUrl.includes('api/v1/users?customerSource=') &&
				!requestUrl.includes('api/v1/user/user-attributes') &&
				!requestUrl.includes('api/v1/user/refresh/')
			) {
				if (error.response && error.response.data) {
					ctx.store.dispatch('USER_STORE/SERVICE_ERROR', {
						message: error.response.data.errorMessage || error.response.data.message,
						data: requestPayload,
					});
				}
			}
			return Promise.reject(error.message);
		}
	);
	inject('repositories', createRepository(ctx.$axios, apiCountry));
};
