export default (context, inject) => {
	const getImage = async (productSku, prismicPage = 'b2b_product_detail') => {
		if (!context.productImages) {
			context.productImages = {};
		}
		if (context.productImages[productSku] && context.productImages[productSku] !== '#') {
			return context.productImages[productSku];
		}
		let imageUrl = '#';
		let query = '';
		try {
			query = `?country=${context.store.getters.apiCountry}`;
		} catch (e) {}
		if (productSku) {
			const response = await context.$axios.get(`/custom-apis/prismic-image/${productSku}${query}`);
			if (response && response.data) {
				imageUrl = response.data;
			}
		}

		console.log(productSku, imageUrl);
		context.productImages[productSku] = imageUrl;
		return imageUrl;
	};
	inject('getImage', getImage);
};
