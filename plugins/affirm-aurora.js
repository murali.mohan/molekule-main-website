export default ({ app, query, $config }, inject) => {
	let affrimPayment;
	const init = () => {
		const $locale = app.store.getters.standardLocale;
		if ($locale !== 'en-us') {
			return;
		}
		if (window.affirmInit) {
			return;
		}
		const affirmConfig = {
			public_api_key: $config.affirmPublicKey, // "1VYM05QS77BSXMOD",
			script: 'https://cdn1-sandbox.affirm.com/js/v2/affirm.js',
			session_id: 'sID',
		};
		(function (m, g, n, d, a, e, h, c) {
			const b = m[n] || {};
			const k = document.createElement(e);
			const p = document.getElementsByTagName(e)[0];
			const l = function (a, b, c) {
				return function () {
					a[b]._.push([c, arguments]);
				};
			};
			b[d] = l(b, d, 'set');
			const f = b[d];
			b[a] = {};
			b[a]._ = [];
			f._ = [];
			b._ = [];
			b[a][h] = l(b, a, h);
			b[c] = function () {
				b._.push([h, arguments]);
			};
			a = 0;
			for (c = 'set add save post open empty reset on off trigger ready setProduct'.split(' '); a < c.length; a++)
				f[c[a]] = l(b, d, c[a]);
			a = 0;
			for (c = ['get', 'token', 'url', 'items']; a < c.length; a++) f[c[a]] = function () {};
			k.async = !0;
			k.src = g[e];
			p.parentNode.insertBefore(k, p);
			delete g[e];
			f(g);
			m[n] = b;
		})(window, affirmConfig, 'affirm', 'checkout', 'ui', 'script', 'ready', 'jsReady');
		affrimPayment = window.affirm;
		window.affirmInit = true;
	};
	const checkoutAffirm = async ($obj) => {
		await affrimPayment.checkout({
			merchant: {
				user_confirmation_url: window.location.href,
				user_cancel_url: window.location.href,
				user_confirmation_url_action: 'POST',
				name: 'Your Customer-Facing Merchant Name',
			},
			shipping: {
				name: {
					first: $obj.cart.shippingAddress.firstName,
					last: $obj.cart.shippingAddress.lastName,
				},
				address: {
					line1: $obj.cart.shippingAddress.streetName,
					line2: $obj.cart.shippingAddress.streetName,
					city: $obj.cart.shippingAddress.city,
					state: $obj.cart.shippingAddress.state,
					zipcode: $obj.cart.shippingAddress.postalCode,
					country: $obj.cart.shippingAddress.country,
				},
				phone_number: '2015550123', // + $obj.cart.shippingAddress.phone,
				email: $obj.cart.customerEmail,
			},
			billing: {
				name: {
					first: $obj.billingAddress.firstName || $obj.cart.shippingAddress.firstName,
					last: $obj.billingAddress.lastName || $obj.cart.shippingAddress.lastName,
				},
				address: {
					line1: $obj.billingAddress.streetName || $obj.cart.shippingAddress.streetName,
					line2: $obj.billingAddress.streetName || $obj.cart.shippingAddress.streetName,
					city: $obj.billingAddress.city || $obj.cart.shippingAddress.city,
					state: $obj.billingAddress.state || $obj.cart.shippingAddress.state,
					zipcode: $obj.billingAddress.postalCode || $obj.cart.shippingAddress.postalCode,
					country: $obj.billingAddress.country || $obj.cart.shippingAddress.country,
				},
				phone_number: '2015550123', // + ($obj.billingAddress.phone || $obj.cart.shippingAddress.phone),
				email: $obj.cart.customerEmail,
			},
			items: $obj.items,
			discounts: {
				RETURN5: {
					discount_amount: 500,
					discount_display_name: 'Returning customer 5% discount',
				},
				PRESDAY10: {
					discount_amount: 1000,
					discount_display_name: "President's Day 10% off",
				},
			},
			metadata: {
				shipping_type: $obj.cart.shippingInfo.shippingMethodName,
				mode: 'modal',
			},
			// "order_id":$data.id,
			currency: 'USD',
			financing_program: 'flyus_3z6r12r',
			shipping_amount: $obj.cart.custom.fields.shippingCost,
			tax_amount: $obj.cart.custom.fields.totalTax,
			total: $obj.cart.custom.fields.orderTotal,
		});
	};
	const openAffirm = ($obj, cb) => {
		let $token = '';
		affrimPayment.checkout.open({
			onSuccess(a) {
				$token = a.checkout_token;
				cb($token);
			},
		});
	};
	const refresh = () => {
		affrimPayment.ui.ready(function () {
			affrimPayment.ui.refresh();
		});
	};
	const affirm = {
		init,
		checkoutAffirm,
		openAffirm,
		refresh,
	};
	inject('affirmAurora', affirm);
};
