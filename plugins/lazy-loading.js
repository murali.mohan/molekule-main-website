import Vue from 'vue';
import debounce from 'lodash/debounce';

const elToStatus = new WeakMap();
const prefetchLazyQueue = new Set();
let observer = null;
let shouldPrefetch = false;
let windowLoaded = false;

class PrefetchQueue {
	isWorking = false;
	queue = [];

	wait(cb) {
		if (!this.isWorking) {
			this.isWorking = true;
			cb();
			return;
		}

		this.queue.push(cb);
	}

	done() {
		this.isWorking = false;

		if (this.queue.length > 0) {
			const cb = this.queue.shift();
			this.isWorking = true;
			cb();
		}
	}
}

const prefetchQueue = new PrefetchQueue();

function once(fn) {
	let called = false;
	return function () {
		if (!called) {
			called = true;
			fn.apply(this, arguments);
		}
	};
}

function schedulePrefetch(el) {
	if (!shouldPrefetch) return;
	if (windowLoaded) {
		prefetchEverythingDebounced();
	}
	prefetchLazyQueue.add(el);
}

function stopPrefetch(el) {
	prefetchLazyQueue.delete(el);
}

function prefetch(el) {
	const done = once(() => {
		prefetchQueue.done();
	});

	prefetchQueue.wait(() => {
		applyIfNecessary(elToStatus.get(el), done);
		setTimeout(done, 5000);
	});
}

function prefetchEverything() {
	// console.log('Everything');
	Array.from(prefetchLazyQueue).forEach((el) => {
		prefetch(el);
	});
	prefetchLazyQueue.clear();
}

const prefetchEverythingDebounced = debounce(prefetchEverything, 1000);

function observerCallback(entries) {
	entries.forEach(({ isIntersecting, target }) => {
		if (!isIntersecting) return;
		observer.unobserve(target);
		const status = elToStatus.get(target);
		applyIfNecessary(status);
	});
}

function callbackForEl(el, cb, isIframe = false) {
	// console.log(el.naturalWidth);
	if (!cb) return;
	if (el.complete && el.naturalWidth > 1) {
		// console.log('fast ready');
		cb();
		return;
	}
	// const start = performance.now();

	// The 'error' event listener doesn't work on iframes
	// so we're using 'isIframe' to avoid adding the event listener
	function localCb() {
		el.removeEventListener('load', localCb, false);
		if (!isIframe) el.removeEventListener('error', localCb, false);
		// console.log('ready in', performance.now() - start);
		cb();
	}
	el.addEventListener('load', localCb, false);
	if (!isIframe) el.addEventListener('error', localCb, false);
}

function applyIfNecessary(status, cb) {
	const cbOnce = once(() => {
		if (cb) cb();
		status.lazyloaded();
	});
	if (!status) {
		cbOnce();
		return;
	}

	const { el, binding, applied, vnode } = status;

	const {
		modifiers: { /* video, */ backgroundImage, picture, iframe },
	} = binding;

	const src = typeof binding.value === 'string' ? binding.value : binding.value ? binding.value.src || '' : '';

	if (backgroundImage) {
		const url = `url("${src}")`;
		if ((src && !applied) || url !== el.style.backgroundImage) {
			el.style.backgroundImage = url;
			status.applied = true;
			const img = new Image();
			callbackForEl(img, cbOnce);
			img.src = src;
		} else {
			cbOnce();
		}
	} else if (picture) {
		if (process.env.NODE_ENV !== 'production' && console.warn && !el.hasAttribute('sizes')) {
			console.warn('v-lazy: Requested prefetch for image but sizes is not set', el);
		}

		if (vnode && vnode.context) {
			vnode.context.preloaded = true;
			Vue.nextTick(() => {
				if (el.complete && el.naturalWidth) {
					cbOnce();
				} else {
					callbackForEl(el, cbOnce);
				}
			});
			return;
		}

		status.applied = true;
	} else if (iframe) {
		let isTheSame = false;

		try {
			isTheSame = src === el.contentWindow.location.href;
		} catch {
			isTheSame = src === el.getAttribute('src');
		}

		if ((src && !applied) || !isTheSame) {
			if (!isTheSame) {
				el.src = src;
				callbackForEl(el, cbOnce, true);
			} else {
				cbOnce();
			}
			status.applied = true;
		} else {
			cbOnce();
		}
	} else {
		// image or video
		const isTheSame = src === el.getAttribute('src');
		if ((src && !applied) || !isTheSame) {
			if (!isTheSame) {
				el.setAttribute('src', src);
				callbackForEl(el, cbOnce);
			} else {
				cbOnce();
			}
			status.applied = true;
		} else {
			cbOnce();
		}
	}

	observer.unobserve(el);
}

Vue.directive('lazy', {
	bind(el, binding, vnode) {
		// console.log('bind', { el, binding, vnode });
		const lazyloaded = new CustomEvent('lazyloaded');
		const status = {
			el,
			binding,
			applied: false,
			prefetch: !binding.value || binding.value.prefetch !== false,
			vnode,
			lazyloaded: () => el.dispatchEvent(lazyloaded),
		};
		// console.log('binding', el.getAttribute('data-src') || el.getAttribute('src'));
		elToStatus.set(el, status);
		if (binding.value && binding.value.critical) {
			applyIfNecessary(status);
		} else {
			observer.observe(el);
			if (status.prefetch) {
				schedulePrefetch(el);
			}
		}
	},

	update(el, binding, vnode) {
		// console.log('update', { el, binding, vnode });
		const status = elToStatus.get(el);
		if (!status) throw new Error('status not found');
		if (vnode.context.preloaded) return;
		status.binding = binding;
		status.vnode = vnode;

		if (status.applied) {
			applyIfNecessary(status);
		} else {
			// console.log('update not applying');
		}
	},

	unbind(el, binding, vnode) {
		// console.log('unbind', { el, binding, vnode });
		stopPrefetch(el);
		observer.unobserve(el);
	},
});

export default function initializeLazyLoading({ query }) {
	observer = new IntersectionObserver(observerCallback, { rootMargin: '300px' });

	if (query.perf && query.perf.split(',').includes('no-prefetch-images')) return;
	const { connection: conn } = navigator;
	const hasBadConnection = conn && ((conn.effectiveType || '').includes('2g') || conn.saveData);
	if (hasBadConnection) return;
	shouldPrefetch = true;

	window.addEventListener('load', () => {
		function initPrefetch() {
			prefetchEverything();
			windowLoaded = true;
		}

		if (window.requestIdleCallback) {
			window.requestIdleCallback(initPrefetch);
			// setTimeout(initPrefetch, 5000);
		} else {
			initPrefetch();
		}
	});

	// return new Promise((resolve) => {
	// 	window.ress = resolve;
	// });
}
