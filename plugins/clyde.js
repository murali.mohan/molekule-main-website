export default ({ app, query, $config }, inject) => {
  let clydeProtection
  const init = (self) => {
    if (window.clydeInit) {
      return
    }
    const clydeScript = document.createElement('script')
    clydeScript.setAttribute('src', $config.clydeUrl)
    clydeScript.async = true
    clydeScript.onload = () => {
      clydeProtection = window.Clyde ? window.Clyde : Clyde // eslint-disable-line
      clydeProtection.init(
        {
          key: $config.clydeKey,
          type: 'list',
          skipGeoIp: true,
          skuList: [
            'SQ2P-US',
            'MH1-BBB-1',
            'MN1B-US',
            'MN2P-US',
            'MH1_Device_US',
            'airpro-us',
            'AIRMINICOHORT1',
          ],
        },
        async function () {}
      )
    }
    document.head.appendChild(clydeScript)
    window.clydeInit = true
  }
  const setActiveProduct = async () => {}
  const showModal = async () => {}
  const getContract = async () => {}
  const getClyde = async ($sku, cb) => {
    await clydeProtection.setActiveProduct($sku)
    clydeProtection.showModal(null, async function () {
      const $selectedContract = await clydeProtection.getSelectedContract()
      cb($selectedContract)
      // return $selectedContract
    })
  }
  const clyde = {
    init,
    setActiveProduct,
    showModal,
    getContract,
    getClyde,
  }
  inject('clyde', clyde)
}
