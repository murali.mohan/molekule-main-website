import Axios from 'axios';

export default function axiosMagentoInstance({ store, $axios }, inject) {
	const magentoUrl = process.env.magento.pathname;
	const { urlPrefix } = store.getters['magento/locationStore'];
	const instance = Axios.create({
		baseURL: `${magentoUrl}${urlPrefix}rest/V1`,
	});

	for (const method of ['get', 'post', 'put', 'delete', 'head', 'options', 'request']) {
		instance['$' + method] = function () {
			return this[method].apply(this, arguments).then((res) => res && res.data);
		}.bind(instance);
	}

	instance.interceptors.request.use(
		function requestSuccessInterceptor(config) {
			if (config.url.includes('/carts/mine')) {
				config.headers['x-requested-with'] = `XMLHttpRequest`;
			}
			return config;
		},
		(err) => Promise.reject(err)
	);

	instance.interceptors.response.use(
		(res) => res,
		(err) => {
			if (
				process.client &&
				!(
					err.config &&
					((err.config.method === 'get' &&
						err.response &&
						((err.config.url.includes('/carts/mine') && (err.response.status === 401 || err.response.status === 404)) ||
							(err.config.url.includes('/guest-carts/') && err.response.status === 404))) ||
						(err.config.method === 'put' &&
							err.response &&
							err.config.url.includes('/coupons/') &&
							err.response.status === 404))
				)
			) {
				if (
					err.response &&
					err.response.data &&
					err.response.data.message &&
					err.response.data.message.includes('qty')
				) {
					alert(err.response.data.message);
				} else {
					alert('Unexpected error occurred. Please try again in a few minutes.');
				}
			}
			return Promise.reject(err);
		}
	);

	inject('axiosMagento', instance);
}
