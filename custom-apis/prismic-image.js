import Prismic from 'prismic-javascript';
import { europeanPrismicLocale, europeanCountries } from '../utils/locale-helpers';

export default async function prismicImage(ctx) {
	try {
		const { PRISMIC_ENDPOINT, PRISMIC_ACCESS_TOKEN } = process.env;
		let localeValue = 'en-us';
		if (ctx.query && ctx.query.country) {
			const country = ctx.query.country;
			if (country === 'GB') {
				localeValue = 'en-gb';
			}
			if (europeanCountries.includes(country)) {
				localeValue = europeanPrismicLocale;
			}
		}
		const sectionValue = 'ct_cart_descriptions';
		const documentTags = ['b2b_global_tag'];
		let sku = '';
		let productImage = '';
		if (ctx.params) {
			sku = ctx.params[0];
		}

		const api = await Prismic.getApi(PRISMIC_ENDPOINT, { req: ctx.request, accessToken: PRISMIC_ACCESS_TOKEN });
		const response = await api.query(Prismic.Predicates.at('document.tags', documentTags), {
			page: 1,
			lang: localeValue,
		});

		const { results } = response;
		if (results.length && results[0].data && results[0].data[sectionValue]) {
			const allValues = results[0].data[sectionValue];
			const skuRecord = allValues.filter((item) => item.sku && item.sku.toString().toLowerCase() === sku.toLowerCase());
			if (skuRecord.length) {
				productImage = skuRecord[0].side_cart_thumbnail.url;
				productImage = productImage.replace('https://images.prismic.io/', '/prismic-images/');
			}
		}
		ctx.append('Cache-Control', 'public, max-age=900');
		ctx.body = productImage;
	} catch (e) {
		ctx.body = '#';
	}
}
