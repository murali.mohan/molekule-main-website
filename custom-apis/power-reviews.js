import cachedRequest from './utils/cached-request';

const ONE_HOUR = 60 * 60;

const PREFIX = '/power-reviews/';
const PREFIX_LENGTH = PREFIX.length;
const { POWERREVIEWS_READ_URL } = process.env;

export default async function powerReviews(ctx) {
	const urlWithoutPrefix = ctx.url.substr(PREFIX_LENGTH);
	try {
		ctx.body = await cachedRequest(`${POWERREVIEWS_READ_URL}/${urlWithoutPrefix}`, { ttl: ONE_HOUR, grace: ONE_HOUR });
		ctx.append('Cache-Control', `public, max-age=${ONE_HOUR}`);
	} catch (e) {
		if (!(e.response && e.response.status)) throw e;
		const { response } = e;
		ctx.status = response.status;
		ctx.message = response.statusText;
		ctx.body = response.data;
	}
}
