import { getAxiosInstanceForStore } from './axios';

export function randomNumberBetween(min, max) {
	return min + Math.floor(Math.random() * (max - min + 1));
}

const BUFFER_SIZE = process.env.NODE_ENV === 'production' ? 5 : 2;
const MAX_NO_REFRESH_TIME = 15 * 60 * 1000;

export default class CartsBuffer {
	constructor({ store, id, avoidStale = false }) {
		this.store = store;
		this.axios = getAxiosInstanceForStore(store);
		this.id = id;
		this.buffer = [];
		this.refillTimeout = 0;
		this.isRefilling = false;
		this.refillBound = this.refillBound.bind(this);
		this.lastRefillTime = 0;
		this.avoidStale = avoidStale;
		this.initialized = false;
	}

	initialize() {
		this.initialized = true;

		if (this.avoidStale) {
			setTimeout(() => {
				setInterval(() => this.refreshToAvoidStaleIfNecessary(), MAX_NO_REFRESH_TIME);
			}, randomNumberBetween(10, 60) * 1000);
		}
	}

	get isEmpty() {
		return this.buffer.length === 0;
	}

	get() {
		const val = this.buffer.shift();
		this.scheduleRefillIfNecessary(true);
		return val;
	}

	scheduleRefillIfNecessary(fast = false) {
		console.log(`Carts buffer ${this.store} ${this.id}: ${this.buffer.length}`);
		if (!this.initialized) this.initialize();
		if (this.refillTimeout || this.isRefilling) return;
		if (this.buffer.length < BUFFER_SIZE) {
			const timeSeconds = fast ? randomNumberBetween(1, 3) : randomNumberBetween(5, 15);
			this.refillTimeout = setTimeout(this.refillBound, timeSeconds * 1000);
		}
	}

	// used to decide if reset is necessary
	isEqual(a, b) {
		return true;
	}

	resetIfNecessary() {
		const { buffer } = this;
		const { length: len } = buffer;
		if (len < 2) return;
		if (!this.isEqual(buffer[len - 1], buffer[len - 2])) {
			const toRemove = len - 1;
			console.log(`Carts buffer ${this.store} ${this.id}: clearing ${toRemove} items due to detected change`);
			for (let i = 0; i < toRemove; i += 1) {
				buffer.shift();
			}
		}
	}

	async refillBound() {
		this.refillTimeout = 0;
		this.isRefilling = true;
		try {
			const val = await this.refill();
			this.buffer.push(val);
			if (this.buffer.length === 1 && this.firstItemInBufferResolve) {
				this.firstItemInBufferResolve(val);
			}
			this.lastRefillTime = Date.now();
			this.resetIfNecessary();
		} catch (e) {
			console.error(`Error when refilling ${this.store} ${this.id}`, e);
		} finally {
			this.isRefilling = false;
			this.scheduleRefillIfNecessary();
		}
	}

	// To make sure that outdated carts are not served for long time
	// one in a while we check if the when the last cart was downloaded
	// and trigger get if necessary
	refreshToAvoidStaleIfNecessary() {
		const { lastRefillTime } = this;
		try {
			if (!(lastRefillTime && Date.now() - lastRefillTime > MAX_NO_REFRESH_TIME)) return;
			console.log(`Carts buffer ${this.store} ${this.id}: triggering get due to long idle time`);
			this.get();
		} catch (error) {
			console.log('Error while refresh stale carts');
			console.error(error);
		}
	}
}
