import CloudFront from 'aws-sdk/clients/cloudfront';

let cloudFront;

const { CACHE_AWS_ACCESS_KEY_ID, CACHE_AWS_SECRET_ACCESS_KEY, CACHE_CLOUDFRONT_ID } = process.env;

if (CACHE_AWS_ACCESS_KEY_ID && CACHE_AWS_SECRET_ACCESS_KEY && CACHE_CLOUDFRONT_ID) {
	cloudFront = new CloudFront({
		accessKeyId: CACHE_AWS_ACCESS_KEY_ID,
		secretAccessKey: CACHE_AWS_SECRET_ACCESS_KEY,
	});
}

export default function invalidateCloudFrontCache() {
	if (cloudFront) {
		cloudFront.createInvalidation(
			{
				DistributionId: CACHE_CLOUDFRONT_ID,
				InvalidationBatch: {
					CallerReference: `${Date.now()}`,
					Paths: {
						Quantity: 1,
						Items: ['/*'],
					},
				},
			},
			(err, res) => {
				console.log({
					err,
					res,
				});
				console.error('Cloudfront invalidation error', err);
			}
		);
	} else {
		console.log('Skipping invalidation as CloudFront configuration is missing');
	}
}
