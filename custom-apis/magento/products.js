import isEqual from 'lodash/isEqual';
import { eachLimit } from 'async-es';
import { getAxiosInstanceForStore } from './axios';
import { isNotBuild, isDev } from './utils';
import invalidateCloudFrontCache from './invalidate-cloudfront-cache';

const storesData = {
	US: {
		id: 1,
		currencyCode: 'USD',
	},
	CA: {
		id: 4,
		currencyCode: 'CAD',
	},
};

function findBestImage(images) {
	const selected = images.find((image) => image.code === 'recently_viewed_products_list_content_widget');
	if (selected) return selected;

	const square = images.find((image) => image.width === image.height);
	if (square) return selected;

	return images[0];
}

function analyzeProductOption(options) {
	if (!options) {
		return {
			hasSubscription: false,
		};
	}

	const optionsSimple = options.map((option) => ({
		option_id: option.option_id,
		option_qty: 1,
		option_selections: (option.product_links || []).map(({ id }) => parseInt(id)),
	}));

	const subscription = options.find((option) => option.title === 'Subscription');

	return {
		productOption: {
			extension_attributes: {
				bundle_options: optionsSimple,
			},
		},
		hasSubscription: Boolean(subscription),
	};
}

class ProductsDataForStore {
	constructor(store, axios) {
		this.store = store;
		this.storeData = storesData[store];
		this.axios = axios;
		this.load = this.load.bind(this);
		this.productsData = null;
		this.loadTime = 0;
		this.lastUpdate = Date.now();

		if (isNotBuild && false) {
			this.productsDataPromise = this.load();
			setTimeout(() => {
				setInterval(this.load, 5 * 60 * 1000);
			}, ((isDev ? 120 : 10) + Math.floor(Math.random() * 60)) * 1000);
		}
	}

	async get() {
		if (!this.productsData) {
			await this.productsDataPromise;
		}

		return {
			products: this.productsData,
			productsTime: this.loadTime,
			productsLastUpdate: this.lastUpdate,
		};
	}

	async load() {
		const { id, currencyCode } = this.storeData;
		const productsRenderInfoResponse = await this.axios.$get(
			`/products-render-info?currencyCode=${currencyCode}&searchCriteria=[]&storeId=${id}`
		);

		// We're putting initial and downloaded later items together in case some
		// individual download will not return item, latest item will same SKU be used
		const items = [...productsRenderInfoResponse.items];

		await eachLimit(productsRenderInfoResponse.items, 5, async ({ sku }) => {
			const params = {
				currencyCode,
				storeId: id,
				'searchCriteria[filter_groups][0][filters][0][field]': 'sku',
				'searchCriteria[filter_groups][0][filters][0][condition_type]': 'eq',
				'searchCriteria[filter_groups][0][filters][0][value]': sku,
			};

			const singleData = await this.axios.$get('/products-render-info', {
				params,
			});
			items.push(...singleData.items);
		});

		const products = {};

		items.forEach((item) => {
			products[item.sku] = {
				sku: item.sku,
				id: item.id,
				type: item.type,
				associatedProductSku: item.associated_product_sku,
				name: item.name,
				price: item.price_info.final_price,
				image: findBestImage(item.images),
				...analyzeProductOption(item.bundle_product_options),
			};
		});

		if (this.productsData && !isEqual(this.productsData, products)) {
			console.log(`Products difference found for ${this.store}, clearing cache`);
			this.lastUpdate = Date.now();
			invalidateCloudFrontCache();
		}

		this.loadTime = Date.now();
		this.productsData = products;
		this.productsDataPromise = null;
	}

	async loadSafe() {}
}

const productsData = {
	US: new ProductsDataForStore('US', getAxiosInstanceForStore('US')),
	CA: new ProductsDataForStore('CA', getAxiosInstanceForStore('CA')),
};

export async function getProductsData(store) {
	return await productsData[store].get();
}

export default async function products(ctx) {
	ctx.body = { ...(await productsData[ctx.params.store].get()) };
}
