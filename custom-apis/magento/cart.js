import CartsBuffer from './carts-buffer';
import { isNotBuild, isDev } from './utils';

export const CART_HASH_CREATE =
	(process.env.magento && process.env.magento.cartHashCreate) || process.env.MAGENTO_CART_HASH_CREATE;

if (!CART_HASH_CREATE) {
	throw new Error('Magento cart hash create not provided');
}

class GuestCartBuffer extends CartsBuffer {
	constructor(store) {
		super({
			store,
			id: 'empty',
		});
	}

	async refill() {
		try {
			return await createCart(this.axios);
		} catch (error) {
			console.error(error);
		}
	}
}

const guestCartsBuffers = {
	US: new GuestCartBuffer('US'),
	CA: new GuestCartBuffer('CA'),
};

if (isNotBuild && !isDev) {
	guestCartsBuffers.US.scheduleRefillIfNecessary();
	guestCartsBuffers.CA.scheduleRefillIfNecessary();
}

export async function getOrCreateCart(store, axios) {
	try {
		return guestCartsBuffers[store].get() || (await createCart(axios));
	} catch (error) {
		console.error('Unable to initialize cart');
		console.error(error);
	}
}

export async function createCart(axios) {
	try {
		return await axios.$post('/guest-carts');
	} catch (error) {
		console.error('Error while creating Guest Cart');
		console.error(error);
	}
}

export async function ensureAndValidateCartMiddleware(ctx, next) {
	try {
		let { cartHash } = ctx.params;

		if (cartHash === CART_HASH_CREATE) {
			cartHash = await getOrCreateCart(ctx.params.store, ctx.axiosMagento);
		}

		ctx.state.cartHash = cartHash;

		await next();

		if (ctx.params.cartHash !== cartHash && typeof ctx.body === 'object') {
			ctx.body.cartHash = cartHash;
		}
	} catch (err) {
		console.error(err);
	}
}

export async function loadCartByHash(axios, hash) {
	try {
		const carts = await axios.$get(`/cart/cartdetails/${hash}`);
		return carts[0];
	} catch (error) {
		console.error(error);
	}
}

export async function loadCart(ctx) {
	try {
		const { axiosMagento: axios } = ctx;
		const { cartHash } = ctx.state;
		return await loadCartByHash(axios, cartHash);
	} catch (error) {
		console.error(error);
	}
}
