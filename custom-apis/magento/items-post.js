import Joi from '@hapi/joi';
import { validateBody } from '../validation';
import { loadCart, createCart, loadCartByHash, CART_HASH_CREATE } from './cart';
import CartsBuffer from './carts-buffer';
import { getProductsData } from './products';
import { isNotBuild, isDev } from './utils';

const cartItemSchema = Joi.object({
	sku: Joi.string().required(),
	qty: Joi.number().positive().required(),
	product_option: Joi.object({
		extension_attributes: Joi.object({
			bundle_options: Joi.array()
				.items({
					option_id: Joi.number().required(),
					option_qty: Joi.number().required(),
					option_selections: Joi.array().items(Joi.number()).required(),
				})
				.required(),
		}).required(),
	}),
});

function sortByField(property) {
	return function sorterByField(a, b) {
		const aP = a[property];
		const bP = b[property];
		if (typeof aP === 'number' && typeof bP === 'number') {
			return aP - bP;
		}
		return String(aP).localeCompare(String(bP));
	};
}

const sortBySku = sortByField('sku');
const sortByOptionId = sortByField('option_id');

export function generateId(requestBody) {
	return requestBody.cartItems
		.sort(sortBySku)
		.map((item) => {
			const options = item.product_option
				? item.product_option.extension_attributes.bundle_options
						.sort(sortByOptionId)
						.map((o) => `${o.option_id}.${o.option_qty}.${o.option_selections.sort().join(',')}`)
						.join('-')
				: '';
			return `${item.sku}--${item.qty}--${options}`;
		})
		.join('-');
}

async function addToCart({ body, cartHash, axios }) {
	const { cartItems: items } = body;
	// this should ge in sync with code in /store/magento.js
	let lastItem;
	try {
		for (const item of items) {
			lastItem = await axios.$post(`/guest-carts/${cartHash}/items`, {
				cartItem: {
					...item,
					quote_id: cartHash,
				},
			});
		}
	} catch (err) {
		console.error(err);
	}

	// based on email from Mark (In the meantime we recommend a temporary fix on the frontend. ..., 2019-10-02/03)
	// we update quantity to the same to force totals/discount recalculation on the backend
	try {
		await axios.$put(`/guest-carts/${cartHash}/items/${lastItem.item_id}`, {
			cartItem: {
				qty: lastItem.qty,
				quote_id: cartHash,
			},
		});
	} catch (err) {
		console.error(err);
	}
}

let createdBuffersResolve;
const maybeResolve = ((resolve) => {
	let count = 0;
	return (resolve) => {
		count++;
		if (count === 2) resolve();
	};
})();
const createdBuffersPromise = new Promise((resolve) => (createdBuffersResolve = resolve));
const cartsWithProducts = {};
class CartWithProductsBuffer extends CartsBuffer {
	static create(...args) {
		const c = new CartWithProductsBuffer(...args);
		if (isNotBuild && !isDev) {
			c.scheduleRefillIfNecessary();
		}
		return c;
	}

	constructor(store, requestBody) {
		const id = generateId(requestBody);
		super({
			store,
			id,
			avoidStale: true,
		});
		this.requestBody = requestBody;
		this.firstItemInBufferPromise = new Promise((resolve) => (this.firstItemInBufferResolve = resolve));
		cartsWithProducts[this.id] = this;
	}

	async refill() {
		try {
			const cartHash = await createCart(this.axios);
			await addToCart({
				body: this.requestBody,
				cartHash,
				axios: this.axios,
			});
			return {
				cart: {
					hash: cartHash,
					...(await loadCartByHash(this.axios, cartHash)),
				},
			};
		} catch (err) {
			console.error(err);
		}
	}

	isEqual(a, b) {
		return a.cart.totals.grand_total === b.cart.totals.grand_total;
	}
}

const itemsPostSchema = Joi.object({
	cartItems: Joi.array().items(cartItemSchema),
	couponCode: Joi.string(),
});

export async function itemsPostValidateBody(ctx, next) {
	try {
		await validateBody(ctx, itemsPostSchema);
	} catch (err) {
		console.error(err);
	}
	await next();
}

export async function itemsPostFast(ctx, next) {
	const { cartHash } = ctx.params;
	if (cartHash === CART_HASH_CREATE) {
		const productsId = generateId(ctx.request.body);
		const preCreated = cartsWithProducts[productsId] && cartsWithProducts[productsId].get();
		if (preCreated) {
			let { cart } = preCreated;
			const cartHash = preCreated.cart.hash;
			const { couponCode } = ctx.request.body;
			let couponCodeError;

			if (couponCode) {
				const couponCodeEncoded = encodeURIComponent(couponCode);
				const { axiosMagento: axios } = ctx;
				await axios.$put(`/guest-carts/${cartHash}/coupons/${couponCodeEncoded}`).catch(() => {
					couponCodeError = true;
				});

				try {
					cart = await loadCartByHash(axios, cartHash);
				} catch (err) {
					console.error(err);
				}
			}

			ctx.body = {
				...preCreated,
				cart: {
					...cart,
					hash: undefined,
				},
				couponCodeError,
				cartPreCreated: true,
				cartHash,
			};
			return;
		}
	}

	await next();
}

export async function itemsPost(ctx) {
	const { axiosMagento: axios, request } = ctx;
	const { body } = request;
	const { cartHash } = ctx.state;

	try {
		await addToCart({
			axios,
			cartHash,
			body,
		});
	} catch (e) {
		if (
			e.response &&
			e.response.status === 400 &&
			e.response.data &&
			e.response.data.message &&
			e.response.data.message.includes('qty')
		) {
			ctx.throw(400, JSON.stringify(e.response.data));
		} else {
			throw e;
		}
	}

	const { couponCode } = body;
	let couponCodeError;

	if (couponCode) {
		const couponCodeEncoded = encodeURIComponent(couponCode);
		await axios.$put(`/guest-carts/${cartHash}/coupons/${couponCodeEncoded}`).catch(() => {
			couponCodeError = true;
		});
	}

	ctx.body = {
		cart: await loadCart(ctx),
		couponCodeError,
	};
}

async function getProductsDataHandled(store) {
	try {
		return await getProductsData(store);
	} catch (e) {
		console.error(e);
		console.error('Failed when loading product data for pre-generating carts');
		process.exit(1);
	}
}

if (isNotBuild) {
	getProductsDataHandled('US').then(({ products }) => {
		if (products.MH1_Device_US) {
			CartWithProductsBuffer.create('US', {
				cartItems: [
					{
						sku: 'MH1_Device_US',
						qty: 1,
						product_option: products.MH1_Device_US.productOption,
					},
				],
			});
		}

		if (products.MinimoB_US) {
			CartWithProductsBuffer.create('US', {
				cartItems: [
					{
						sku: 'MinimoB_US',
						qty: 1,
						product_option: products.MinimoB_US.productOption,
					},
				],
			});
			CartWithProductsBuffer.create('US', {
				cartItems: [
					{
						sku: 'MinimoB_US',
						qty: 2,
						product_option: products.MinimoB_US.productOption,
					},
				],
			});
		}

		if (products.MinimoP_US) {
			CartWithProductsBuffer.create('US', {
				cartItems: [
					{
						sku: 'MinimoP_US',
						qty: 1,
						product_option: products.MinimoP_US.productOption,
					},
				],
			});
			CartWithProductsBuffer.create('US', {
				cartItems: [
					{
						sku: 'MinimoP_US',
						qty: 2,
						product_option: products.MinimoP_US.productOption,
					},
				],
			});
		}

		if (products.MH1_Device_US && products.MinimoB_US) {
			CartWithProductsBuffer.create('US', {
				cartItems: [
					{
						sku: 'MH1_Device_US',
						qty: 1,
						product_option: products.MH1_Device_US.productOption,
					},
					{
						sku: 'MinimoB_US',
						qty: 1,
						product_option: products.MinimoB_US.productOption,
					},
				],
			});
			CartWithProductsBuffer.create('US', {
				cartItems: [
					{
						sku: 'MH1_Device_US',
						qty: 1,
						product_option: products.MH1_Device_US.productOption,
					},
					{
						sku: 'MinimoB_US',
						qty: 2,
						product_option: products.MinimoB_US.productOption,
					},
				],
			});
		}

		if (products.MH1_Device_US && products.MinimoP_US) {
			CartWithProductsBuffer.create('US', {
				cartItems: [
					{
						sku: 'MH1_Device_US',
						qty: 1,
						product_option: products.MH1_Device_US.productOption,
					},
					{
						sku: 'MinimoP_US',
						qty: 1,
						product_option: products.MinimoP_US.productOption,
					},
				],
			});
			CartWithProductsBuffer.create('US', {
				cartItems: [
					{
						sku: 'MH1_Device_US',
						qty: 1,
						product_option: products.MH1_Device_US.productOption,
					},
					{
						sku: 'MinimoP_US',
						qty: 2,
						product_option: products.MinimoP_US.productOption,
					},
				],
			});
		}

		if (products['airpro-us']) {
			CartWithProductsBuffer.create('US', {
				cartItems: [
					{
						sku: 'airpro-us',
						qty: 1,
						product_option: products['airpro-us'].productOption,
					},
				],
			});
		}

		maybeResolve(createdBuffersResolve);
	});

	getProductsDataHandled('CA').then(({ products }) => {
		if (products.MH1_Device_CA) {
			CartWithProductsBuffer.create('CA', {
				cartItems: [
					{
						sku: 'MH1_Device_CA',
						qty: 1,
						product_option: products.MH1_Device_CA.productOption,
					},
				],
			});
		}

		if (products.MinimoP_CA) {
			CartWithProductsBuffer.create('CA', {
				cartItems: [
					{
						sku: 'MinimoP_CA',
						qty: 1,
						product_option: products.MinimoP_CA.productOption,
					},
				],
			});
			CartWithProductsBuffer.create('CA', {
				cartItems: [
					{
						sku: 'MinimoP_CA',
						qty: 2,
						product_option: products.MinimoP_CA.productOption,
					},
				],
			});
		}

		if (products.MH1_Device_CA && products.MinimoP_CA) {
			CartWithProductsBuffer.create('CA', {
				cartItems: [
					{
						sku: 'MH1_Device_CA',
						qty: 1,
						product_option: products.MH1_Device_CA.productOption,
					},
					{
						sku: 'MinimoP_CA',
						qty: 1,
						product_option: products.MinimoP_CA.productOption,
					},
				],
			});
			CartWithProductsBuffer.create('CA', {
				cartItems: [
					{
						sku: 'MH1_Device_CA',
						qty: 1,
						product_option: products.MH1_Device_CA.productOption,
					},
					{
						sku: 'MinimoP_CA',
						qty: 2,
						product_option: products.MinimoP_CA.productOption,
					},
				],
			});
		}

		maybeResolve(createdBuffersResolve);
	});
}

export { cartsWithProducts, createdBuffersPromise };
