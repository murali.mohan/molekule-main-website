/* global BigInt */

import Axios from 'axios';

const requestsTiming = new WeakMap();

function createAxios({ urlPrefix, ctx }) {
	const magentoUrl = process.env.MAGENTO_URL;
	const instance = Axios.create({
		baseURL: `${magentoUrl}${urlPrefix}rest/V1`,
		timeout: 15 * 10000,
	});

	instance.interceptors.request.use(
		function requestSuccessInterceptor(config) {
			// console.log(config);
			requestsTiming.set(config, process.hrtime.bigint());

			// if (config.url.startsWith('/')) {
			// 	config.headers.Authorization = `Bearer ${process.env.MAGENTO_ACCESS_TOKEN}`;
			// }
			return config;
		},
		function (error) {
			console.error(error);
			return Promise.reject(error);
		}
	);

	instance.interceptors.response.use(
		function responseSuccessInterceptor(response) {
			const start = requestsTiming.get(response.config);
			if (start) {
				const diff = Number((process.hrtime.bigint() - start) / BigInt(1000)) / 1000;

				if (ctx) {
					const { method, url, baseURL } = response.config;
					ctx.axiosTimings.push([`${method} ${url.replace(baseURL, '')}`, diff]);
				}
			}
			// console.log(response.config);
			return response;
		},
		function responseErrorInterceptor(error) {
			console.error(error.config && error.config.url);
			console.error(error.response && error.response.status);
			console.error(error.response && error.response.data);
			return Promise.reject(error);
		}
	);

	for (const method of ['get', 'post', 'put', 'delete', 'head', 'options', 'request']) {
		instance['$' + method] = function () {
			return this[method].apply(this, arguments).then((res) => res && res.data);
		}.bind(instance);
	}

	return instance;
}

const axiosPrefixes = {
	US: '',
	CA: 'ca-en/',
};

const axiosInstances = {
	US: createAxios({ urlPrefix: '' }),
	CA: createAxios({ urlPrefix: 'ca-en/' }),
};

export async function initializeAxiosMiddleware(ctx, next) {
	ctx.axiosTimings = [];
	ctx.axiosMagento = createAxios({ urlPrefix: axiosPrefixes[ctx.params.store], ctx });
	await next();
	if (ctx.axiosTimings.length > 0) {
		ctx.append('Server-Timing', ctx.axiosTimings.map(([req, time]) => `request;desc="${req}";dur=${time}`).join(', '));
	}
}

export function getAxiosInstanceForStore(store) {
	return axiosInstances[store];
}
