function handleValidationErrors(ctx, e) {
	ctx.throw(400, Array.isArray(e.details) ? e.details.map((e) => e.message).join(', ') : undefined);
}

export function createValidateParamsMiddleware(schema, options) {
	return async function validateParamsMiddleware(ctx, next) {
		try {
			const value = await schema.validateAsync(ctx.params, { abortEarly: false, allowUnknown: true });
			ctx.request.params = ctx.params = value;
		} catch (e) {
			handleValidationErrors(ctx, e);
		}
		await next();
	};
}

export async function validateBody(ctx, schema, options) {
	try {
		const value = await schema.validateAsync(ctx.request.body, { abortEarly: false, ...options });
		ctx.request.body = value;
	} catch (e) {
		handleValidationErrors(ctx, e);
	}
}
