import Router from '@koa/router';
import { prismicGlobalMiddleware, prismicDataMiddleware } from './middleware';

const router = new Router({ prefix: '/:locale(en-us|en-ca|ja-jp|en-in|en-gb|en-eu|en-gr)?' });

router.get('/global', prismicGlobalMiddleware).get('/:type?/:uid', prismicDataMiddleware);

export default router;
