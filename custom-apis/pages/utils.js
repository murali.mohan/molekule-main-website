import axios from 'axios';
import { camelCase, pascalCase, fromEntries, asArray } from '../../utils';
import { slicesDataLoaders } from './slices-data';

export const anattaPrismicRequest = async ({ locale, type, uid }, ctx) => {
	try {
		return (
			await axios.get(`/api/${locale || 'en-us'}/${uid && !type ? uid : `${type}/${uid}`}.json`, {
				headers: {
					Cookie: ctx.headers.cookie || '',
				},
				baseURL: process.env.CUSTOM_APIS_BASE_URL || 'http://localhost:3000',
			})
		).data;
	} catch (error) {
		console.error(error);
	}
};

export const processRegularPrismicDoc = async ({ url, params, headers, prismicUid = '' }, doc = {}) =>
	!Object.keys(doc).length
		? {}
		: await asyncPipe(
				processDoc({
					url,
					params,
					headers,
					prismicUid,
				})
		  )(doc);

export const processGlobalPrismicDoc = async ({ url, params, headers, prismicUid = '' }, doc) =>
	await asyncPipe(
		processDoc({
			url,
			params,
			headers,
			prismicUid,
		}),
		withGroupedCategories,
		normalizeGlobalData
	)(doc);

// Source: https://blog.bloomca.me/2018/01/27/asynchronous-reduce-in-javascript.html
const asyncReducer = async (arr, handler, initialValue) => {
	let output = initialValue;

	for (const v of arr) {
		output = await handler(output, v);
	}

	return output;
};

export const asyncPipe = (...fns) => async (x) => await asyncReducer(fns, async (y, f) => await f(y), x);

const convertSlice = (slice) => {
	return Object.entries(slice).reduce(
		(final, [key, val]) => {
			if (key === 'primary') {
				final.props = {
					...final.props,
					...val,
				};
			} else if (key === 'items') {
				if (!final.props.items) final.props.items = [];

				final.props = {
					...final.props,
					items: [...final.props.items, ...val],
				};
			} else if (key === 'sliceType') {
				final[key] = pascalCase(val);
			} else {
				final[key] = val;
			}

			return final;
		},
		{
			props: {},
		}
	);
};

const withNormalizeSlices = (doc, slicesPropName = 'slices') => {
	const { [slicesPropName]: slices, elementProducts, afterSubmit } = doc;
	if (!slices) return doc;

	const normalizedSlices = {
		slices: slices.map((slice) => convertSlice(slice)),
	};

	if (elementProducts) {
		normalizedSlices.elementProducts = elementProducts.map((slice) => convertSlice(slice));
	}

	if (afterSubmit) {
		normalizedSlices.afterSubmit = afterSubmit.map((slice) => convertSlice(slice));
	}

	return {
		...doc,
		...normalizedSlices,
	};
};

const withCamelCase = (obj, depth = 0) => {
	if (!obj || typeof obj !== 'object') return obj;

	return Array.isArray(obj)
		? obj.map((x) => withCamelCase(x, depth + 1))
		: Object.entries(obj).reduce((final, [key, val]) => {
				final[camelCase(key)] = withCamelCase(val, depth + 1);

				return final;
		  }, {});
};

const withOrganizedSeo = (doc) =>
	Object.entries(doc).reduce(
		(final, [key, val]) => {
			if (key.match(/^seo([A-Z])?/)) {
				final.seo[key === 'seo' ? 'slices' : key[3].toLowerCase() + key.slice(4)] =
					key === 'seo' ? withCamelCase(val).map((x) => convertSlice(x)) : val;
			} else {
				final[key] = val;
			}

			return final;
		},
		{
			seo: {},
		}
	);

const withAddedElementProducts = (doc) => {
	if (doc.elementProducts && doc.elementProducts.length) {
		const elementProductsById = Object.fromEntries(
			doc.elementProducts.map((slice) => [slice.props.elementProductsId, slice])
		);

		const injectProductsFromPrismic = (obj) =>
			Object.fromEntries(
				Object.entries(obj)
					.filter(([key]) => key !== 'items')
					.map(([key, val]) => {
						return key === 'elementProductsId'
							? ['productsFromPrismic', (elementProductsById[val] && elementProductsById[val].props.items) || val]
							: [key, val];
					})
			);

		const slices = doc.slices.map((slice) => {
			const { props } = slice;

			const newPropsWithoutItems = injectProductsFromPrismic(props);
			const newItems = (props.items || []).map(injectProductsFromPrismic);

			return {
				...slice,
				props: {
					...newPropsWithoutItems,
					items: newItems,
				},
			};
		});

		// Create a copy of doc to avoid mutating the doc object
		const docCopy = { ...doc };
		delete docCopy.elementProducts;

		return { ...docCopy, slices };
	}

	return doc;
};

const withAdditionalSlicesData = (ctx) => async (doc) => {
	const uid =
		ctx.prismicUid ||
		(await asyncPipe(
			(url) => url.split('/'),
			(splitUrl) => splitUrl[splitUrl.length - 1]
		)(ctx.url));

	const { slices } = doc;

	if (!slices || !slices.length) return await Promise.resolve(doc);

	const dataLoaders = slices.reduce((loaders, { sliceType }) => {
		const loader = slicesDataLoaders[sliceType];

		if (loader && !loaders.includes(loader)) loaders.push(loader);

		return loaders;
	}, []);

	const productReviewsLoader = slicesDataLoaders[uid];

	if (productReviewsLoader) dataLoaders.push(productReviewsLoader);

	let additionalResources = {};

	if (dataLoaders.length) {
		const response = await Promise.all(dataLoaders.map((loader) => loader(ctx)));

		additionalResources = Object.fromEntries(response.map((res) => [res.objectName, res.data]));
	}

	return {
		...doc,
		...additionalResources,
	};
};

export const processDoc = (ctx) => async (doc) =>
	await asyncPipe(
		withCamelCase,
		withNormalizeSlices,
		withAddedElementProducts,
		withOrganizedSeo,
		withAdditionalSlicesData(ctx)
	)(doc);

const groupedCategoriesExclusions = [
	'promoModals',
	'autoApplyCouponCodesErrorMessage',
	'productsToAutoApplyCouponCodesTo',
	'elementProducts',
];

export const withGroupedCategories = (doc) => {
	return Object.entries(doc).reduce((final, [key, val]) => {
		const keys = Object.keys(doc);
		const prefix = (key.match(/^([a-z]+)[A-Z]/) || [])[1];

		if (
			!groupedCategoriesExclusions.includes(key) &&
			prefix &&
			!keys.includes(prefix) &&
			keys.some((key) => key.startsWith(prefix))
		) {
			if (!final[prefix]) final[prefix] = {};

			const prop = key.slice(prefix.length);
			final[prefix][camelCase(prop[0].toLowerCase() + prop.slice(1))] = val;
		} else {
			final[key] = val;
		}

		return final;
	}, {});
};

export const normalizeGlobalData = (doc) => {
	const variationFeature =
		doc.body3 &&
		fromEntries(
			doc.body3
				.filter((slice) => slice.sliceType === 'feature_variation')
				.map((slice) => convertSlice(slice))
				.map((slice) => [slice.props.variationId, slice.props])
		);
	const variationPromoBar =
		doc.body1 &&
		fromEntries(
			doc.body1
				.filter((slice) => slice.sliceType === 'promo_bar_variant')
				.map((slice) => convertSlice(slice))
				.map((slice) => [slice.props.variationId, slice.props])
		);
	const footer = doc.footer.map((slice) => convertSlice(slice));
	const clyde = doc.body5.map((slice) => convertSlice(slice));
	const productsGroupedVariations =
		doc.slices &&
		fromEntries(
			doc.slices
				.filter((slice) => slice.sliceType === 'ProductsGrouped')
				.map((slice) => convertSlice(slice))
				.map((slice) => [slice.props.variationId, slice.props])
		);

	const alertVariations =
		doc.body2 &&
		fromEntries(
			doc.body2
				.filter((slice) => slice.sliceType === 'alert_variation')
				.map((slice) => convertSlice(slice))
				.map((slice) => [slice.props.variationId, slice.props])
		);

	const clydeShow = doc.clyde.show;

	const footerSubscribe = footer.find((slice) => slice.sliceType === 'Subscribe');
	const footerText = footer.find((slice) => slice.sliceType === 'Text');
	const footerLinks = footer.find((slice) => slice.sliceType === 'Links');
	const footerMenus = footer.filter((slice) => slice.sliceType === 'Menu');
	const footerSocialLinks = footer.find((slice) => slice.sliceType === 'SocialIcons');

	const clydePages = clyde.find((slice) => slice.sliceType === 'ClydePages');

	const errorMessages = asArray(doc.errorMessages);

	const newPreorderProducts = asArray(doc.magento.preorderProducts);
	const newAutoApplyCouponCodes = asArray(doc.magento.productsToAutoApplyCouponCodesTo);

	return {
		header: {
			...doc.header,
			menu: asArray(doc.header.menu),
			submenuItems: asArray(doc.header.submenuItems),
			menuRight: asArray(doc.header.menuRight),
			products: asArray(doc.header.products),
			mobileLinks: asArray(doc.header.mobileLinks),
			desktopCtaText: doc.header.productsCtaText,
			desktopCtaLink: doc.header.productsCtaLink,
			promoBars: asArray(doc.header.promoBars),
			ctas: asArray(doc.header.ctas),
		},

		footer: {
			subscribe: footerSubscribe ? footerSubscribe.props : null,
			menus: footerMenus,
			text: footerText ? footerText.props.text : '',
			links: footerLinks ? footerLinks.props.items : [],
			socialLinks: footerSocialLinks ? footerSocialLinks.props.items : [],
		},

		features: {
			items: doc.features,
			maxWidthSingleMobile: doc.featuresMaxWidthSingleMobile,
			maxWidthSingleDesktop: doc.featuresMaxWidthSingleDesktop,
			maxWidthContainer: doc.featuresMaxWidthContainer,
			optionalTitle: doc.featuresOptionalTitle,
		},

		clyde: {
			showClyde: clydeShow,
			clydePages: clydePages ? clydePages.props.items : [],
		},

		productsGroupedV2: productsGroupedVariations,

		promoBarVariations: variationPromoBar,
		alertV1: alertVariations,
		featureVariations: variationFeature,

		magento: {
			...doc.magento,

			cartDescriptions: asArray(doc.magento.cartDescriptions).reduce((obj, { sku, product, description }) => {
				const objSku = (product && product.sku) || sku;
				obj[objSku] = description;
				return obj;
			}, {}),

			cartSubscriptionInfo: asArray(doc.magento.cartSubscriptionInfo).reduce((obj, { sku, product, description }) => {
				const objSku = (product && product.sku) || sku;
				obj[objSku] = description;
				return obj;
			}, {}),

			outOfStockToggles: asArray(doc.magento.outOfStockToggles).reduce(
				(obj, { sku = '', product, outOfStockToggle = false, outOfStockValue: value }) => {
					const objSku = (product && product.sku) || sku;
					if (!objSku || !outOfStockToggle) return obj;

					value = value || '';
					const insertIndex = value && value.includes(' -') && value.indexOf(' -');

					return {
						...obj,
						[objSku]: {
							value: insertIndex ? `${value.slice(0, insertIndex)} - ${objSku}${value.slice(insertIndex)}` : value,
							ctaText: value && (!value.includes(' -') ? value : value.match(/.+?(?= -)/g)[0]),
						},
					};
				},
				{}
			),

			preOrders: (newPreorderProducts.length
				? newPreorderProducts
				: (doc.magento.preorderProducts || '').split(',')
			).reduce((obj, val) => {
				const sku = typeof val === 'string' ? val : val.product && val.product.sku;
				obj[(sku || '').trim()] = true;
				return obj;
			}, {}),

			autoApplyCouponCodes: (newAutoApplyCouponCodes.length
				? newAutoApplyCouponCodes
				: (doc.productsToAutoApplyCouponCodesTo || '').split(',')
			).reduce((obj, val) => {
				const sku = typeof val === 'string' ? val : val.product && val.product.sku;
				obj[(sku || '').trim()] = true;
				return obj;
			}, {}),

			autoApplyCouponCodesErrorMessage: doc.autoApplyCouponCodesErrorMessage,

			specialPrices: asArray(doc.magento.specialPrices).reduce((obj, { sku, product, price }) => {
				const objSku = (product && product.sku) || sku;
				obj[objSku] = price;
				return obj;
			}, {}),
		},

		promoModals: asArray(doc.promoModals),

		other: doc.other,

		statisticsV3: {
			...doc.statistics,
			items: asArray(doc.statistics.panels),
		},

		alert: {
			...doc.alert,
			isClosingEnabled: doc.alert.closingEnabled,
			link: doc.alert.link || {},
		},

		antiFlicker: {
			...doc.anti,
			enabled: doc.anti.flickerEnabled,
			onlyOnSpecifiedPaths: doc.anti.flickerOnlyOnSpecifiedPaths,
			specifiedPaths: asArray(doc.anti.flickerSpecifiedPaths).map(({ path }) => path),
		},

		error:
			errorMessages && errorMessages.length
				? fromEntries(
						errorMessages.map((item) => [
							item.message_for,
							{
								heading: item.heading,
								message: item.message,
								ctaLink: item.cta_link,
								ctaText: item.cta_text,
							},
						])
				  )
				: {},
	};
};
