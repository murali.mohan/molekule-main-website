import { anattaPrismicRequest, asyncPipe, processDoc, withGroupedCategories, normalizeGlobalData } from './utils';

export const prismicDataMiddleware = async (ctx) => {
	const { locale, type, uid } = ctx.params;
	let doc;
	try {
		doc = await anattaPrismicRequest({ locale, type, uid }, ctx);
	} catch (e) {
		ctx.throw(e.message.includes('404') ? 404 : 500, e.message);
	}

	ctx.body = await asyncPipe(processDoc(ctx))(doc);
};

export const prismicGlobalMiddleware = async (ctx) => {
	const { locale } = ctx.params;
	let doc;
	try {
		doc = await anattaPrismicRequest({ locale, uid: 'global' }, ctx);
	} catch (e) {
		ctx.throw(e.message.includes('404') ? 404 : 500, e.message);
	}

	ctx.body = await asyncPipe(processDoc(ctx), withGroupedCategories, normalizeGlobalData)(doc);
};
