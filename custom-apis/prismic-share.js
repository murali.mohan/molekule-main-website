export default function prismicShare(ctx) {
	const url = ctx.params[0];
	ctx.assert(url, 'URL is required');
	console.log('prismic url', url);
	ctx.cookies.set('io.prismic.molekulePreview', url, { path: '/', httpOnly: false });
	ctx.redirect(url);
}
