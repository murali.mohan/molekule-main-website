import cachedRequest from './utils/cached-request';

export default async function careersPostings(ctx) {
	try {
		const res = await cachedRequest('https://api.lever.co/v0/postings/molekule?group=team&mode=json');
		ctx.append('Cache-Control', 'public, max-age=900');
		ctx.body = res;
	} catch ({ response }) {
		ctx.status = response.status;
		ctx.message = response.statusText;
		ctx.body = response.data;
	}
}
