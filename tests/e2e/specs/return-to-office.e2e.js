describe('/return-to-office', () => {
	it('has the correct title', () => {
		cy.visit('/return-to-office');
		cy.title().should('equal', 'Title');
	});
});
