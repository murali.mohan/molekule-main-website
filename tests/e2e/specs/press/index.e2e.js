describe('/press/', () => {
	it('has the correct title', () => {
		cy.visit('/press/');
		cy.title().should('equal', 'Press');
	});
});
