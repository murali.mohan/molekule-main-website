/// <reference types="Cypress" />
// https://docs.cypress.io/guides/tooling/intelligent-code-completion.html#Set-up-in-your-Dev-Environment

const miniNameShop = 'Air Mini';
const miniNameCart = 'Molekule Air Mini';
const miniPrice = '$399';
const productClass = '[class*="product_"]:visible';
const pecoFilterName = 'PECO-Filter';
const pecoFilterPrice = 80;
const preFilterTrayName = 'Pre-Filter Tray';
const preFilterTrayPrice = 10;

const magentoEmail = `jakub+molekule-automated-tests${
	(Cypress.env('MAGENTO_URL') || '').includes('staging-anatta') ? '3' : '2'
}@anattadesign.com`;
const magentoPassword = 'U/zW#Bq:]j7&K-+q2LhEG+&Cll]-o@kZ';

describe('Magento Integration', () => {
	it('Mini Add To Cart', () => {
		cy.visit('/shop');

		cy.addToCart(miniNameShop);

		cy.get('#cart-panel:visible').within(() => {
			cy.contains(miniNameCart)
				.closest('[class*="product_"]:visible')
				.within(() => {
					cy.root().should('contain', 'Features auto-refills');
					cy.get('[class*="productPrice_"]:visible').first().should('contain', miniPrice);
					cy.get('[class*="productText_"]:visible').should('contain', 'Includes a 6mo. filter');
					cy.get('select').should('have.value', '1');
				});

			cy.contains('Total').next().should('contain', miniPrice);
		});

		cy.goToCheckout();
		cy.magentoSummaryContains(miniNameCart, miniPrice, '1');
	});

	it('Guest Cart Operations', () => {
		cy.visit('/shop');

		cy.addToCart(miniNameShop);
		cy.cartClose();
		cy.addToCart(pecoFilterName);
		cy.cartClose();
		cy.addToCart(preFilterTrayName);

		cy.get('#cart-panel').within(() => {
			// TODO: check if subtotal updates

			cy.get(productClass).should('have.length', '3');
			cy.cartUpdateQuantity(pecoFilterName, 5);

			cy.get('h6:visible')
				.contains(pecoFilterName)
				.closest(productClass)
				.within(($el) => {
					cy.get('[class*="productPrice_"]:visible')
						.first()
						.should('contain', `${pecoFilterPrice * 5}`);
					cy.get('[class*="productText_"]:visible').should('contain', 'For Molekule Air');
					cy.get('select').within(() => {
						cy.get('option').should('have.length', 11);
						cy.root().should('have.value', '5');
					});
				});

			cy.cartDelete(preFilterTrayName, 2);
		});

		cy.goToCheckout();
		cy.magentoSummaryContains(miniNameCart, miniPrice, '1');
		cy.magentoSummaryContains(pecoFilterName, `$${pecoFilterPrice * 5}`, '5');
		cy.get('.cart-items').should('not.contain', preFilterTrayName);

		cy.visit('/shop');
		cy.addToCart(pecoFilterName);
		cy.cartClose();
		cy.addToCart(preFilterTrayName);

		cy.goToCheckout();
		cy.magentoSummaryContains(miniNameCart, miniPrice, '1');
		cy.magentoSummaryContains(pecoFilterName, `$${pecoFilterPrice * 6}`, '6');
		cy.magentoSummaryContains(preFilterTrayName, `$${preFilterTrayPrice}`, '1');
	});

	it('Guest Cart Then Authenticated Operations', () => {
		cy.visit('/shop');

		cy.addToCart(miniNameShop);
		cy.cartClose();
		cy.addToCart(preFilterTrayName);
		cy.cartClose();
		cy.addToCart(preFilterTrayName);

		cy.goToCheckout();
		cy.magentoSummaryContains(miniNameCart, miniPrice, '1');
		cy.magentoSummaryContains(preFilterTrayName, `$${preFilterTrayPrice * 2}`, '2');
		cy.get('.aw-sidebar-product-list > .cart-items > *').should('have.length', 2);

		cy.get('.tab-sign-in').contains('Sign in').click();

		cy.get('.aw-onestep-groups .account .block-customer-login').within(() => {
			cy.get('input[type="email"]').type(magentoEmail);
			cy.get('input[type="password"]').type(magentoPassword);
			cy.get('button[type="submit"]').click();
		});

		cy.get('.aw-onestep-groups .account').should('contain', magentoEmail);

		cy.magentoSummaryContains(miniNameCart, null, ($el) => parseInt($el.text()) >= 1);
		cy.magentoSummaryContains(preFilterTrayName, null, ($el) => parseInt($el.text()) >= 2);

		cy.visit('/shop');
		cy.get('header [class*="cartIndicator_"]'); // wait for cart to load

		cy.addToCart(pecoFilterName);
		cy.get('#cart-panel:visible').within(() => {
			// cy.get('[class*="workingOverlay_"]').should('not.exist');
			cy.cartUpdateQuantity(miniNameCart, 1);
			cy.cartUpdateQuantity(pecoFilterName, 3);
			cy.cartDelete(preFilterTrayName, 2);
		});
		cy.cartClose();
		cy.addToCart(preFilterTrayName);

		cy.goToCheckout();
		cy.magentoSummaryContains(miniNameCart, miniPrice, '1');
		cy.magentoSummaryContains(pecoFilterName, `$${pecoFilterPrice * 3}`, '3');
		cy.magentoSummaryContains(preFilterTrayName, `$${preFilterTrayPrice}`, '1');
	});
});
