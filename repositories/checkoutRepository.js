const checkoutBaseUrl = process.env.baseApi.checkout_base_url + '/users/checkout';
export default ($axios, countryCode) => ({
	applyPromoCode(promocode, cartid) {
		return $axios.post(`${checkoutBaseUrl}/${cartid}/discounts/${promocode}?country=${countryCode}`);
	},
	removePromoCode(promocode, cartid) {
		return $axios.delete(`${checkoutBaseUrl}/${cartid}/discounts/${promocode}?country=${countryCode}`);
	},
	placeOrder(payload) {
		return $axios.post(`${checkoutBaseUrl}/order`, payload);
	},
	setupIntent(payload) {
		return $axios.post(`${checkoutBaseUrl}/payment?country=${countryCode}`, payload);
	},
	setupPaymentToken(payload) {
		return $axios
			.post(`${checkoutBaseUrl}/payment/token`, payload)
			.then((res) => {
				return res;
			})
			.catch((error) => {
				console.log('error', error);
				return {};
			});
	},
	getAccount(id, customerId = '') {
		let cusParam = '';
		if (customerId) {
			cusParam = `?customerId=${customerId}`;
		}
		return $axios.get(`${checkoutBaseUrl}/account/${id}${cusParam}`);
	},
	getShippingMethods(cartid) {
		return $axios
			.get(`${checkoutBaseUrl}/${cartid}/shipping-methods?country=${countryCode}`)
			.then((res) => {
				return res;
			})
			.catch((error) => {
				console.log('error', error.response.data.statusCode, error.response.data.errorMessage);
				return {};
			});
	},
	getCheckoutSurveys() {
		return $axios
			.get(`${checkoutBaseUrl}/surveys`)
			.then((res) => {
				return res;
			})
			.catch((error) => {
				console.log('error', error.response.data.statusCode, error.response.data.errorMessage);
				return {};
			});
	},
	addSurveyResponse(payload) {
		const { customerId, customerEmail, surveyResponse, customerNumber, other } = payload;
		return $axios.post(
			`${checkoutBaseUrl}/surveys/response?customerId=${customerId}&customerNumber=${customerNumber}&email=${encodeURIComponent(
				customerEmail.toLowerCase()
			)}&other=${other}&surveyResponse=${surveyResponse}`
		);
	},
	reOrder(orderId) {
		return $axios.post(
			`${checkoutBaseUrl}/reorder/${orderId}/loadShippingAddress/false/loadShippingMethod/false?country=${countryCode}`
		);
	},
	smartPost(cartId) {
		return $axios.get(`${checkoutBaseUrl}/${cartId}/smart-post`);
	},
});
