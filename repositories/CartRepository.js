const cartBaseUrl = process.env.baseApi.cart_base_url + '/carts';
export default ($axios, countryCode = 'US') => ({
	get(id) {
		return $axios.get(`${cartBaseUrl}/${id}?country=${countryCode}`);
	},

	create(payload) {
		payload.country = countryCode;
		return $axios.post(`${cartBaseUrl}?country=${countryCode}`, payload);
	},
	update(id, payload) {
		payload.country = countryCode;
		return $axios.post(`${cartBaseUrl}/${id}?country=${countryCode}`, payload);
	},
});
