import Vue from 'vue';
import VueRouter from 'vue-router';

export const defaultLayout = () => {
	return {
		name: 'defaultLayout',

		router: new VueRouter(),

		provide() {
			return {
				openPromoModal: () => {},
				openCart: () => {},
				getHeaderHeight: () => {},
			};
		},

		render(createElement) {
			return createElement('story');
		},
	};
};
