const { getWebpackConfig } = require('nuxt');

module.exports = {
	stories: ['../components/**/*.stories.@(js|mdx)'],
	addons: ['@storybook/addon-docs', '@storybook/addon-links', '@storybook/addon-controls', '@storybook/addon-knobs'],
	// Source: https://dev.to/f3ltron/storybook-tailwind-nuxt-one-webpack-config-3kbj
	webpackFinal: async (sbWebpack, { configType }) => {
		const nuxtWebpack = await getWebpackConfig('client', {
			for: process.env.NODE_ENV === 'production' ? 'build' : 'dev',
		});

		const mdxRules = sbWebpack.module.rules.filter((rule) => {
			const reg = RegExp(rule.test);
			return reg.test('.mdx') || reg.test('.stories.mdx');
		});

		const vueDocGenRule = sbWebpack.module.rules.find(
			(rule) => rule.loader && rule.loader.includes('vue-docgen-loader')
		);

		const recomposedWebpackConfig = {
			mode: nuxtWebpack.mode,
			devtool: nuxtWebpack.devtool,
			entry: sbWebpack.entry,
			output: sbWebpack.output,
			bail: sbWebpack.bail,
			module: {
				rules: [...nuxtWebpack.module.rules, ...mdxRules, vueDocGenRule],
			},
			plugins: sbWebpack.plugins,
			resolve: {
				extensions: nuxtWebpack.resolve.extensions,
				modules: nuxtWebpack.resolve.modules,
				alias: {
					...nuxtWebpack.resolve.alias,
					...sbWebpack.resolve.alias,
				},
			},
			optimization: sbWebpack.optimization,
			performance: {
				...sbWebpack.performance,
				...nuxtWebpack.performance,
			},
		};

		return recomposedWebpackConfig;
	},
};
