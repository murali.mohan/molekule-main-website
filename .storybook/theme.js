import { create } from '@storybook/theming/create';

export default create({
	base: 'light',
	colorSecondary: '#20e47d',
	textColor: '#3c3b3b',
	brandTitle: `Molekule Modules`,
});
