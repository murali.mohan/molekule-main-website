import Vue from 'vue';
import { addDecorator } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import StoryRouter from 'storybook-vue-router';
import BaseComponentPlugin from '@/plugins/base-components';
import initializeLazyLoading from '@/plugins/lazy-loading';
import initializeRichContent from '@/plugins/rich-content';
import '@/plugins/lazysizes.client';
import '@/plugins/global-components';
import '@/plugins/filters';
import '@/layouts/app.scss';
import './index.css';
import { withKnobs } from '@storybook/addon-knobs';

BaseComponentPlugin();
initializeLazyLoading({ query: {} });
initializeRichContent({ app: { router: StoryRouter } });

Vue.component('nuxt-link', {
	props: ['to'],
	methods: {
		log() {
			action('link target')(this.to);
		},
	},
	template: '<a href="#" @click.prevent="log()"><slot>NuxtLink</slot></a>',
});

Vue.component('client-only', {
	template: '<div><slot /></div>',
});

Vue.prototype.$tealiumEvent = action('$tealiumEvent');

export const parameters = {
	controls: { expanded: true },
};

export const decorators = [withKnobs];
