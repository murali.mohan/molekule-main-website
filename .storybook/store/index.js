import Vue from 'vue';
import Vuex from 'vuex';
import { state, actions, mutations } from '@/store/screen';
import magentoProducts from './json/magento-products.json';
import globalDataFeatures from './json/global-data-features.json';
import globalDataProductsGrouped from './json/global-data-products-grouped.json';
import globalDataPromoBarVariations from './json/global-data-promo-bar-variations.json';
import globalDataFeatureVariations from './json/global-data-feature-variations.json';
import { camelize } from 'camelize-object-key';

Vue.use(Vuex);

const isUSWebsite = true;
const prSnippets = {
	en_US: {
		'molekule-air': {
			page_id: 'molekule-air',
			rollup: { average_rating: 4.3, review_count: 794, answered_questions: 0 },
		},
		'molekule-air-mini-plus': {
			page_id: 'molekule-air',
			rollup: { average_rating: 4.3, review_count: 795, answered_questions: 0 },
		},
		'molekule-air-mini': {
			page_id: 'molekule-air-mini',
			rollup: { average_rating: 3.89, review_count: 19, answered_questions: 0 },
		},
	},
	en_CA: {},
};

const screen = {
	namespaced: true,
	state,
	actions,
	mutations,
};

const magento = {
	namespaced: true,
	state: () => ({
		products: magentoProducts,
	}),
	getters: {
		currencySymbol: () => '$',
	},
};

const reviews = {
	namespaced: true,
	state: {
		isUSWebsite: true,
		prSnippets,
	},
	getters: {
		reviewsSnippetsById({ isUSWebsite, prSnippets }) {
			const locale = isUSWebsite ? 'en_US' : 'en_CA';
			return prSnippets[locale];
		},
	},
};

export default new Vuex.Store({
	state: {
		isUSWebsite,
		prSnippets,
		globalData: {
			magento: {
				preOrders: { '': true },
			},

			alert: {
				content:
					'<p>The US FDA has cleared Molekule Air Pro RX as a Class II Medical Device to destroy viruses &amp; bacteria in commercial settings. <a  href="/air-purifier-pro-rx"><strong>Learn more &gt;</strong></a></p>',
				textColor: '#393939',
				backgroundColor: '#cfe9db',
				backgroundImage: {},
				backgroundImageMobile: {},
			},

			features: globalDataFeatures,

			productsGroupedV2: {
				global: camelize(globalDataProductsGrouped),
			},

			promoBarVariations: {
				global: camelize(globalDataPromoBarVariations),
			},

			featureVariations: {
				global: camelize(globalDataFeatureVariations),
			},

			alertV1: {
				global: {
					alertBackgroundColor: '#b8dbc7',
					alertBackgroundImage: {},
					alertBackgroundImageMobile: {},
					alertClosingEnabled: true,
					alertContent:
						'<p>The US FDA has cleared Molekule Air Pro RX as a Class II Medical Device to destroy viruses &amp; bacteria in commercial settings. <a  href="/air-purifier-pro-rx"><strong>Learn more &gt;</strong></a></p>',
					alertLink: '',
					alertShowAgainAfter: '',
					alertTextColor: '#ffffff',
					id: '',
					variationId: 'global',
				},
			},
		},
	},

	actions: {
		FETCH_PRODUCT_REVIEWS: () => {},
	},

	modules: {
		screen,
		magento,
		reviews,
	},
});
