import BlockFilters from './-block-filters.vue';

describe('@views/technology/BlockFilters', () => {
	it('exports a valid component', () => {
		expect(BlockFilters).toBeAComponent();
	});
});
