import BlockMicroscope from './-block-microscope.vue';

describe('@views/technology/BlockMicroscope', () => {
	it('exports a valid component', () => {
		expect(BlockMicroscope).toBeAComponent();
	});
});
