import IndexPage from './';

describe('@views/share', () => {
	it('is a valid view', () => {
		expect(IndexPage).toBeAPageComponent();
	});
});
