import createSlicesPage from '@/utils/create-slices-page';

export default createSlicesPage('air_purifier_pro', {
	additionalRequests: async ({ store }) => await store.dispatch('reviews/FETCH_PRODUCT_REVIEWS_SNIPPETS'),
	pageComponentData: (vm) => ({ class: 'no-block-spacing' }),
})();
