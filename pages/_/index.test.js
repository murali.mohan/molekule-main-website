import IndexPage from './';

describe('@views/_', () => {
	it('is a valid view', () => {
		expect(IndexPage).toBeAPageComponent();
	});
});
