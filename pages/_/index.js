import createSlicesPage from '@/utils/create-slices-page';
import { getLocaleMatchInRoutePath } from '@/utils/locale-helpers';

export default createSlicesPage('Page', {
	pageRequest: async ({ store, route, req }) => {
		const localeMatchInRoute = getLocaleMatchInRoutePath(route.path);
		console.log('pages route', localeMatchInRoute)
		const path = route.path.replace(`/${localeMatchInRoute}`, '').replace(/(?:^\/|\/$)/g, '');
		// As prismic doesn't support nesting with / we are using ---- for that
		const uid = path.replace(/\//g, '----');

		return await store.dispatch('pages/FETCH_PAGE_DATA', { pageName: `page/${uid}`, req });
	},
	pageComponentData: (vm) => ({ class: 'no-block-spacing' }),
})();
