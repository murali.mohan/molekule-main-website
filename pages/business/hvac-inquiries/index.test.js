import IndexPage from './index.vue';

describe('@views/index', () => {
	it('is a valid view', () => {
		expect(IndexPage).toBeAPageComponent();
	});
});
