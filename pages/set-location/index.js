export default {
	fetch(ctx) {
		return new Promise((resolve) => {
			const oldRoute = ctx.from || { path: '/', query: {} };
			const newRoute = {
				...oldRoute,
				query: { ...oldRoute.query, setLocation: ctx.query.location, redirect: undefined },
			};
			const newResolved = ctx.app.router.resolve(newRoute);

			if (process.server) {
				ctx.redirect(newResolved.href);
				resolve();
			} else {
				window.location.replace(newResolved.href);
			}
		});
	},

	// beforeRouteEnter(to, from, next) {
	// 	console.log({ from });
	// 	console.log(this);
	// },

	render() {
		return null;
	},
};
