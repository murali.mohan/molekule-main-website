import IndexPage from './';

describe('@views/return-to-office', () => {
	it('is a valid view', () => {
		expect(IndexPage).toBeAPageComponent();
	});
});
