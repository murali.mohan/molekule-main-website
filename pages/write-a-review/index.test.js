import IndexPage from './index.vue';

describe('@views/write-a-review', () => {
	it('is a valid view', () => {
		expect(IndexPage).toBeAPageComponent();
	});
});
