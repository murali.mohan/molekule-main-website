import BlockIntro from './-block-intro.vue';

describe('@views/write-a-review/BlockIntro', () => {
	it('exports a valid component', () => {
		expect(BlockIntro).toBeAComponent();
	});
});
