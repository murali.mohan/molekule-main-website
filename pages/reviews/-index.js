import createSlicesPage from '@/utils/create-slices-page';

const getPageName = (path) => path.split('/').filter(Boolean).pop().replace('-reviews', '').replace(/-/g, '_');

export default createSlicesPage('ReviewsPage', {
	pageRequest: async ({ store, route, error, req }) => {
		const pageName = getPageName(route.path);
		if (!['air', 'air_mini', 'air_mini_plus', 'air_pro'].includes(pageName)) {
			error({ statusCode: 404, message: 'This page could not be found' });
			return;
		}

		return await Promise.resolve(store.dispatch('pages/FETCH_PAGE_DATA', { pageName: `reviews_${pageName}`, req }));
	},

	asyncDataResponseFormatter: (res) => ({
		...res,
		productReviews: { ...res.page.productReviews },
		sortBy: 'Newest',
		pageId: res.page.productReviews.pageId,
	}),

	sliceNamesMap: {
		Reviews: 'ReviewsReviews',
		Social: 'ReviewsSocial',
	},

	additionalSliceProps: (vm) => ({
		pageId: vm.pageId,
		reviewsListing: vm.productReviews,
		reviewsSortBy: vm.sortBy,
		sortByChanged: ({ reviews, sortBy }) => {
			vm.sortBy = sortBy;
			vm.productReviews = reviews;
		},
		loadMore: (reviews) => {
			const product = vm.productReviews;
			const currentReviews = product.results;

			product.paging = reviews.paging;
			product.results = currentReviews.concat(reviews.results);
		},
	}),
})();
