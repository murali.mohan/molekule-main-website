import IndexPage from './';

describe('@views/shop', () => {
	it('is a valid view', () => {
		expect(IndexPage).toBeAPageComponent();
	});
});
