import AccessibilityCommitment from './index.js';

describe('@views/accessibility-commitment', () => {
	it('is a valid view', () => {
		expect(AccessibilityCommitment).toBeAPageComponent();
	});
});
