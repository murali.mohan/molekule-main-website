import createSlicesPage from '@/utils/create-slices-page';
import TemplateContentSections from '@components/template-content-sections';

export default createSlicesPage('a11y_commitment', {
	pageComponentData: (vm) => ({
		props: { contents: vm.page.slices.filter((slice) => slice.sliceType === 'ContentSection') },
	}),
})(TemplateContentSections);
