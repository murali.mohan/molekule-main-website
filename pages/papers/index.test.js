import IndexPage from './';

describe('@views/index', () => {
	it('is a valid view', () => {
		expect(IndexPage).toBeAPageComponent();
	});
});
