import createSlicesPage from '@/utils/create-slices-page';
import TemplateContentSections from '@components/template-content-sections';

export default createSlicesPage('legal', {
	pageComponentData: (vm) => ({
		props: { contents: vm.page.slices.filter((slice) => slice.sliceType === 'ContentSection') },
	}),
})(TemplateContentSections);
