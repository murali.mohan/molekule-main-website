import createSlicesPage from '@/utils/create-slices-page';
import TemplateHomepage from '@components/template-homepage';

export default createSlicesPage('homepage_b', {
	additionalRequests: async ({ store }) => {
		await store.dispatch('reviews/FETCH_PRODUCT_REVIEWS_SNIPPETS');
	},
	pageComponentData: (vm) => ({ class: 'no-block-spacing' }),
})(TemplateHomepage);
