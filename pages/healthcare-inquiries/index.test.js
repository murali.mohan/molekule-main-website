import IndexPage from './';

describe('@views/healthcare-inquiries', () => {
	it('is a valid view', () => {
		expect(IndexPage).toBeAPageComponent();
	});
});
