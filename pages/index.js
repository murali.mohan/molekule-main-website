import createSlicesPage from '@/utils/create-slices-page';
import TemplateHomepage from '@components/template-homepage';

export default createSlicesPage('homepage', {
	additionalRequests: async ({ store }) => {
		await store.dispatch('reviews/FETCH_PRODUCT_REVIEWS_SNIPPETS');
	},
	additionalSliceProps: (vm) => ({ multipleProductReviews: vm.page.productReviews }),
	pageComponentData: (vm) => ({ class: 'no-block-spacing' }),
})(TemplateHomepage);
